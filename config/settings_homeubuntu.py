#settings_workosx.py

vtk_dir = '/opt/kmc/include/vtk-6.2'
stxxl_dir = '/usr/local'
itk_libdir = '/opt/kmc/lib'
itk_incdir = '/opt/kmc/include/ITK-4.7'
itk_ver = '4.7'
vtk_ver = '6.2'