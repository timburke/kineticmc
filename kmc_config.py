#kmc_config.py
#Routines to help parse json config files and build variable matrices for testing
#a number of variable combinations.

import json
import numbers
import collections
import itertools
import kineticmc

def is_sequence(arg):
	return (not hasattr(arg, "strip") and
		hasattr(arg, "__getitem__") or
		hasattr(arg, "__iter__"))

class MonteCarloConfigurator:
	def __init__(self, filename):
		self.file = filename
		
		with open(filename, "r") as file:
			config = json.load(file)
			self.config = config
		
		if "params" in config:
			self.params = config["params"]
		else:
			self.params = {}
		
		if "config" in config:
			self.conditions = config["config"]
		else:
			raise ValueError("Config file had no 'config' section.  This is probably a mistake.")
			
		if "iteration_var" in self.conditions:
			self.itervar = self.conditions["iteration_var"]
		else:
			print "Assuming a default iteration variable of 'fields'"
			self.itervar = "fields"
	
	def get_postprocessing(self):
		"""
		If the user requested that the results from the trials be combined in some way,
		return the postprocessing method indicated.
		"""
		if "postprocessing" in self.conditions:
			return self.conditions["postprocessing"]
		
		return None
	
	def get_variables(self):
		"""
		Return all the defined variable names in this config file as well as if they are being varied or not
		Returns a dictionary mapping variable names to tuples (<is sequence>, value)
		"""
		
		vars = {}
		
		for var,value in self.params.iteritems():
			if is_sequence(value):
				vars[var] = (True, value)
			else:
				vars[var] = (False, value)
		
		return vars

	def get_results(self):
		return map(lambda x: str(x), self.conditions["results"]) #force nonunicode so we can pass it to C++
		
	def get_result_widths(self):
		"""
		Return a list of the widths of the results listed in the config file
		"""
		results = self.get_results()
		
		c = kineticmc.SimulationController()
		
		return map(lambda x: c.get_result_columns(x), results)
	
	def get_result_names(self):
		"""
		Return a list of all the column names produced by these results, in order
		"""
		
		cols = []
		
		results = self.get_results()
		c = kineticmc.SimulationController()
		
		for result in results:
			names = c.get_result_column_names(result)
			cols.extend(names)
			
		return cols
		
	def get_total_result_width(self):
		widths = self.get_result_widths()
		
		return reduce(lambda x,y: x+y, widths)
	
	def get_result_offset_map(self):
		"""
		Build a map from result column name to offset without the result list
		"""

		results = self.get_results()
		
		curr_idx = 0
		c = kineticmc.SimulationController()
		
		offset_map = {}
		
		for result in results:
			width = c.get_result_columns(result)
			names = c.get_result_column_names(result)
			
			for name in names:
				offset_map[name] = curr_idx
				curr_idx += 1
		
		return offset_map

	def get_title(self):
		return self.conditions["title"]
	
	def _get_prefix(self):
		"""
		Get the file prefix for results coming from this simulation
		"""
		if "file_prefix" in self.params:
			return self.params["file_prefix"]

		return "run"
	
	def _is_list(self, var):
		if not isinstance(var, collections.Iterable):
			return False
		
		if isinstance(var, str) or isinstance(var, unicode):
			return False
		
		return True
	
	def _categorize_params(self):
		"""
		Categorize all the parameters in the config file as either
		fixed or variable
		"""
		
		fixed = []
		variable = []
		
		for (key, value) in self.params.iteritems():
			if self._is_list(value):
				variable.append((key, value))
			else:
				fixed.append((key, value))
		
		return (fixed, variable)
	
	def _get_format_spec(self, var):
		if isinstance(var, numbers.Number):
			return "f"
		elif isinstance(var, str) or isinstance(var, unicode):
			return "s"

		raise TypeError("You can only pass string or numeric variables")
	
	def _build_format_string(self, variable):
		"""
		Build a format string using all the variable parameters
		"""
		
		format = self._get_prefix()
		
		for var in variable:
			name = var[0]
			
			if name == self.itervar:
				continue
			
			spec = self._get_format_spec(var[1][0])
			
			format += '-{%s:%s}' % (name, spec)
		
		return format
	
	def get_format(self):
		"""
		Build format string automatically for this simulation
		"""
	
		(fixed, var) = self._categorize_params()
		
		return self._build_format_string(var)

	def count_expected_files(self):
		"""
		Return the number of expected data files given the parameter space dimensionality
		for this run.
		"""
		
		(fixed, var) = self._categorize_params()
		
		return reduce(lambda x,y: x*y, [len(x[1]) for x in var if x[0] != self.itervar], 1)
	
	def parameter_space(self):
		"""
		Return an iterator that will product every possible combination of variables not including the iteration variable
		"""
		
		(fixed, var) = self._categorize_params()
		
		space = [map(lambda y: (y, x[0]), x[1]) for x in var if x[0] != self.itervar]
				
		return itertools.product(*space)
	
	def get_fixed_params(self):
		return {key: value for (key, value) in self._categorize_params()[0]}
	
	def get_iteration_variable(self):
		return (self.itervar, self.params[self.itervar])
	
	def trials(self):
		return self.params["trials"]
	
	def make_parameter_dictionary(self, p):
		"""
		Take a list of tuples and convert it to a dictionary
		"""
		
		return {key: value for (value, key) in p}