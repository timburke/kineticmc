#!/usr/bin/python

import argparse
import numpy as np
import os.path
import os
from subprocess import call
import sys

def verify_same(arr):
	v = arr[0]
	
	return reduce(lambda x,y: x and y, map(lambda x: x==v, arr))

#Parse the arguments we need
parser = argparse.ArgumentParser(description='Combine multiple numpy arrays that were created by parallel processes into a single array.')
	
parser.add_argument('--mode', '-m', dest='mode', type=str, action='store', default='row', 
					choices = ['row, column'],
					help="Whether the files will be concatenated row by row or column by column.  Row by row means each row will combined.  The number of rows will be constant")

parser.add_argument('--output', '-o', dest='outfile', required=False,
					help="The destination file for the concatenated array.")

parser.add_argument('files', nargs="+", metavar="FILE",
					help = 'The input subarray files to be concatenated')

args = parser.parse_args()

subarrs = []

for file in args.files:
	(base, ext) = os.path.splitext(file)
	
	if ext == '.gz':
		call(['gunzip', file], shell = True)
		subarrs.append( (base, True) )
	else:
		subarrs.append( (file, False) )

#find the common prefix of the files to autogenerate an output file
if args.outfile is not None:
	outfile = args.outfile
else:
	#try to autogenerate an output file name
	common = os.path.commonprefix(map(lambda x: x[0], subarrs))
	if common[-1] == '/':
		print "Could not autogenerate an output file name, the longest substring was", common
		sys.exit(1)
	
	if common[-1] == '-':
		common = common[0:len(common[:-1])] #get rid of trailing -
		
	outfile = common + ".npy"
	print "Autogenerating output file: %s" % outfile


sub_w = []
sub_l = []

#get the length and width of each subarray

for arr in subarrs:
	a = np.load(arr[0])
	sub_l.append(a.shape[0])
	sub_w.append(a.shape[1])

res = None

if args.mode == 'row':
	if verify_same(sub_l) is False:
		print "Could not combine arrays because they did not have the same length"
		print sub_l
		sys.exit(1)
	
	w = reduce(lambda x,y: x+y, sub_w, 0)
	l = sub_l[0]
	
	res = np.ndarray([l, w])
	
	for i in xrange(0, len(subarrs)):
		a = np.load(subarrs[i][0])
		if i>0:
			start = reduce(lambda x,y: x+y, sub_w[0:i-1], 0)
		else:
			start = 0
		res[:, start:start+sub_w[i]] = a
else:
	if verify_same(sub_w) is False:
		print "Could not combine arrays because they did not have the same width"
		print sub_w
		sys.exit(1)
	
	l = reduce(lambda x,y: x+y, sub_l, 0)
	w = sub_w[0]
	
	res = np.ndarray([l, w])
	
	for i in xrange(0, len(subarrs)):
		a = np.load(subarrs[i][0])
		if i>0:
			start = reduce(lambda x,y: x+y, sub_l[0:i-1], 0)
		else:
			start = 0
		res[start:start+sub_l[i],:] = a
		
np.save(outfile, res)

