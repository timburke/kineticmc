#!/usr/bin/tcsh
#Pull kineticmc, build it for corn and place the result so that it can be used by python on corn
#note that this will overwrite whatever previous kineticmc version exists in ~/code

if ($#argv != 1) then
        echo "Usage: $0 [corn|barley]"
        echo "Build and install kineticmc either for corn or the barley cluster"
        exit 1
endif

echo "=== === === Starting Build Process === === ==="
cd /farmshare/user_data/timburke/code
rm -rf kineticmc

#clone the repo
echo "Cloning repository"
git clone git@bitbucket.org:timburke/kineticmc.git >& /dev/null 
cd kineticmc

set success=1
setenv cluster 1 #Set the cluster variable so that we pull in the right library directories in setup.py

#Build and install kineticmc
echo "Building"
python setup.py build >& /dev/null
if ($status == 0) then
	cp src/kineticmc.py ./
	python setup.py build >& /dev/null
	
	if ($1 == 'corn') then
		echo "Installing for corn"
		python setup.py install --user >& /dev/null
	else
		echo "Installing for barley"
		python setup.py install --prefix=/mnt/glusterfs/timburke/support >& /dev/null
	endif
else
	set success=0
	echo "***Error building code.***"
	python setup.py build #repeat to show error
endif

cd ../
rm -rf kineticmc

if ($success == 1) then
	echo "***Compilation successful***"
endif 

echo "=== === === Finished Build Process === === ==="

if ($success == 1) then
	exit 0
else
	exit 1
endif