import kineticmc as mc
from subprocess import call
import numpy as np
import os
import sys

c = mc.SimulationController()
params = mc.SimulationParameters()

params.field_type = mc.kIsotropic
params.dialectric_const = 4.0
params.lattice_const = 1.0
params.temp = 300
params.recomb_life = 1.0
params.elec_mob = 1.0
params.hole_mob = 1.0
params.trials = 100
params.field = 5e5
params.initial_r = 40.0

#set the world generator
params.world_spec = mc.PureMaterialSpec(-4.0, -3.5)

c.track_result('Fate')
c.track_result('Distance')

def run_with_field(field):
	global c, params, threads
	
	params.field = field
	c.initialize(params)
	c.run(threads)
	
	res = c.get_results()
	
	return res
	
def write_format(path):
	pass 

def run_for_sep(sep, nfields = 50, ntrials=10000):
	global c, params, nodes, me, suffix
	
	params.trials = ntrials
	
	if nodes != 1:
		#if we're using multiple nodes, reduce our trials
		load = params.trials / nodes
		if me == nodes-1:
			#last node takes up rounding slack
			params.trials = ntrials - (nodes-1)*load
		else:
			params.trials = load
	
		print "Node %d is taking %d of %d trials" % (me, params.trials, ntrials)
	
	res = np.ndarray([nfields*4, params.trials])
	
	params.initial_r = sep
	
	fields = np.logspace(0, 7, 50)
	
	for i in xrange(0,nfields):
		print "Starting Field %f" % fields[i]
		arr = run_with_field(fields[i])
		
		res[4*i,:] 		= arr[:,0]
		res[4*i+1,:] 	= arr[:,1] 
		res[4*i+2,:]	= arr[:,2]
		res[4*i+3,:]	= arr[:,3]
	
	path = "world-separation-%.0f%s.npy" % (sep, suffix)
	print "Saving"
	np.save(path, res)
	print "Compressing"
	call(['gzip', path])
	
	return res

#Entry point for parallel_container
def start_job(env):
	#Start main program
	global suffix, threads, nodes, me
	
	if env["multinode"]:
		threads = 1
		nodes = env["num_nodes"]
		me = env["node_number"]
		suffix = env["suffix"]
		
		print "Computing with %d nodes, I'm %d" % (nodes, me)
		
	else:
		threads = env["cores"]
		
		if threads < 1:
			threads = 1
			
	
		print "One node assigned, using %d threads" % threads
		
	seps = [10.]
	
	for sep in seps:
		print "**Starting Separation %f**" % sep
		
		run_for_sep(sep)
		
		print "**Finished Separation %f**" % sep

suffix = ""
threads = 1
nodes = 1
me = 0