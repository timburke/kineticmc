#figure1.py
#Test our monte carlo code by reproducing the graph in Peumanns 2004 Monte Carlo Paper

import matplotlib as plt
import numpy as np
import kineticmc as mc

c = mc.SimulationController()
params = mc.SimulationParameters()

params.field_type = mc.kIsotropic
params.dialectric_const = 4.0
params.lattice_const = 1.0
params.temp = 300
params.recomb_life = 1.0
params.elec_mob = 1.0
params.hole_mob = 1.0
params.trials = 10000

def run_sim(sep, field):
	global c, params
	
	params.field = field
	params.initial_r = sep
	
	c.initialize(params)
	
	fields = np.logspace(3,7,30)
	
	for i in xrange(0,30):
		c.run(2) #two threads
		