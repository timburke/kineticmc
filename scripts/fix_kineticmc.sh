#!/bin/bash

LIB=/Users/timburke/anaconda/python.app/Contents/lib/python2.7/site-packages/_kineticmc.so

install_name_tool -change libvtkCommon.5.10.dylib /usr/local/lib/vtk-5.10/libvtkCommon.5.10.dylib $LIB
install_name_tool -change libvtkImaging.5.10.dylib /usr/local/lib/vtk-5.10/libvtkImaging.5.10.dylib $LIB
install_name_tool -change libvtkFiltering.5.10.dylib /usr/local/lib/vtk-5.10/libvtkFiltering.5.10.dylib $LIB
install_name_tool -change libvtkGraphics.5.10.dylib /usr/local/lib/vtk-5.10/libvtkGraphics.5.10.dylib $LIB
install_name_tool -change libvtkIO.5.10.dylib /usr/local/lib/vtk-5.10/libvtkIO.5.10.dylib $LIB
install_name_tool -change libvtksys.5.10.dylib /usr/local/lib/vtk-5.10/libvtksys.5.10.dylib $LIB