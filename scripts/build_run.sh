#!/bin/bash

dir="/Users/timburke/Documents/Graduate School/Research/Monte Carlo/kineticmc/runs"

read -e -p 'Enter the run name: ' run
read -e -p 'How many slots? ' slots
read -e -p 'Run title: ' title

out=${dir}/${run}
echo ${out}

cp -r "${dir}/skeleton" "${out}"

sed -i '' "s/FOLDERNAME/${run}/g" "${out}/submit.sh"
sed -i '' "s/SLOTS/${slots}/g" "${out}/submit.sh"
sed -i '' "s/TITLE/${title}/g" "${out}/config.json"