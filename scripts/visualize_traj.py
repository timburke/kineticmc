#!/usr/bin/python
#Save a trajectory to an xyz file for visualization

import kineticmc as mc
from subprocess import call
import numpy as np
import os
import sys

c = mc.SimulationController()
params = mc.SimulationParameters()

params.field_type = mc.kIsotropic
params.dialectric_const = 4.0
params.lattice_const = 1.0
params.temp = 300
params.recomb_life = 1.0
params.elec_mob = 1.0
params.hole_mob = 1.0
params.trials = 1
params.field = 5e5
params.initial_r = 40.0

#set the world generator (Change these lines to change what type of world is created
p3ht = mc.PureMaterialSpec(-4.72, -3.09)
pcbm = mc.PureMaterialSpec(-5.98, -3.77)
params.world_spec = p3ht

if len(sys.argv) != 5:
	print "usage visualize_traj <separation> <field> <animate> <outfile>"
	sys.exit(1)
	
params.field = float(sys.argv[2])
params.initial_r = float(sys.argv[1])
	
c.initialize(params)
c.run(1)
c.visualize_last_trace(sys.argv[4], sys.argv[3]=='true')