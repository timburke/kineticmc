#growlannounce.py
#Parse an email from the barley cluster and turn it into a growl announcement
#containing the same information

import re
from subprocess import call
import os

path = "/tmp/message.txt"

data = {}

with open(path, "r") as f:
	#Parse the reason for this message
	reason = f.readline()
	reason.rstrip()
	r_ex = re.compile("Job ([0-9]+) \((.*)\) ([a-zA-Z]+)")
	
	match = re.match(r_ex, reason)
	data["Job"] = match.group(1)
	data["Name"] = match.group(2)
	data["Action"] = match.group(3)
	
	expr = re.compile("([a-zA-Z]+(?:[ ][a-zA-Z]+)?).*= (.*)")
	
	for line in f:
		line.rstrip()
		match = re.match(expr, line)
		
		if match is None:
			print "Could not match line"
			print line
		else:
			data[match.group(1)] = match.group(2)

if data["Action"] == "Started":
	date = data["Start Time"]
elif data["Action"] == "Complete":
	date = data["End Time"]
	
message = "'Job %s at %s'" % (data["Action"], date)
title = "'Job %s (%s) %s'" % (data["Job"], data["Name"], data["Action"])

cmd = ['/usr/local/bin/growlnotify', "-m", message, title]

call(" ".join(cmd), shell=True)