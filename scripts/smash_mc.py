import numpy as np
import itertools
import os.path

def load_file(self, filename):
	"""
	Load a numpy array from the file with ungzipping if required
	"""

	(base, ext) = os.path.splitext(filename)
	if ext == ".gz":
		file = gzip.open(filename, "rb")
	else:
		file = open(filename, "rb")

	arr = np.load(file)

	file.close()

	return arr

#parameters
elec_x = [0, 1, 2, 3, 4, 5,6,7,8,9,10]
elec_y = [-10,-9,-8,-7,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9,10]
hole_x = [-1, -2, -3, -4,-5,-6,-7,-8,-9,-10,-11]
hole_y = [-10,-9,-8,-7,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9,10]

if len(sys.argv) < 3:
	print "Usage: smash_mc.py <input dir> <output file>"
	sys.exit(1)
	
indir = sys.argv[1]
outfile = sys.argv[2]

format = "run-%f-%f-%f-%f"
ext = ".npy.gz"
numnodes = 4

#build output
length = len(elec_x)*len(elec_y)*len(hole_x)*len(hole_y)
width = 50
depth = 7 #splitting, recomb average max sep
trials = 100000

res = np.ndarray([length,width,depth,numnodes])

i = 0

for comb in itertools.product(elec_x, elec_y, hole_x, hole_y):
	pathbase = os.path.join(indir, format % (comb[0], comb[1], comb[2], comb[3]))

	for n in xrange(0,numnodes):
		path = pathbase + "-%d" % n + ext
		
		#now we're iterating over every file
		arr = load_file(path)
		
		#now iterate over each field
		for f in xrange(0, 50):
			frow = f*4
			mhrow = f*4 + 1
			merow = f*4 + 2
			msrow = f*4 + 3
			
			fate = np.mean(arr[frow,:])
			mhdist = np.mean(arr[mhrow,arr[frow,:]>0.5])
			stdhdist = np.std(arr[mhrow,arr[frow,:]>0.5])
			
			medist = np.mean(arr[merow,arr[frow,:]>0.5])
			stdedist = np.std(arr[merow,arr[frow,:]>0.5])
			
			msep  = np.mean(arr[msrow,arr[frow,:]>0.5])
			stdsep = np.std(arr[msrow,arr[frow,:]>0.5])
		
			res[i, f, :, n] = [fate, mhdist, medist, msep, stdhdist, stdedist, stdsep]
	
	i += 1
	
	if i % 1000 == 0:
		print "Done with %d combinations of %d" % (i, length)
		
np.save(outfile, res)