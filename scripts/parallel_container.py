#!/usr/bin/python
#parallel_container.py
#Gather information about our environment and the process that we're trying to invoke
#then present that information in a unified fashion to the underlying process and call it

import os
import argparse
import json
import kineticmc
import kmc_config

def parse_parameter(key, configname, args, config):
	"""
	Combine command line and config argument with the command line args overriding the config ones
	"""
	
	#check if it was specified on the command line
	if getattr(args, key) is not None:
		return getattr(args, key)
	
	#otherwise get it from config file
	if configname in config:
		return config[configname]
	else:
		raise KeyError
	
##BEGIN ACTUAL SCRIPT
log = kineticmc.Logger()
log.configure_file("log.conf")

#Parse the arguments we need
parser = argparse.ArgumentParser(description='Provide a generic way to run a process in a parallel environment.')
	
parser.add_argument('--env', '-e', dest='env', type=str, action='store', required=False,
					choices = ['local', 'barley', 'corn'],
					help="The environment that we're running on.  One of (local, corn, barley)")

parser.add_argument('--module', '-m', dest='module', type=str, required=False,
					help="The python file that will be called by this script")
										
parser.add_argument('--dir', '-d', type=str, dest="dir", required=False,
					help="The directory that the program should be run in.  Note that we load the program before changing directories")

parser.add_argument('--cores', '-c', type=int, dest='cores', required=False,
					help="Explicitly set the number of cores.  This is ignored on barley since we get that information from the grid engine.")

parser.add_argument('--mpi', action='store_const', const=True, dest="mpi", required=False,
					help="Will this process be run from multiple nodes or as multiple cores on a single node.")

parser.add_argument('--config', dest='config', default=None, type=str, required=False, help="A config file specifying the arguments to parallel container")
					
cmd_args = parser.parse_args()

log.debug("Command Line Arguments: %s" % str(cmd_args))

args = lambda x: x #functions can have arbitrary attributes, unlike some other objects
params = {} #other params in the config file are passed on to the called module

#If there's a config file, read in those arguments and combine them, with command line args overriding when there's conflict
#Also allow any other parameters set in the config file to be passed on to the executing code
env = {}

if cmd_args.config is not None:
	with open(cmd_args.config, "r") as configfile:
		config = json.load(configfile)
	
	#combine all the args
	known_opts = {	"dir": 		"directory",
					"mpi": 		"mpi",
					"cores":	"cores",
					"env":		"destination",
					"module":	"module"
				 }
	
	for key, val in known_opts.iteritems():		
		try:
			p = parse_parameter(key, val, cmd_args, config)
			setattr(args, key, p)
		except KeyError:
			#If the key is not set in either, don't worry about it.
			pass
	
	if "params" in config:
		params = config["params"]
	
	env["config"] = kmc_config.MonteCarloConfigurator(cmd_args.config) #pass on the configurator object to our subprocess
else:
	log.info("Running without config file.")
	args = cmd_args

env["dir"] = args.dir
env["params"] = params

if args.env != "barley" and args.cores > 0:
	env["cores"] = args.cores

env["multinode"] = False

if args.env == "barley":
	#load number of procs 
	slots =  int(os.environ["NSLOTS"])
	
	if args.mpi:
		env["nodes"] = slots
		env["multinode"] = True
		
		from mpi4py import MPI
		
		comm = MPI.COMM_WORLD
		env["num_nodes"] = comm.Get_size()
		env["node_number"] = comm.Get_rank()
		env["suffix"] = "-" + str(env["node_number"])
		
		#Make sure all logger messages are marked with our MPI number
		log.push_ctx("MPI %d/%d" % (comm.Get_rank()+1, comm.Get_size()));

		if env["node_number"] == 0:
			log.info("Starting MPI Enabled job (as master)")
			log.info("%d nodes are allocated for it" % env["num_nodes"])
	else:
		env["cores"] = slots
		
log.debug("Environment Vars: %s" % str(env))

os.chdir(env["dir"])
proc = __import__(args.module, fromlist=[])

proc.start_job(env=env)