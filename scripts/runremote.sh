#!/bin/bash
# runremote.sh
# usage: runremote.sh localscript arg1 arg2 ...

realscript=$1
shift 1

declare -a args

count=0
for arg in "$@"; do
  args[count]=$(printf '%q' "$arg")
  count=$((count+1))
done

ssh corn 'cat | tcsh /dev/stdin' "${args[@]}" < "$realscript"