#!/usr/bin/python
#run_cluster.py
#A script to automatically run a script driving the kineticmc python module

import os
import argparse
import random
import os.path
import re
from subprocess import call, check_output

def prepare_remote_dir(path, name=None):
	if name is None:
		name = "run-%05d" % random.randint(0,99999)
	
	cmdformat = ("#!/usr/bin/tcsh\n"
			"cd %s\n"
			"mkdir %s\n"
			"mkdir %s/%s\n")
			
	cmd = cmdformat % (path, name, name, "data")
	
	file = open("/tmp/prep-script.sh", "w")
	
	file.write(cmd)
	file.close()
	
	call("ssh corn < /tmp/prep-script.sh > /dev/null 2>&1", shell=True)
	
	return os.path.join(path, name)
	
def submit_job(working_dir, script):
	
	cmdformat = ("#!/usr/bin/tcsh\n"
				 "cd %s\n"
				 "qsub %s\n")
	
	cmd = cmdformat % (dir, script)
	
	file = open("/tmp/submit-script.sh", "w")
	
	file.write(cmd)
	file.close()
	
	out = check_output("ssh corn < /tmp/submit-script.sh 2>/dev/null", shell=True)
	
	expr = re.compile('Your job ([0-9]+) ')
	
	match = re.search(expr, out)
	if match is None:
		print "Could not extract a job number, something must have gone wrong.  The output follows."
		print out
	else:
		job = match.group(1)
		print "Request was assigned job number %s" % job

def build_script(**kw):
	path = "/tmp/cluster-script.py"
	outfile = open(path, "w")
	
	print >>outfile, "#!/usr/bin/tcsh"
	
	print >>outfile, "#$-cwd"
	print >>outfile, "#$-o stdout.txt"
	print >>outfile, "#$-e stderr.txt"
	print >>outfile, "#$-m abe"
	print >>outfile, "#$-M timburke@stanford.edu"
	
	#set up parameters
	if "cores" in kw and kw["mpi"] == False:
		print >>outfile, "#$-pe fah %d" % kw["cores"]
	elif "cores" in kw:
		print >>outfile, "#$-pe orte %d" % kw["cores"]

	if "name" in kw and kw["name"] is not None:
		print >>outfile, "#$-N %s" % kw['name']
		
	print >>outfile, "#$-l mem_free=3G"
	print >>outfile, "setenv LD_LIBRARY_PATH /afs/ir.stanford.edu/users/t/i/timburke/support/lib/vtk-5.10:/afs/ir.stanford.edu/users/t/i/timburke/support/kineticmath/lib:/afs/ir.stanford.edu/users/t/i/timburke/support/lib"
	
	if kw["mpi"] == False:
		print >>outfile, "python %s/%s --env barley --module batch_payload --dir data -c $NSLOTS" % (kw["dir"], "parallel_container.py")
	else:
		print >>outfile, "mpirun -np $NSLOTS python %s/%s --env barley --module batch_payload --dir data --mpi %s" % (kw["dir"], "parallel_container.py", " ".join(kw["args"]))
	
	outfile.close()

	return path

#constants
container = '/Users/timburke/Documents/Graduate\ School/Research/Monte\ Carlo/kineticmc/scripts/parallel_container.py'
#end constants


parser = argparse.ArgumentParser(description='Run the specified local script file as a batch job on the stanford barley cluster.')
	
parser.add_argument('--slots', '-s', dest='cores', type=int, action='store', default=1, 
					help="The number of slots to request.  These will be shared memory unless --mpi is passed")

parser.add_argument('script',type=str,
					help="The python file that will be called by the qsub script")
					
parser.add_argument("--file", "-f", action="append",
					help = "If there are other files that need to be copied in order to support this batch job, list them here.")
					
parser.add_argument('--name', '-n', type=str, dest="name", default=None,
					help="The batch job name to be given to qsub")

parser.add_argument('--mpi', default=False, action='store_const', const=True, dest="mpi",
					help="Will this process be run from multiple nodes or as multiple cores on a single node.")

parser.add_argument('args', type=str, nargs="*", 
					help="Arguments to be passed to the script upon execution")

parser.add_argument('--submit', default=False, action='store_const', const=True,dest="submit",
					help="Also submit this job to the barley cluster rather than just uploading it to corn.")
										
args = parser.parse_args()

if args.mpi:
	print "MPI Job"
	print "Requesting %d nodes" % args.cores
else:
	print "Single Node Job"
	print "Requesting %d cores" % args.cores

#print "Creating remote directories"
dir = prepare_remote_dir("/farmshare/user_data/timburke/batch", args.name)
print "Created: %s" % dir

#print "Packaging local script: %s" % args.script
#print "Arguments will be: '%s'" % " ".join(args.args)
path = build_script(cores=args.cores, script=args.script, name=args.name, args=args.args, dir=dir, mpi=args.mpi)

#print "Sending files"
call("scp %s corn:%s/batch-script.sh > /dev/null 2>&1" % (path, dir), shell=True)
call("scp %s corn:%s/batch_payload.py > /dev/null 2>&1" % (args.script, dir), shell=True)
call("scp %s corn:%s/parallel_container.py > /dev/null 2>&1" % (container, dir), shell=True)

for file in args.file:
	filename = os.path.basename(file)
	call("scp %s corn:%s/%s > /dev/null 2>&1" % (file, dir, filename), shell=True)

if args.submit:
	print "Submitting job"
	submit_job(dir, "batch-script.sh")
	if args.name is not None:
		print "Job name is %s" % args.name
else:
	print "Not submitting job to cluster"