#include "control-sim.h"
#include "kinetic-mc.h"

//Result Types that we know about
#include "FateResult.h"
#include "DistanceResult.h"
#include "LogTrajectory.h"
#include "CollisionsResult.h"
#include "LogTimeTrace.h"
#include "FirstMoveResult.h"
#include "ElapsedTime.h"
#include "PureMaterialGenerator.h"
#include "VirtualWorld.h"
#include "PreallocatedWorld.h"
#include "SparseWorld.h"
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <ctime>

#include "CachedEnergy.h"
#include "ContextFreeEnergy.h"
#include "PreallocatedEnergy.h"

SimulationController::SimulationController() : calc_sim(NULL), verbose(false), node_number(-1), next_id(0), results(NULL)
{
	add_result_type("Fate", new TemplatedFactory<FateResult>(this));
	add_result_type("Distance", new TemplatedFactory<DistanceResult>(this));
	add_result_type("LogTrajectory", new TemplatedFactory<LogTrajectory>(this));
	add_result_type("LogTimeTrace", new TemplatedFactory<LogTimeTrace>(this));
	add_result_type("Collisions", new TemplatedFactory<CollisionsResult>(this));
	add_result_type("FirstMove", new TemplatedFactory<FirstMoveResult>(this));
	add_result_type("ElapsedTime", new TemplatedFactory<ElapsedTimeResult>(this));
	
	srand48(time(NULL));  //Make sure the random seed is initialized
	
	pthread_mutex_init(&trace_mutex, NULL);
	pthread_mutex_init(&id_mutex, NULL);
	
	perf_l = log4cxx::Logger::getLogger("kineticmc.sim.perf");
}

SimulationController::~SimulationController()
{
	clear_sims();
	clean_results();
	
	if (calc_sim)
		delete calc_sim;
}

void SimulationController::clear_sims()
{
	for (size_t i=0; i<sims.size(); ++i)
		if (sims[i] != NULL)
			delete sims[i];
			
	sims.clear();
	threads.clear();
}

void SimulationController::clean_results()
{
	for (std::map<std::string, FunctorFactory*>::iterator it = result_types.begin(); it != result_types.end(); ++it)
	{
		if (it->second != NULL)
		{
			delete it->second;
			it->second = NULL;
		}
	}
	
	for (std::list<std::pair<ResultCondition, ResultFunctor*> >::iterator it = tracked_results.begin(); it != tracked_results.end(); ++it)
	{
		if (it->second != NULL)
		{
			delete it->second;
			it->second = NULL;
		}
	}
}

size_t SimulationController::calculate_result_width()
{
	size_t width = 0;
	
	for (std::list<std::pair<ResultCondition, ResultFunctor*> >::iterator it = tracked_results.begin(); it != tracked_results.end(); ++it)
		width += it->second->num_values();
	
	return width;
}
	
void SimulationController::initialize(SimulationParameters &p)
{
	params = p;
	
	//Log parameters
	log4cxx::LoggerPtr l = log4cxx::Logger::getLogger("kineticmc.sim.initial");
	
	LOG_TRACE(l, "World Spec: " << params.world_spec);
	LOG_TRACE(l, "Hole Mobility: " << params.hole_mob);
	LOG_TRACE(l, "Electron Mobility: " << params.elec_mob);
}

void SimulationController::save_trace(std::vector<CarrierPositions> &trace)
{
	pthread_mutex_lock(&trace_mutex);
	
	last_trace = trace;
	
	pthread_mutex_unlock(&trace_mutex);
}

void SimulationController::preallocate_world(KMCInterface *sim, std::vector<int> &sides, real_t lattice, WorldGenerator *gen)
{
	coord_t min_pos[3];
	coord_t max_pos[3];
	coord_t loc[3];

	for (size_t i=0; i<3; ++i)
	{
		min_pos[i] = (coord_t)(-1 * (sides[i]/2));
		max_pos[i] = min_pos[i] + sides[i];
	}

	printf("Preallocating world with sides (%d, %d, %d)\n", sides[0], sides[1], sides[2]);
	printf("World Bounds\n");
	printf("x: (%d, %d)\n", min_pos[0],max_pos[0]);
	printf("y: (%d, %d)\n", min_pos[1],max_pos[1]);
	printf("z: (%d, %d)\n", min_pos[2],max_pos[2]);

	PreallocatedWorld *world = new PreallocatedWorld(sides[0], sides[1], sides[2]);

	for (loc[0]=min_pos[0]; loc[0]<max_pos[0]; ++loc[0])
	{
		for (loc[1]=min_pos[1]; loc[1]<max_pos[1]; ++loc[1])
		{
			for (loc[2]=min_pos[2]; loc[2]<max_pos[2]; ++loc[2])
				world->set_value(loc, gen->generate(loc, lattice, world), kElectron);
		}
	}

	sim->set_world(world);
}

void SimulationController::build_sim(SimulationParameters &p, KMCInterface **out)
{
	KMCInterface *sim;
	WorldGenerator *gen = p.world_spec->create();
	
	//Build the world and choose the right optimized MCSimulation version for it
	if (gen->context_free())
	{
		sim = new MCSimulation<ContextFreeEnergy, ContextFreeEnergy>();
		sim->set_world(new VirtualWorld());
	}
	else if (p.preallocate)
	{
		sim = new MCSimulation<PreallocatedEnergy, PreallocatedEnergy>();
		preallocate_world(sim, p.periodic_size, p.lattice_const, gen);
	}
	else
	{
		sim = new MCSimulation<CachedEnergy, CachedEnergy>();
		set_world(sim, 50, 50*16);
	}
	
	sim->set_world_generator(gen);
	sim->initialize_simulation(p.lattice_const, p.initial_r, p.dialectric_const, p.temp);
	sim->set_carrier_params(p.elec_mob->create(), p.hole_mob->create(), p.recomb_life); //elec mob, hole mob, recomb time	
	sim->calc_with_simd(p.use_simd);
	
	*out = sim;
}

void SimulationController::create_sim_thread(unsigned long trials)
{	
	//Create sim, initialize it, add it to the list and then run it in a thread
	SimEnvironment *env = new SimEnvironment();
	
	if (verbose)
	{
		printf("Creating thread\n");
		printf("Lattice Const: %f\n", params.lattice_const);
		printf("Initial Separation: %f\n", params.initial_r);
		printf("Dialectric Constant: %f\n", params.dialectric_const);
		printf("Temperature: %f\n", params.temp);
	}
		
	build_sim(params, &env->sim); 
	
	env->trials = trials;
	env->params = params;
	env->master = this;
	
	//Allocate space for the results
	env->results = new double[calculate_result_width()*trials];
	
	sims.push_back(env);
	
	pthread_t thread;
	
	//TODO: check this for errors
	pthread_create(&thread, NULL, &SimulationController::run_sim, (void*)env);
	threads.push_back(thread);
}

void SimulationController::set_world(KMCInterface *sim, coord_t block, coord_t side)
{
	coord_t start[3];
	
	for (size_t i=0;i<3;++i)
		start[i] = -1*side/2;
	
	sim->set_world(new SparseWorld(start, block, side));
}

void SimulationController::visualize_world(const std::string &filename, size_t side)
{
	SimEnvironment env;
	
	env.sim->initialize_simulation(params.lattice_const, params.initial_r, params.dialectric_const, params.temp);
	env.sim->set_carrier_params(params.elec_mob->create(), params.hole_mob->create(), params.recomb_life); //elec mob, hole mob, recomb time	
	
	env.sim->set_world_generator(params.world_spec->create());
	
	//Build the world
	set_world(env.sim, 50, 50*32);
	
	env.sim->visualize_world(filename, side);
}

void SimulationController::visualize_trace(const std::string &filename, std::vector<CarrierPositions> & trace, bool animation)
{
	pthread_mutex_lock(&trace_mutex);

	FILE *f = fopen(filename.c_str(), "w");
	
	if (!f)
	{
		printf("Could not open file %s\n", filename.c_str());
		return;
	}
	
	if (animation == false)
	{
		fprintf(f, "%lu\n", trace.size());
		fprintf(f, "Electron and hole trajectories\n");
	}
	
	for (size_t i=0; i<trace.size(); ++i)
	{
		if (animation)
		{
			fprintf(f, "2\n");
			fprintf(f, "Frame %lu\n", i+1);
		}
		
		CarrierPositions &p = trace[i];
		
		fprintf(f, "E %.6f %.6f %.6f\n", p.elec_pos[0]*params.lattice_const, p.elec_pos[1]*params.lattice_const, p.elec_pos[2]*params.lattice_const);
		fprintf(f, "H %.6f %.6f %.6f\n", p.hole_pos[0]*params.lattice_const, p.hole_pos[1]*params.lattice_const, p.hole_pos[2]*params.lattice_const);
	}
	
	fclose(f);

	pthread_mutex_unlock(&trace_mutex);
}
		
		
void* SimulationController::run_sim(void *envP)
{
	SimEnvironment *env = (SimEnvironment*)envP;
	
	size_t width = env->master->calculate_result_width();
	
	for (unsigned long i=0; i<env->trials; ++i)
	{
		env->sim->set_field(env->params.field*1e2, env->params.field_type); //Multiply by 1e2 to convert from V/cm to V/m
		
		//If we're creating states just separated by a specific distance, do that
		if (env->params.initial_type == kSpecificDistance)
			env->sim->create_initial_state_bulk();
		else
		{
			coord_t elec[3];
			coord_t hole[3];
			
			for (size_t i=0; i<3; ++i)
			{
				elec[i] = env->params.initial_elec[i];
				hole[i] = env->params.initial_hole[i];
			}
			
			env->sim->create_state(elec, hole);
		}
				
		//Check if we're running this using the vectorized code
		if (env->params.use_simd)
			env->sim->run_simd(1000000, true);
		else
			env->sim->run(1000000, true);
		
		//Now analyze the run and pull out the data
		size_t j=0;
		for (std::list<std::pair<ResultCondition, ResultFunctor*> >::iterator it = env->master->tracked_results.begin(); it != env->master->tracked_results.end(); ++it)
		{			
			ResultCondition cond = it->first;
			bool 			log_result = false;
			
			switch (env->sim->get_results()->result)
			{
				case kEscaped:
				if (cond & kEscapedCondition)
					log_result = true;
				break;
				
				case kRecombined:
				if (cond & kRecombinedCondition)
					log_result = true;
				break;
				
				case kTimeExpired:
				if (cond & kTimeExpiredCondition)
					log_result = true;
				break;
			}
			
			if (log_result)
			{
				it->second->operator()(env->sim->get_results(), env->params, &(env->results[width*i + j]));
				j += it->second->num_values(); //Advance the offset by the number of values this result outputs
			}
		}
		
		//Now see if we should log this trace.
		//Provide support for logging the first X of each kind of trajectory.
		bool log_trace = false;
		const char *prefix = "";
		
		if (env->sim->get_results()->result == kEscaped && env->params.num_escaped_traj > 0)
		{
			--env->params.num_escaped_traj;
			prefix = "escape";
			log_trace = true;
		}
		else if (env->sim->get_results()->result == kRecombined && env->params.num_recomb_traj > 0)
		{
			--env->params.num_recomb_traj;
			prefix = "recomb";
			log_trace = true;
		}
		else if (env->sim->get_results()->result == kTimeExpired && env->params.num_timeout_traj > 0)
		{
			--env->params.num_timeout_traj;
			prefix = "timeout";
			log_trace = true;
		}
		
		if (log_trace)
		{
			std::string name = std::string(prefix) + std::string("-traj") + env->master->unique_id();
			
			env->master->visualize_trace(name, env->sim->get_results()->trace, true);
		}
	}
		
	return NULL;
}

void SimulationController::run(size_t num_threads)
{
	clock_t start_t, end_t;
	
	clear_sims();
	
	//Calculate trials per thread
	unsigned long trials_per = params.trials / num_threads;
	unsigned long extra_trials = params.trials % num_threads;
	
	start_t = clock();
	
	for (size_t i=0; i<num_threads;++i)
	{
		unsigned long trials = trials_per;
		
		if (i == 0)
			trials += extra_trials;
		
		create_sim_thread(trials);
	}
	
	//Now wait for all the threads to finish
	for (size_t i=0; i<threads.size(); ++i)
		pthread_join(threads[i], NULL);
	
	end_t = clock();
	
	float elapsed = ((float)(end_t - start_t)) / CLOCKS_PER_SEC;
	
	LOG_INFO(perf_l, "Trials per second: " << (float)params.trials / elapsed);
	
	//Now combine the results and clear the sims
	size_t width = calculate_result_width();
	
	if (results)
	{
		delete[] results;
		results = NULL;
	}
	
	results = new double[width*params.trials];
	results_l = params.trials;
	results_w = width;
	
	size_t i = 0;
	
	for (size_t sim=0; sim < num_threads; ++sim)
	{
		double *res = sims[sim]->results;
		size_t offset = i;
		
		for (size_t trial=0; trial < sims[sim]->trials; ++trial)
		{
			for (size_t j=0; j<width; ++j)
			{
				results[trial*width+j+offset] = res[trial*width+j];
				++i;
			}
		}
	}		
	
	clear_sims();
}

void SimulationController::add_result_type(const std::string &name, FunctorFactory *factory)
{
	result_types[name] = factory;
}

void SimulationController::track_result(const std::string &name, ResultCondition cond)
{
	if (result_types.find(name) == result_types.end())
	{
		printf("Could not find result type named %s.\n", name.c_str());
		return;
	}
	
	tracked_results.push_back(std::pair<ResultCondition, ResultFunctor *>(cond, result_types[name]->create()));
}

/*
this function is for use with python via swig.  It should not be called directly by C++ code
*/

void SimulationController::get_results(double *n, int n_dim)
{
	size_t l = (size_t)n_dim;
	
	if (l == results_l*results_w)
		memcpy(n, results, sizeof(double)*results_l*results_w);
	else
	{
		size_t wanted = results_l*results_w;
		printf("Sizes did not match in get_results(...).  Wanted %lu and got %lu.  Doing nothing.\n", wanted, l);
	}
}

unsigned int SimulationController::get_result_columns(const std::string &name)
{
	if (result_types.find(name) == result_types.end())
	{
		printf("Could not find result type named %s.\n", name.c_str());
		return 0;
	}
	
	ResultFunctor *res = result_types[name]->create();
	
	unsigned int cols = res->num_values();
	
	delete res;
	
	return cols;
}

std::vector<std::string> SimulationController::get_result_column_names(const std::string &name)
{
	if (result_types.find(name) == result_types.end())
	{
		printf("Could not find result type named %s.\n", name.c_str());
		return std::vector<std::string>();
	}
	
	ResultFunctor *res = result_types[name]->create();
	
	std::vector<std::string> colnames = res->column_names();
	
	delete res;
	
	return colnames;
}

int SimulationController::get_result_width()
{
	return (int)results_w;
}

int SimulationController::get_result_length()
{
	return (int)results_l;
}

void SimulationController::set_node_number(int number)
{
	node_number = number;
}


/*
 * Returns a unique id even if we're multithreaded and running on multiple machines using mpi.
 */

std::string SimulationController::unique_id()
{
	char buff[256];
	
	pthread_mutex_lock(&id_mutex);
	
	if (node_number > -1)
		sprintf(buff, "%d-%d", node_number, next_id);
	else
		sprintf(buff, "%d", next_id);
		
	++next_id;
	
	pthread_mutex_unlock(&id_mutex);
	
	return std::string(buff);
}

void SimulationController::create_calc_sim(SimulationParameters &p)
{
	if (calc_sim)
	{
		delete calc_sim;
		calc_sim = NULL;
	}
	
	build_sim(p, &calc_sim);
}
	
void SimulationController::set_calc_field(std::vector<real_t> field)
{
	real_t f[3];
	
	for (size_t i=0; i<3; ++i)
		f[i] = field[i];
	
	if (!calc_sim)
		return;
	
	calc_sim->set_field(f);

}

void SimulationController::set_calc_carrier_pos_c(coord_t *elec, coord_t *hole)
{
	if (!calc_sim)
		return;
	
	calc_sim->create_state(elec, hole);
}

void SimulationController::set_calc_carrier_pos(std::vector<int32_t> elec, std::vector<int32_t> hole)
{
	coord_t e[3];
	coord_t h[3];
	
	for (size_t i=0; i<3; ++i)
	{
		e[i] = elec[i];
		h[i] = hole[i];
	}
	
	set_calc_carrier_pos_c(e, h);
}

void SimulationController::calc_hopping_rates(real_t *n, int n_dim)
{
	if (!calc_sim || n_dim != 12)
		return;
	
	std::vector<real_t> rates = calc_sim->calc_hopping_rates();
	
	for (size_t i=0; i<12; ++i)
		n[i] = rates[i];
}

void SimulationController::calc_xy_world_slice(int side, real_t *n, int n_dim)
{
	if (!calc_sim)
		return;

	calc_sim->fill_xy_world_slice(side, n);
}