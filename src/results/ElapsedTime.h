//ElapsedTime.h

#ifndef __ElapsedTime_h__
#define __ElapsedTime_h__

#include <vector>
#include <math.h>
#include "Types.h"
#include <string.h>
#include <stdlib.h>

class SimulationController;

/*
 * Record the total time elapsed in the simulation according to the KMC
 * waiting time algorithm.
 */

class ElapsedTimeResult : public ResultFunctor
{
	private:
		
	public:
	ElapsedTimeResult(SimulationController *m) {};

	virtual std::string name() {return std::string("Elapsed Simulation Time");};

	virtual std::vector<std::string> column_names()
	{
		std::vector<std::string> names;
		
		names.resize(1);
		names[0] = std::string("Elapsed Time");
		
		return names;
	}

	virtual void operator()(SimulationData *data, SimulationParameters &params, double *out)
	{
		double total = 0.0;

		for (std::vector<float>::iterator it = data->delays.begin(); it != data->delays.end(); ++it)
			total += *it;

		*out = total;
	}
};

#endif