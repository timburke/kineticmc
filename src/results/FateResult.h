//FateResult.h

#ifndef __FateResult_h__
#define __FateResult_h__

#include "ResultFunctor.h"

/*
 * 	Escaped = 0,
 *	Recombined = 1,
 *	TimeExpired = 2
 */

class SimulationController;

class FateResult : public ResultFunctor
{
	private:
	
	public:
	FateResult(SimulationController *m) {};
	
	virtual std::string name() {return std::string("Geminate Pair Fate");};
	
	virtual std::vector<std::string> column_names()
	{
		std::vector<std::string> names;
		
		names.resize(1);
		names[0] = std::string("Simulation Result");
		
		return names;
	}
	
	virtual void operator()(SimulationData *data, SimulationParameters &params, double *out)
	{
		out[0] = (double)data->result;
	}
};

#endif