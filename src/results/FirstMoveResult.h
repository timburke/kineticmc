//FirstMoveResult.h

#ifndef __FirstMoveResult_h__
#define __FirstMoveResult_h__

#include <vector>
#include <math.h>
#include "Types.h"
#include <string.h>
#include <stdlib.h>
#include <cmath>

class SimulationController;

/*
 * FirstMoveResult 
 * Calculate which particle moved first and what direction it moved
 * 
 * 
 */

class FirstMoveResult : public ResultFunctor
{
	private:	
	bool moved(coord_t *p1, coord_t *p2)
	{
		coord_t manhatten = 0;
		
		for (size_t i=0; i<3; ++i)
			manhatten += p1[i]-p2[i]; //Since we only moved in one direction, no need for abs here.
		
		return (manhatten != 0);
	}
	
	void direction(coord_t *p1, coord_t *p2, unsigned int *direction, unsigned int *axis)
	{
		if (p2[0] != p1[0])
		{
			*axis = kXAxis;
			*direction = p2[0] > p1[0]? kPositiveDirection : kNegativeDirection;
		}
		else if (p2[1] != p1[1])
		{
			*axis = kYAxis;
			*direction = p2[1] > p1[1]? kPositiveDirection : kNegativeDirection;
		}
		else
		{
			*axis = kZAxis;
			*direction = p2[2] > p1[2]? kPositiveDirection : kNegativeDirection;
		}
	}
	
	public:
	FirstMoveResult(SimulationController *m) {};
	
	virtual std::string name() {return std::string("First Move");};
	virtual unsigned int num_values() {return 3;};
	
	virtual std::vector<std::string> column_names()
	{
		std::vector<std::string> names;
		
		names.resize(num_values());
		names[0] = std::string("First Jump - Particle");
		names[1] = std::string("First Jump - Axis");
		names[2] = std::string("First Jump - Direction");
		
		return names;
	}
		
	virtual void operator()(SimulationData *data, SimulationParameters &params, double *out)
	{		
		unsigned int axis;
		unsigned int dir;
		
		bool elec_first = moved(data->trace[0].elec_pos, data->trace[1].elec_pos);
		
		if (elec_first)
			direction(data->trace[0].elec_pos, data->trace[1].elec_pos, &dir, &axis);
		else
			direction(data->trace[0].hole_pos, data->trace[1].hole_pos, &dir, &axis);
		
		//Save out the first jump information
		out[0] = elec_first? 0.0: 1.0;
		out[1] = (double)axis;
		out[2] = (double)dir;
	}
};


#endif