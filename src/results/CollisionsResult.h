//CollisionsResult.h.

#ifndef __CollisionsResult_h__
#define __CollisionsResult_h__

#include <vector>
#include <math.h>
#include "Types.h"
#include <string.h>
#include <stdlib.h>
#include <cmath>

class SimulationController;

/*
 * CollisionsResult 
 * Calculate how many times the electron and hole were next to each other and could
 * have potentially recombined.
 * 
 * 
 */
class CollisionsResult : public ResultFunctor
{
	private:	
	bool adjacent(coord_t *e, coord_t *h)
	{
		coord_t manhatten = 0;
		
		for (size_t i=0; i<3; ++i)
			manhatten += abs(e[i]-h[i]);
		
		return (manhatten == 1);
	}
	
	public:
	CollisionsResult(SimulationController *m) {};
	
	virtual std::string name() {return std::string("Potential Collisions");};
	virtual unsigned int num_values() {return 1;};
	
	virtual std::vector<std::string> column_names()
	{
		std::vector<std::string> names;
		
		names.resize(1);
		names[0] = std::string("Potential Collisions");
		
		return names;
	}
		
	virtual void operator()(SimulationData *data, SimulationParameters &params, double *out)
	{		
		size_t num_adj = 0;
		
		for (std::vector<CarrierPositions>::iterator it = data->trace.begin(); it != data->trace.end(); ++it)
		{
			if (adjacent(it->elec_pos, it->hole_pos))
				++num_adj;
		}
		
		//Save out the number of times e/h pair were adjacent
		out[0] = (double)num_adj;
	}
};


#endif 