//DistanceResult.h.

#ifndef __DistanceResult_h__
#define __DistanceResult_h__

#include <vector>
#include <math.h>
#include "Types.h"
#include <string.h>
#include <stdlib.h>

class SimulationController;

/*
 * Record various characteristic values of the distance separating the electron and hole during the simulation
 * Currently calculated 3 things
 * - max separation between electron and hole
 * - max distance traveled by electron from where it started
 * - max distance traveled by hole from where it started
 */
class DistanceResult : public ResultFunctor
{
	private:
	
	coord_t r2(coord_t *p1, coord_t *p2)
	{
		coord_t val = 0;
		
		for (size_t i=0; i<3; ++i)
			val += (p1[i] - p2[i])*(p1[i] - p2[i]);
		
		return val;
	}
	
	public:
	DistanceResult(SimulationController *m) {};
	
	virtual std::string name() {return std::string("Maximum Distance");};
	virtual unsigned int num_values() {return 3;};
	
	virtual std::vector<std::string> column_names()
	{
		std::vector<std::string> names;
		
		names.resize(3);
		names[0] = std::string("Max Electron Distance");
		names[1] = std::string("Max Hole Distance");
		names[2] = std::string("Max Separation");
		
		return names;
	}
		
	virtual void operator()(SimulationData *data, SimulationParameters &params, double *out)
	{
		coord_t max_elec = 0;
		coord_t max_hole = 0;
		coord_t max_sep = 0;
		
		coord_t init_elec[3];
		coord_t init_hole[3];
		
		for (std::vector<CarrierPositions>::iterator it = data->trace.begin(); it != data->trace.end(); ++it)
		{
			if (it == data->trace.begin())
			{
				//Initialize starting positions
				memcpy(init_elec, it->elec_pos, 3*sizeof(coord_t));
				memcpy(init_hole, it->hole_pos, 3*sizeof(coord_t));
			}
			
			coord_t curr_elec = r2(init_elec, it->elec_pos);
			coord_t curr_hole = r2(init_hole, it->hole_pos);
			coord_t curr_sep = r2(it->elec_pos, it->hole_pos);
			
			if (curr_elec > max_elec)
				max_elec = curr_elec;
				
			if (curr_hole > max_hole)
				max_hole = curr_hole;
			
			if (curr_sep > max_sep)
				max_sep = curr_sep;
		}
		
		//Save out the values converted to distances using sqrt and the lattice const
		out[0] = sqrt((double)max_elec)*params.lattice_const;
		out[1] = sqrt((double)max_hole)*params.lattice_const;
		out[2] = sqrt((double)max_sep)*params.lattice_const;
	}
};


#endif 