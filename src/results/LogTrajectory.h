//LogTrajectory.h

#ifndef __LogTrajectory_h__
#define __LogTrajectory_h__

#include "ResultFunctor.h"
#include <string>
#include "XYZHelper.h"

/*
 * 	Escaped = 0,
 *	Recombined = 1,
 *	TimeExpired = 2
 */

class SimulationController;

class LogTrajectory : public ResultFunctor
{
	private:
	SimulationController *master;
	
	public:
	LogTrajectory(SimulationController *m) : master(m) {};
	
	virtual std::string name() {return std::string("Log Pair Trajectory");};
	virtual unsigned int num_values() {return 0;};
	
	virtual void operator()(SimulationData *data, SimulationParameters &params, double *out)
	{
		std::string filename;
		std::string id = master->unique_id();
		
		filename = std::string("traj-") + id + std::string(".xyz");
		
		XYZHelper xyz;
		
		xyz.write_trajectory(filename, data->trace, params.lattice_const, true);
		//data->sim->save_world(world_file);
	}
};

#endif