//LogTimeTrace.h

#ifndef __LogTimeTrace_h__
#define __LogTimeTrace_h__

#include "ResultFunctor.h"
#include <string>
#include "XYZHelper.h"
#include <stdio.h>

/*
 * 	Escaped = 0,
 *	Recombined = 1,
 *	TimeExpired = 2
 */

class SimulationController;

class LogTimeTrace : public ResultFunctor
{
	private:
	SimulationController *master;
	
	public:
	LogTimeTrace(SimulationController *m) : master(m) {};
	
	virtual std::string name() {return std::string("Log Time Trace");};
	virtual unsigned int num_values() {return 0;};
	
	virtual void operator()(SimulationData *data, SimulationParameters &params, double *out)
	{
		std::string filename;
		std::string id = master->unique_id();
		
		filename = std::string("time-") + id + std::string(".txt");
		
		FILE *f = fopen(filename.c_str(), "w");

		for (std::vector<float>::iterator it = data->delays.begin(); it != data->delays.end(); ++it)
			fprintf(f, "%f\n", *it);
		
		fclose(f);
	}
};

#endif