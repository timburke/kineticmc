#include "ChainAnalyzer.h"

ChainAnalyzer::ChainAnalyzer(CSRSparseMatrix<uint32_t, markov_real> *m) : mat(NULL), prefactor(1.0), kT(0.026)
{
	set_matrix(m);
}

ChainAnalyzer::ChainAnalyzer() : mat(NULL), prefactor(1.0), kT(0.026)
{

}

void ChainAnalyzer::set_matrix(CSRSparseMatrix<uint32_t, markov_real> *m)
{
	mat = m;
}

void ChainAnalyzer::set_prefactor(markov_real pre)
{
	prefactor = pre;
}

void ChainAnalyzer::set_temperature(markov_real T)
{
	//kT is always in eV;

	kT = 8.6173324e-5 * T;
}

void ChainAnalyzer::denormalize(DenseVector<markov_real> *v)
{
	if (v == NULL)
		return;

	if (mat == NULL)
		return;

	std::vector<markov_real> &scale = v->get_vector();

	if(scale.size() != (size_t)mat->rows())
		return;

	for(uint32_t i=0; i<scale.size(); ++i)
	{
		uint32_t cols = mat->row_length(i);

		for (uint32_t j = 0; j<cols; ++j)
		{
			double val = mat->row_iter_vals(i, j);

			val /= scale[i];
			mat->set(i,j, val);
		}
	}
}

void ChainAnalyzer::to_hopping_barrier(DenseVector<markov_real> *v)
{
	if (v == NULL)
		return;

	if (mat == NULL)
		return;

	std::vector<markov_real> &scale = v->get_vector();

	if(scale.size() != (size_t)mat->rows())
		return;

	for(uint32_t i=0; i<scale.size(); ++i)
	{
		uint32_t cols = mat->row_length(i);

		for (uint32_t j = 0; j<cols; ++j)
		{
			double val = mat->row_iter_vals(i, j);

			val /= scale[i]*-1.0*prefactor;
			mat->set(i,j, val);
		}
	}
}

void ChainAnalyzer::to_energy_barrier(DenseVector<markov_real> *v)
{
	if (v == NULL)
		return;

	if (mat == NULL)
		return;

	std::vector<markov_real> &scale = v->get_vector();

	if(scale.size() != (size_t)mat->rows())
		return;

	for(uint32_t i=0; i<scale.size(); ++i)
	{
		uint32_t cols = mat->row_length(i);

		for (uint32_t j = 0; j<cols; ++j)
		{
			double val = mat->row_iter_vals(i, j);

			val /= scale[i]*-1.0*prefactor;

			//clear the diagonal entries, which are going to be negative
			if (val <= 0.0)
				mat->set(i,j, 0.0);
			else
			{
				val = log(val)*-1.0*kT;
				mat->set(i,j, val);
			}
		}
	}
}