//MonteCarlo.h

/*
 * Given a CSR matrix specifying transition rates between states, quickly calculate
 * N monte carlo trajectories starting at a given row of the matrix.
 */

#ifndef __MonteCarlo_h__
#define __MonteCarlo_h__

#include <vector>
#include <list>
#include <stdint.h>
#include "TimeTrace.hxx"
#include "MarkovState.h"
#include "CSRSparseMatrix.h"
#include "StateList.h"

#define kTrajectorySize		10*1024*1024
#define kSentinalValue		0xFFFFFFFF

class MatrixMonteCarlo
{
	private:
	StateList 								*mapper;
	DenseVector<markov_real> 				*waits;
	CSRSparseMatrix<uint32_t, markov_real> 	*chain;
	bool									poisson_times;
	
	int										world_size;
	double									max_time;
	double									timescale;
	double									bin_resolution;

	MarkovState								starting_state;

	//Traces
	TimeTrace<int>			hole_disp2;
	TimeTrace<int>			elec_disp2;
	TimeTrace<int>			separation2;
	TimeTrace<int>			absorption;

	uint32_t next_step(uint32_t row);

	int periodic_dist2(MarkovState s1, MarkovState s2, bool elec);
	markov_real get_delay(uint32_t state);
	void trace(double time, uint32_t row);

	public:
	MatrixMonteCarlo();

	void clear();
	void setup_traces(double max_time, double resolution, double scale, int world_size);
	void setup_chain(StateList *mapper, DenseVector<markov_real> *waits, CSRSparseMatrix<uint32_t, markov_real> *matrix);
	void run(uint32_t start);
	void set_poisson(bool poisson);

	//Python callable methods for getting trajectories.
	TimeTrace<int>*			get_hole_disp2();
	TimeTrace<int>*			get_elec_disp2();
	TimeTrace<int>*			get_separation2();
	TimeTrace<int>*			get_absorption();
};

#endif