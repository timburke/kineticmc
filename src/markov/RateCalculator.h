//RateCalculator.h

#ifndef __RateCalculator_h__
#define __RateCalculator_h__

#include "MarkovTypes.h"
#include "PreallocatedWorld.h"
#include "Types.h"
#include "MarkovState.h"
#include "InteractionModel.h"
#include "RateModel.h"
#include <list>

class RateCalculator
{
	private:
	PreallocatedWorld 				*world;
	coord_t			  				bounds[3][2];

	markov_real						lattice_const;
	markov_real						temp;
	markov_real						dielectric;

	std::list<InteractionModel*>	interactions;

	RateModel						*rate_model;

	public:
	RateCalculator();
	virtual ~RateCalculator();

	//Configuration Routines
	void set_bounds(const std::vector<int> &b);
	void set_lattice(markov_real lattice);
	void set_temperature(markov_real temp);
	void set_dielectric(markov_real die);

	void set_world(WorldGenerator *gen);
	void set_ratemodel(RateModel *rm);

	void add_interation(InteractionModel *model);

	//Preparation Routines
	virtual void prepare_calculation();

	//Calculation Routines
	virtual markov_real calculate_energy(MarkovState state);
	virtual markov_real calculate_rate(MarkovState src, MarkovState dst);

	PreallocatedWorld *get_world();


	//Saving Routines
	void save_nrrd(const std::string &filename, ImageType type);
};

#endif