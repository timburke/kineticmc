//MarkovChain.cpp

#include "MarkovChain.h"
#include "BigMarkovStateMapper.h"
#include "BitmapStateMapper.h"
#include "utilities/IndirectCompare.hxx"
#include <stdio.h>

MarkovChain::MarkovChain() : mapper(NULL), num_transient(0), sim(NULL), rc(NULL)
{
	for (size_t i=0; i<3; ++i)
	{
		bounds[i][0] = -128;
		bounds[i][1] = 127;	
	}

	block_size = 2*1024*1024;
	elem_per_block = block_size / sizeof(uint64_t);
}

MarkovChain::~MarkovChain() 
{
	if (mapper)
		delete mapper;
}

void MarkovChain::set_bounds(std::vector<int> x, std::vector<int> y, std::vector<int> z)
{
	for (size_t i=0; i<2; ++i)
	{
		bounds[0][i] = x[i];
		bounds[1][i] = y[i];
		bounds[2][i] = z[i];
	}

	choose_mapper();
}

void MarkovChain::add_transient_triplet(uint64_t row, uint64_t col, double value)
{
	CoordTriplet triplet;

	triplet.row = row;
	triplet.col = col;
	triplet.val = value;
	
	trans.push_back(triplet);
}

void MarkovChain::add_absorbing_triplet(uint64_t row, uint64_t col, double value)
{
	CoordTriplet triplet;

	triplet.row = row;
	triplet.col = col;
	triplet.val = value;
	
	absorb.push_back(triplet);
}

void MarkovChain::load_results_vector(const std::string &file)
{

}

int64_t MarkovChain::map(MarkovState state)
{
	return (int64_t) find_state(state.get_signature());
}

void MarkovChain::set_rate_calculator(RateCalculator *r)
{
	rc = r;
}

double MarkovChain::get_probability(MarkovState initial)
{
	int64_t row = map(initial);

	if (row == -1)
	{
		printf("Invalid initial state that has not been encountered in the chain.\n");
		return 0.0;
	}

	return results[row];
}

void MarkovChain::set_simulator(SimulationController *s)
{
	sim = s;
}

/*
 * Each pruning rule gets a chance to substitute this transition with its own final state.  If that happens
 * stop processing and return that state.  Otherwise return the original ending state.
 */
MarkovState MarkovChain::check_transition(MarkovState start, MarkovState end, double cum_prob, double local_prob)
{
	for (std::vector<ChainPruningRule *>::iterator it=pruning_rules.begin(); it!=pruning_rules.end(); ++it)
	{
		end = (*it)->check_transition(start, end, cum_prob, local_prob);
		if (end.is_absorbing())
			return end;
	}

	return end;
}

void MarkovChain::load_location(MarkovState loc)
{
	coord_t elec[3];
	coord_t hole[3];

	for (size_t i=0; i<3; ++i)
	{
		elec[i] = loc.elec_loc[i];
		hole[i] = loc.hole_loc[i];
	}

	sim->set_calc_carrier_pos_c(elec, hole);
}

/*
 * The state vector is going to be a sorted list of all of the states in this chain
 * we want to be able to efficiently look up the index of a state in this list. Since
 * the list is stored externally and read in by block, we'll cache the first entry in each block
 * so that we can efficiently bound the binary search to just one block and not pay a random
 * access penalty reading in many blocks to look for a state.
 */

void MarkovChain::prepare_state_table()
{
	unsigned mem_to_use = memtune.sorting_mem; 
	size_t num_blocks = (states.size() + (elem_per_block - 1)) / elem_per_block; //integer round up

	printf("Preparing state table (mapping MarkovStates to indices)\n");
	printf("Using %lu blocks.\n", num_blocks);

	//Sort the states vector to be able to efficiently look up state -> index mappings
	stxxl::ksort(states.begin(), states.end(), StateKeyExtractor(), mem_to_use);

	state_blks.resize(num_blocks);

	for (size_t i=0; i<num_blocks; ++i)
	{
		size_t index = i*elem_per_block;
		state_blks[i] = states[index];
	}

	num_transient = states.size();
}

void MarkovChain::print_states()
{
	printf("Printing states table\n");

	for(size_t i=0;i<states.size(); ++i)
		printf("State %lu: %lu\n", i, states[i]);
}

//NB: prepare_state_table must have been called before calling this function
uint64_t MarkovChain::find_state(uint64_t state)
{
	//First find the block that it's in
	//lower_bound returns the first block whose intial element is greater or equal to state
	//state, if it exists will be in the block immediately before that or it will be the first element in the found block.
	size_t block = (std::lower_bound(state_blks.begin(), state_blks.end(), state)-state_blks.begin());

	//printf("Found states %llu in block %llu\n", state, block);

	//If state is lower than all known states, it must not exist.
	if (block == 0 && state < state_blks[0])
		return (uint64_t)-1LL;

	if (state == state_blks[block])
		return block*elem_per_block;

	block--;

	size_t lower = block*elem_per_block;
	size_t upper = lower + elem_per_block - 1;

	if (upper > states.size())
		upper = states.size() - 1;

	while (lower < upper)
	{
		size_t mid = (lower+upper)/2;
		uint64_t midval = states[mid];

		if (state == midval)
			return mid;
		else if (state < midval)
			upper = mid-1;
		else
			lower = mid+1;
	}

	//We've explored all other options, see if we found it on the last hit
	if (states[lower] == state)
		return lower;

	//Otherwise it wasn't found
	return (uint64_t)-1LL;
}

/*
 * After the markov chain is built, we have a sparse matrix in COO format
 * which means we have a list of row, col, valublocke tuples (stored as 3 separate lists)
 * row and col are the actual signature of the markov state.  We need to map this to
 * sequential indices from 0 to len(states) so that this matrix can be output.
 *
 * Since we maintained a list of all the states we visited, the states vector,
 * we can just sort that and then a binary search on that vector will give us
 * a valid row or column index for that state.
 *
 * So, we just iterate through the row and column vectors and replace the state signatures with
 * the corresponding row or column indices.
 *
 * Similarly, we update r_rows as well.  r_cols takes values in the absorbing state number 
 * and does not need to be updated since this is already sequential.
 */

void MarkovChain::renumber_transients()
{
	uint64_t mem_to_use = memtune.sorting_mem;
	std::vector<uint32_t> 		q_rows;
	std::vector<uint32_t> 		q_cols;
	std::vector<markov_real> 	q_vals;

	prepare_state_table();

	printf("Renumbering Transients\n");

	//Sort these first because it's harder to sort once there are zeros (maybe)
	printf("Ordering list of transient states\n");
	stxxl::ksort(trans.begin(), trans.end(), RowExtractor(), mem_to_use);

	printf("Ordering list of absorbing states\n");
	stxxl::ksort(absorb.begin(), absorb.end(), RowExtractor(), mem_to_use);

	printf("Renaming transient state transitions and building CSR representation\n");

	//Replace all row and column state signatures with indices
	//Update them into the CSR representation of the Q Matrix
	//No i,j entry should appear twice, so we don't have to worry
	//about adding different triplets together.
	uint64_t dim = states.size();
	uint64_t n = trans.size();

	uint64_t last_row_state = (uint64_t)-1LL;

	q_rows.reserve(dim+1);
	q_cols.reserve(n);
	q_vals.reserve(n);

	for (size_t i=0; i<trans.size(); ++i)
	{
		CoordTriplet &entry = trans[i];
		
		uint32_t col = (uint32_t)find_state(entry.col);

		q_cols.push_back(col);
		q_vals.push_back(entry.val);

		if (trans[i].row != last_row_state)
		{
			q_rows.push_back(i);
			last_row_state = trans[i].row;
		}
	}

	q_rows.push_back(trans.size());

	//We no longer need the triplet form of the Q Matrix
	trans.clear();

	q.init_from_vectors(q_cols, q_rows, q_vals, true); //q swaps out the vector contents so these can be disposed of.

	printf("Renaming absorbing state transitions\n");

	for (size_t i=0; i<absorb.size();++i)
	{
		CoordTriplet &entry = absorb[i];
		entry.row = find_state(entry.row);
	}

	printf ("Number of transient states: %llu\n", num_transient);
}

MarkovState MarkovChain::create_neighbor_state(MarkovState curr, size_t part, size_t ax, size_t dir)
{
	MarkovState neigh(curr);

	int8_t d = 1 - 2*dir;
	//bool wrapped = false;

	if (part == 0)
		neigh.elec_loc[ax] += d;
	else
		neigh.hole_loc[ax] += d;


	if (neigh.elec_loc[ax] < bounds[ax][0])
		neigh.elec_loc[ax] = bounds[ax][1];
	else if (neigh.elec_loc[ax] > bounds[ax][1])
		neigh.elec_loc[ax] = bounds[ax][0];

	if (neigh.hole_loc[ax] < bounds[ax][0])
		neigh.hole_loc[ax] = bounds[ax][1];
	else if (neigh.hole_loc[ax] > bounds[ax][1])	
		neigh.hole_loc[ax] = bounds[ax][0];

	/*if (wrapped)
	{
		printf("Wrapped electron: Current elec: (%d, %d, %d), next (%d, %d, %d)\n", curr.elec_loc[0], curr.elec_loc[1], curr.elec_loc[2], neigh.elec_loc[0], neigh.elec_loc[1], neigh.elec_loc[2]);
	}*/

	return neigh;
}

/*
 * Save the generator matrix in CSR format 
 */

void MarkovChain::save_binary(const std::string &filename)
{	
	q.save(filename);
}

void MarkovChain::save_statemap_binary(const std::string &filename)
{
	uint64_t dim = states.size();

	FILE *f = fopen(filename.c_str(), "w");

	if (!f)
		return;

	fwrite(&dim, sizeof(uint64_t), 1, f);

	for (size_t i=0; i<dim; ++i)
	{
		uint64_t state = states[i];
		fwrite(&state, sizeof(uint64_t), 1, f);
	}

	fclose(f);
}

void MarkovChain::save_absorbing_binary(const std::string &filename, MarkovState state)
{	
	uint64_t n = states.size();
	uint64_t sig = state.get_signature();

	DenseVector<markov_real> dense_abs(n);

	for (uint64_t i=0; i<n; ++i)
		dense_abs[i] = 0.0;

	for (uint64_t i=0; i<absorb.size(); ++i)
	{
		if (absorb[i].col == sig)
			dense_abs[absorb[i].row] += absorb[i].val; //+= is important bc there could be multiple triplets pointing to the same dense_abs[i]
	}

	dense_abs.save(filename);
}

void MarkovChain::save_wait_times(const std::string &filename)
{
	save_absorbing_binary(filename, wait_state);
}

void MarkovChain::normalize_probabilities(markov_real *probs, markov_real *total)
{
	markov_real sum = 0.0;

	for (size_t i=0; i<12; ++i)
		sum += probs[i];

	for (size_t i=0; i<12; ++i)
		probs[i] /= sum;

	*total = sum;
}

void MarkovChain::walk_chain(MarkovState curr, uint64_t curr_row, markov_real prob, MarkovMatrixType type, std::vector<NextStep> &next_steps)
{
	markov_real trans[12];
	markov_real total_rate;

	adj_states.clear();
	adj->find_adj_states(curr, adj_states);

	if (adj_states.size() != 12)
	{

		printf("Wrong number of adjacent states: %zu\n", adj_states.size());
		return;
	}

	for (size_t i=0;i<12;++i)
		trans[i] = rc->calculate_rate(curr, adj_states[i]);

	normalize_probabilities(trans, &total_rate);

	/*
	 * NormalMatrix: I - Q where Q is the markov chain generator matrix.  This allows one to find the probabiliy 
	 * of ending up in a given absorbing state at long times 
	 * ExtendedMatrix: Q with the absorbing states added on as rows in the matrix.  This allows one to evolve
	 * a state vector explicitly for each transition and see the distribution of possible states after X steps
	 */

	if (type == kNormalMatrix)
	{
		add_transient_triplet(curr_row, curr_row, 1.0);
		add_absorbing_triplet(curr_row, wait_state.get_signature(), 1.0/total_rate);
	}

	//Recurse through all nearest neighbors
	for(size_t i=0; i<12; ++i)
	{
		MarkovState next = adj_states[i];
		
		markov_real cum_prob = prob*trans[i];

		next = check_transition(curr, next, cum_prob, trans[i]);

		if (next.is_absorbing())
		{
			uint64_t col = next.get_signature();
			add_absorbing_triplet(curr_row, col, trans[i]);
		}
		else
		{
			bool 	 visited = mapper->check_state(next);
			uint64_t col = next.get_signature();
			
			markov_real val = trans[i];

			if (type != kExtendedMatrix)
				val *= -1.0;

			add_transient_triplet(curr_row, col, val);  //We want I-Q (if not extended)

			if (!visited)
			{
				//Add this state to our state list
				states.push_back(next.get_signature());

				NextStep step;

				step.col = col;
				step.prob = cum_prob;
				step.next = next;

				next_steps.push_back(step);
			}
		}
	}
}

long MarkovChain::map_state(std::vector<int> elec, std::vector<int> hole)
{
	MarkovState curr(elec, hole);

	return map(curr);
}

/*
 * Extend transient matrix with absorbing entries
 */
void MarkovChain::prepare_extended_matrix()
{
	/*uint64_t dim = num_transient;

	for (size_t i=0; i<r_rows.size(); ++i)
		add_transient_triplet(r_rows[i], r_cols[i]+dim, r_vals[i]);

	for (size_t i=0; i<mapper->get_num_absorbing(); ++i)
		add_transient_triplet(dim+i, dim+i, 1.0);

	printf("Building Extended Q matrix\n");
	printf("Number of absorbing states: %lu\n", mapper->get_num_absorbing());
	q_matrix = new SparseMatrix(rows, cols, vals, num_transient+mapper->get_num_absorbing());
	rows.resize(0);
	cols.resize(0);
	vals.resize(0);	
	r_rows.resize(0);
	r_cols.resize(0);
	r_vals.resize(0);*/

	//Temporarily commented out while I optimize walk_chain for large matrices
}

void MarkovChain::choose_mapper()
{
	size_t size = bounds[0][1] - bounds[0][0];
	size *= bounds[1][1] - bounds[1][0];
	size *= bounds[2][1] - bounds[2][0];

	size *= size;

	if (mapper)
		delete mapper;

	if (size/8 > 1<<29)
	{
		printf("state space too big, using slow hash map state mapper\n");
		mapper = new BigMarkovStateMapper();
	}
	else
	{
		printf("using fast bitmap state mapper\n");
		mapper = new BitmapStateMapper(&bounds[0][0], &bounds[1][0], &bounds[2][0]);
	}
}

bool MarkovChain::prepare_for_walk()
{
	if (!rc || !adj)
	{
		printf("You must specify a RateCalculator and and AdjacencyGenerator for this markov chain.\n");
		return false;
	}

	//Propagate configuration information to all sub objects that need it.
	for (std::vector<ConfiguredObject*>::iterator it = sub_objs.begin(); it != sub_objs.end(); ++it)
	{
		(*it)->set_bounds((coord_t *)bounds);
	}

	adj->set_bounds((coord_t*)bounds);
	rc->prepare_calculation();

	if (!mapper)
	{
		printf("You must set bounds on the simulation first so we can choose an appropriate mapper.\n");
		return false;
	}

	return true;
}

void MarkovChain::build(coord_t elec_start[3], coord_t hole_start[3], MarkovMatrixType type)
{
	MarkovState start(elec_start, hole_start);

	std::vector<NextStep> next_steps;
	std::vector<NextStep> current_steps;

	wait_state = create_absorbing_state("Waiting Time");

	if (!prepare_for_walk())
	{
		printf("Canceling chain building because the system is not properly initialized.\n");
		return;
	}

	//Make sure we don't visit the starting state twice.
	mapper->check_state(start);
	states.push_back(start.get_signature());

	NextStep first_step;

	first_step.prob = 1.0;
	first_step.next = start;
	first_step.col = start.get_signature();

	current_steps.push_back(first_step);

	do 
	{
		printf("Evaluating %lu sites\n", current_steps.size());
		//Visit all of the sites in this round and accumulate those that we need to visit in the next round
		for (size_t i=0; i<current_steps.size(); ++i)
		{
			NextStep &step = current_steps[i];
			walk_chain(step.next, step.col, step.prob, type, next_steps);
		}

		current_steps.swap(next_steps);
		next_steps.clear();
	} while (current_steps.size() > 0);

	printf("Transient Entries: %llu\n", trans.size());
	printf("Absorbing Entries: %llu\n", absorb.size());

	renumber_transients();
	
	if (type == kExtendedMatrix)
		prepare_extended_matrix();
}

void MarkovChain::build(std::vector<int> elec, std::vector<int> hole, MarkovMatrixType type)
{
	coord_t e[3];
	coord_t h[3];

	for (size_t i=0; i<3; ++i)
	{
		e[i] = elec[i];
		h[i] = hole[i];
	}

	build(e, h, (MarkovMatrixType)type);
}

MarkovState MarkovChain::create_absorbing_state(const std::string &name)
{
	return mapper->add_absorbing_state(name);
}

void MarkovChain::add_pruning_rule(ChainPruningRule *rule)
{
	pruning_rules.push_back(rule);
	sub_objs.push_back(rule);
}

void MarkovChain::print_triplets()
{
	for (size_t i = 0; i<trans.size(); ++i)
		printf("(%llu, %llu): %g\n", trans[i].row, trans[i].col, trans[i].val);
}

void MarkovChain::fill_xy_slice(MarkovState initial, CarrierType carrier, int side, double *out, int n)
{
	//Make sure there's room
	if (n != side*side)
	{
		printf("Invalid array size (should be side^2): %d\n", n);
		return;
	}
	
	coord_t mobile[3];
	coord_t fixed[3];

	if (carrier == kHole)
	{
		for (size_t i=0; i<3; ++i)
		{
			mobile[i] = initial.hole_loc[i];
			fixed[i] = initial.elec_loc[i];
		}
	}
	else
	{
		for (size_t i=0; i<3; ++i)
		{
			mobile[i] = initial.elec_loc[i];
			fixed[i] = initial.hole_loc[i];
		}
	}

	coord_t min_x = mobile[0] - side/2;
	coord_t max_x = min_x + side;

	coord_t min_y = mobile[0] - side/2;
	coord_t max_y = min_y + side;

	for (mobile[0]=min_x; mobile[0]<max_x; ++mobile[0])
	{
		for (mobile[1]=min_y; mobile[1]<max_y; ++mobile[1])
		{
			MarkovState curr;

			if (carrier == kHole)
				curr = MarkovState(fixed, mobile);
			else
				curr = MarkovState(mobile, fixed);

			int64_t found = map(curr);

			size_t offset = (mobile[1]-min_y)*side + (mobile[0] - min_x);

			if (found == -1)
				out[offset] = -1.0;
			else
				out[offset] = results[found];
		}
	}
}

void MarkovChain::set_adj_generator(AdjacencyGenerator *a)
{
	adj = a;
}