#include "ConfiguredObject.h"

ConfiguredObject::ConfiguredObject()
{
	lattice = 10.0;
	dielectric = 4.0;
	world = NULL;
}

void ConfiguredObject::set_bounds(const std::vector<int> &b)
{
	for (size_t i=0; i<6; ++i)
	{
		size_t axis = i/2;
		size_t dir = i%2;

		bounds[axis][dir] = b[i];
	}

	update_dist();
}

void ConfiguredObject::set_bounds(coord_t *b)
{
	for (size_t i=0; i<6; ++i)
	{
		size_t axis = i/2;
		size_t dir = i%2;

		bounds[axis][dir] = b[i];
	}

	update_dist();
}

void ConfiguredObject::update_dist()
{
	for(size_t i=0;i<3;++i)
		dist[i] = bounds[i][1] - bounds[i][0] + 1;
}

void ConfiguredObject::set_lattice(markov_real lat)
{
	lattice = lat;
}

void ConfiguredObject::set_dielectric(markov_real die)
{
	dielectric = die;
}

coord_t ConfiguredObject::nearest_replica_dist2(coord_t *origin, coord_t *pt)
{
	coord_t dist2 = 0;

	for (size_t i=0; i<3; ++i)
	{
		coord_t d = std::min(abs(origin[i] - pt[i]), dist[i] - abs(origin[i] - pt[i]));
		dist2 += d*d;
	}

	return dist2;
}

void ConfiguredObject::set_world(World *w)
{
	world = w;
}