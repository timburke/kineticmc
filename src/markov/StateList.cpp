//StateList.cpp

#include <stdio.h>
#include "StateList.h"

StateList::StateList(const std::string &file)
{
	load_file(file);
}

StateList::~StateList()
{

}

void StateList::load_file(const std::string &file)
{
	size_t size;
	size_t num;
	FILE *f = fopen(file.c_str(),"r");

	if (!f)
	{
		printf("Could not open file %s\n", file.c_str());
		return;
	}

	//Get number of entries
	fseek(f, 0, SEEK_END);
	size = ftell(f);
	fseek(f, 0, SEEK_SET);
	num = size/sizeof(uint64_t);

	uint64_t header;
	fread(&header, sizeof(uint64_t), 1, f);

	if (header != (num-1))
	{
		printf("Invalid file, number of entries (%zu) did not equal header (%llu)\n", num, header);
		fclose(f);
		return;
	}

	if (num*sizeof(uint64_t) != size)
	{
		printf("Invalid file loaded.  It was not a list of uint64_t integers (size was wrong)\n.");
		fclose(f);
		return;
	}

	--num;

	states.resize(num);

	for (size_t i=0; i<num; ++i)
	{
		uint64_t n;

		fread(&n, sizeof(uint64_t), 1, f);

		states[i] = MarkovState::FromValue(n);
	}

	fclose(f);
}

int StateList::map_state(const std::vector<int> &elec, const std::vector<int> &hole)
{
	MarkovState state(elec, hole);

	std::vector<MarkovState>::iterator found;
	found = std::lower_bound(states.begin(), states.end(), state);

	if (found->get_signature() == state.get_signature())
		return found - states.begin();

	return -1;
}

MarkovState StateList::map_index(int index)
{
	if (index < 0 || index >= states.size())
		return MarkovState();

	return states[index];
}

int StateList::len()
{
	return states.size();
}