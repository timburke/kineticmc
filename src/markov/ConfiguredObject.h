#ifndef __ConfiguredObject_h__
#define __ConfiguredObject_h__

#include "Types.h"
#include "MarkovTypes.h"
#include "World.h"

/*
 * Generic base class for objects that have configuration information about a simulation, 
 * since many different objects need to know this configuration.
 */

class ConfiguredObject
{
	protected:
	coord_t dist[3];	//size of our periodic world
	coord_t bounds[3][2];

	markov_real lattice;
	markov_real dielectric;

	World 		*world;

	coord_t nearest_replica_dist2(coord_t *origin, coord_t *pt);
	void	update_dist();
	
	public:
	ConfiguredObject();
	virtual ~ConfiguredObject() {};

	void set_bounds(const std::vector<int> &b);
	void set_bounds(coord_t *b);
	void set_lattice(markov_real lat);
	void set_dielectric(markov_real die);
	void set_world(World *world);
};

#endif