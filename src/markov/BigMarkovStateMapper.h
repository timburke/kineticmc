/*
 * MarkovStateMapper.h
 * Define the StateMapper class for maintaining an efficient mapping between
 * MarkovState objects and transition matrix rows
 */

#ifndef __BigMarkovStateMapper_h__
#define __BigMarkovStateMapper_h__

#include <vector>
#include <map>
#include <string>
#include <google/dense_hash_map>
#include "MarkovState.h"
#include "StateMapperInterface.h"

#define kMarkovStateMapperDefaultSlab	(1<<20)

class BigMarkovStateMapper : public StateMapperInterface
{
	private:
	std::vector<MarkovState*> state_slabs;
	google::dense_hash_map<uint64_t, uint64_t> row_map;

	uint64_t next_transient_id;
	
	uint64_t slab_size;
	uint64_t slab_mask;

	bool check_power_of_two(uint64_t val);
	uint64_t create_slab_mask(uint64_t size);

	uint64_t get_next_transient();

	void allocate_new_slab();
	uint64_t add_transient_row(MarkovState state);

	public:
	BigMarkovStateMapper();
	virtual ~BigMarkovStateMapper();

	//Setup functions

	//Configuration functions
	void set_slab_size(uint64_t size);

	virtual bool check_state(MarkovState state);
	virtual void reset();
};

#endif