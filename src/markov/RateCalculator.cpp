//RateCalculator.cpp

#include "RateCalculator.h"

RateCalculator::RateCalculator() : world(NULL), rate_model(NULL)
{
	for (size_t i=0; i<6; ++i)
		((coord_t*)bounds)[i] = 0;

	lattice_const = 10.0;
	temp = 300.0;
	dielectric = 4.0;
}

RateCalculator::~RateCalculator()
{

}

void RateCalculator::set_bounds(const std::vector<int> &b)
{
	for (size_t i=0; i<6; ++i)
	{
		size_t axis = i/2;
		size_t dir = i%2;

		bounds[axis][dir] = b[i];
	}
}

PreallocatedWorld *RateCalculator::get_world()
{
	return world;
}

void RateCalculator::set_temperature(markov_real t)
{
	temp = t;
}

void RateCalculator::set_lattice(markov_real c)
{
	lattice_const = c;
}

void RateCalculator::set_dielectric(markov_real die)
{
	dielectric = die;
}

void RateCalculator::set_world(WorldGenerator *gen)
{
	coord_t loc[3];
	coord_t sides[3];

	std::vector<int> smallest;

	if (world)
	{
		delete world;
		world = NULL;
	}

	smallest.resize(3);

	for (size_t i=0; i<3; ++i)
	{
		smallest[i] = bounds[i][0];
		sides[i] = bounds[i][1] - bounds[i][0] + 1;
	}

	world = new PreallocatedWorld(sides[0], sides[1], sides[2]);
	world->set_smallest(smallest);

	for (loc[0]=bounds[0][0]; loc[0]<=bounds[0][1]; ++loc[0])
	{
		for (loc[1]=bounds[1][0]; loc[1]<=bounds[1][1]; ++loc[1])
		{
			for (loc[2]=bounds[2][0]; loc[2]<=bounds[2][1]; ++loc[2])
				world->set_value(loc, gen->generate(loc, lattice_const, world), kElectron);
		}
	}
}

void RateCalculator::add_interation(InteractionModel *model)
{
	interactions.push_back(model);
}

void RateCalculator::save_nrrd(const std::string &filename, ImageType type)
{
	if (!world)
		return;

	world->save_nrrd(filename, type);
}

void RateCalculator::prepare_calculation()
{
	for(std::list<InteractionModel*>::iterator iter = interactions.begin(); iter != interactions.end(); ++iter)
	{
		(*iter)->set_bounds((coord_t*)bounds);
		(*iter)->set_lattice(lattice_const);
		(*iter)->set_dielectric(dielectric);
		(*iter)->set_world(world);
	}

	if (rate_model)
		rate_model->set_temp(temp);
}

markov_real RateCalculator::calculate_energy(MarkovState state)
{
	markov_real en = 0.0;

	coord_t	elec[3], hole[3];

	state.fill_positions(elec, hole);

	for(std::list<InteractionModel*>::iterator iter = interactions.begin(); iter != interactions.end(); ++iter)
		en += (*iter)->calc_energy(elec, hole);

	return en;
}

markov_real RateCalculator::calculate_rate(MarkovState src, MarkovState dst)
{
	markov_real src_en, dst_en;

	src_en = calculate_energy(src);
	dst_en = calculate_energy(dst);

	return rate_model->calculate_rate(src, dst, src_en, dst_en);
}

void RateCalculator::set_ratemodel(RateModel *rm)
{
	rate_model = rm;
}	