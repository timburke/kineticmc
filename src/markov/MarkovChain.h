/*
 * MarkovChain
 * An object representing an AbsorbingMarkovChain used to simulate 2-particle dynamics in Organic Semiconductors
 */

#ifndef __MarkovChain_h__
#define __MarkovChain_h__

#include "MarkovState.h"
#include "pruning/ChainPruningRule.h"
#include "control-sim.h"
#include "StateMapperInterface.h"
#include "MarkovTypes.h"
#include "RateCalculator.h"
#include "AdjacencyGenerator.h"
#include "CSRSparseMatrix.h"
#include <google/dense_hash_map>
#include <vector>
#include <stxxl/vector>
#include <stxxl/algorithm>

struct NextStep
{
	uint64_t col;
	double prob;
	MarkovState next;
};

enum MarkovMatrixType
{
	kNormalMatrix,
	kExtendedMatrix
};

struct CoordTriplet
{
	uint64_t row;
	uint64_t col;
	double 	 val;

	static CoordTriplet min_value()
	{
		CoordTriplet v;

		v.row = 0;
		v.col = 0;
		v.val = 0.0;

		return v;
	}

	static CoordTriplet max_value()
	{
		CoordTriplet v;

		v.row = (uint64_t)-1LL;
		v.col = (uint64_t)-1LL;
		v.val = std::numeric_limits<double>::max();

		return v;
	}

	bool operator==(const CoordTriplet &rhs) const
	{
		return (row == rhs.row && col == rhs.col && val == rhs.val);
	}

	bool operator!=(const CoordTriplet &rhs) const
	{
		return !(*this == rhs);
	}
};

/*
 * Compare two triplets by row first and the column.
 * WARNING
 */

struct CoordTripletCompare
{
	bool operator()(const CoordTriplet &lhs, const CoordTriplet &rhs) const
	{
		if (lhs.row < rhs.row)
			return true;

		if (lhs.row == rhs.row && lhs.col < rhs.col)
			return true;

		return false;
	}

	CoordTriplet min_value() const {return CoordTriplet::min_value();};
	CoordTriplet max_value() const {return CoordTriplet::max_value();};
};

struct RowExtractor
{
	typedef uint64_t key_type;

	key_type operator()(const CoordTriplet &coord) const
	{
		return coord.row;
	}

	CoordTriplet min_value() const {return CoordTriplet::min_value();};
	CoordTriplet max_value() const {return CoordTriplet::max_value();};
};

struct STXXLMemConfig
{
	uint64_t sorting_mem;
	uint64_t vector_building_mem;

	STXXLMemConfig() : sorting_mem(512*1024*1024), vector_building_mem(512*1024*1024) {};
};

typedef stxxl::VECTOR_GENERATOR<uint64_t>::result uint64_vector;
typedef stxxl::VECTOR_GENERATOR<uint32_t>::result uint32_vector;
typedef stxxl::VECTOR_GENERATOR<double>::result double_vector;
typedef stxxl::VECTOR_GENERATOR<double>::result double_vector;
typedef stxxl::VECTOR_GENERATOR<CoordTriplet>::result triplet_vector;

class MarkovChain
{
	private:
	StateMapperInterface 			*mapper;
	std::vector<ChainPruningRule *> pruning_rules;

	std::vector<ConfiguredObject *> sub_objs;

	STXXLMemConfig					memtune;

	//Sparse transition matrix in triplet format
	triplet_vector trans;
	triplet_vector absorb;

	//Sparse transition matrix is csr format
	CSRSparseMatrix<uint32_t, markov_real> q;

	std::vector<double>				  results;

	size_t block_size;
	size_t elem_per_block;

	//Temporary storage used during  chain walking
	std::vector<MarkovState>		 adj_states;

	uint64_t num_transient;
	
	//sorted list of visited states to allow for 
	//quick mapping back and forth between states and row numbers
	uint64_vector states;
	std::vector<uint64_t> state_blks;

	coord_t bounds[3][2];

	SimulationController *sim;
	RateCalculator		 *rc;
	AdjacencyGenerator	 *adj;

	MarkovState			wait_state;

	void add_transient_triplet(uint64_t row, uint64_t col, double value);
	void add_absorbing_triplet(uint64_t row, uint64_t col, double value);

	void 		load_location(MarkovState loc);
	MarkovState check_transition(MarkovState start, MarkovState end, double cum_prob, double local_prob);
	MarkovState create_neighbor_state(MarkovState curr, size_t part, size_t ax, size_t dir);
	void		walk_chain(MarkovState curr, uint64_t curr_row, markov_real prob, MarkovMatrixType type, std::vector<NextStep> &next_steps);
	void 		normalize_probabilities(markov_real *probs, markov_real *total);
	void 		prepare_extended_matrix();
	void		choose_mapper();
	void		renumber_transients();
	int64_t		map(MarkovState state);
	void		prepare_state_table();
	uint64_t 	find_state(uint64_t state);
	bool		prepare_for_walk();

	public:
	MarkovChain();
	~MarkovChain();

	//Configuration routines
	void set_bounds(std::vector<int> x, std::vector<int> y, std::vector<int> z);

	void set_simulator(SimulationController *s);
	void set_rate_calculator(RateCalculator *r);
	void set_adj_generator(AdjacencyGenerator *a);
	void add_pruning_rule(ChainPruningRule *rule);

	void build(coord_t elec_start[3], coord_t hole_start[3], MarkovMatrixType type);
	void build(std::vector<int> elec, std::vector<int> hole, MarkovMatrixType type);	//for python

	//To save results to a file for later parallel solving
	void save_binary(const std::string &filename);
	void save_absorbing_binary(const std::string &filename, MarkovState state);
	void save_wait_times(const std::string &filename);
	void save_statemap_binary(const std::string &filename);

	void load_results_vector(const std::string &file);

	MarkovState create_absorbing_state(const std::string &name);

	double get_probability(MarkovState initial);

	void fill_xy_slice(MarkovState initial, CarrierType carrier, int side, double *n, int n_dim);

	void print_triplets();
	void print_states();

	long map_state(std::vector<int> elec, std::vector<int> hole);
};

#endif