#include "MonteCarlo.h"
#include "IndirectCompare.hxx"

MatrixMonteCarlo::MatrixMonteCarlo()
{
	mapper = NULL;
	waits = NULL;
	chain = NULL;

	poisson_times = false;
	world_size = 1;
	timescale = 1.0;
}

void MatrixMonteCarlo::clear()
{
	hole_disp2.reset(bin_resolution, max_time);
	elec_disp2.reset(bin_resolution, max_time);
	separation2.reset(bin_resolution, max_time);
	absorption.reset(bin_resolution, max_time);
}

markov_real MatrixMonteCarlo::get_delay(uint32_t state)
{
	markov_real wait = waits->operator[](state);

	if (!poisson_times)
		return wait/timescale;

	markov_real delay = 0.0;
	while(delay == 0.0) 
		delay = drand48();

	return -1.0*wait*log(delay)/timescale;
}

/*
 * Given that the system is in state i (row), pick the state that it will end up in next according to
 * the kinetic monte carlo algorithm.  If the simulation will terminate, return a sentinal value (-1)
 */
uint32_t MatrixMonteCarlo::next_step(uint32_t row)
{
	double val = drand48();

	uint32_t row_start = chain->rowptrs[row]; 
	uint32_t row_end = chain->rowptrs[row+1];

	for (uint32_t i=row_start; i<row_end; ++i)
	{
		//Ignore diagonal values
		if (chain->cols[i] == row)
			continue;

		val += chain->vals[i];	//off diagonal entries of the transition chain are negative.
		if (val < 0.0)
			return chain->cols[i];
	}

	return kSentinalValue;
}

void MatrixMonteCarlo::setup_traces(double max, double resolution, double scale, int size)
{
	bin_resolution = resolution;
	max_time = max;
	timescale = scale;
	world_size = size;

	clear();
}

void MatrixMonteCarlo::trace(double time, uint32_t row)
{
	MarkovState state = mapper->map_index(row);

	hole_disp2.log(time, periodic_dist2(starting_state, state, false));
	elec_disp2.log(time, periodic_dist2(starting_state, state, true));
	separation2.log(time, state.periodic_separation2(world_size));
}

void MatrixMonteCarlo::setup_chain(StateList *m, DenseVector<markov_real> *w, CSRSparseMatrix<uint32_t, markov_real> *matrix)
{
	mapper = m;
	waits = w;
	chain = matrix;
}

int MatrixMonteCarlo::periodic_dist2(MarkovState s1, MarkovState s2, bool elec)
{
	int dist2 = 0;

	for (size_t i=0; i<3; ++i)
	{
		int d;

		if (elec)
			d = std::min(abs(s1.elec_loc[i] - s2.elec_loc[i]), world_size - abs(s1.elec_loc[i] - s2.elec_loc[i]));
		else
			d = std::min(abs(s1.hole_loc[i] - s2.hole_loc[i]), world_size - abs(s1.hole_loc[i] - s2.hole_loc[i]));

		dist2 += d*d;
	}

	return dist2;
}

TimeTrace<int>*	MatrixMonteCarlo::get_hole_disp2()
{
	return &hole_disp2;
}

TimeTrace<int>*	MatrixMonteCarlo::get_elec_disp2()
{
	return &elec_disp2;
}

TimeTrace<int>* MatrixMonteCarlo::get_separation2()
{
	return &separation2;
}

TimeTrace<int>* MatrixMonteCarlo::get_absorption()
{
	return &absorption;
}

void MatrixMonteCarlo::set_poisson(bool poisson)
{
	poisson_times = poisson;
}

void MatrixMonteCarlo::run(uint32_t start)
{
	if (waits == NULL || mapper == NULL || chain == NULL)
		return;

	uint32_t row = start;
	double time = 0.0;

	starting_state = mapper->map_index(start);

	while(time < max_time)
	{
		trace(time, row);
		row = next_step(row);

		if (row == kSentinalValue)
		{
			absorption.log(time, 1);
			return;
		}

		time += get_delay(row);
	}
}