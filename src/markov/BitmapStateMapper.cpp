
#include "BitmapStateMapper.h"

BitmapStateMapper::BitmapStateMapper(coord_t *xb, coord_t *yb, coord_t *zb)
{
	dims[0] = xb[1] - xb[0] + 1;
	dims[1] = yb[1] - yb[0] + 1;
	dims[2] = zb[1] - zb[0] + 1;

	small[0] = xb[0];
	small[1] = yb[0];
	small[2] = zb[0];

	part_size = dims[0]*dims[1]*dims[2];

	states.resize(part_size*part_size);
}

size_t BitmapStateMapper::map_state(MarkovState state)
{
	size_t elec_part = (state.elec_loc[0]-small[0])*dims[1]*dims[2] + (state.elec_loc[1]-small[1])*dims[2] + (state.elec_loc[2]-small[2]);
	size_t hole_part = (state.hole_loc[0]-small[0])*dims[1]*dims[2] + (state.hole_loc[1]-small[1])*dims[2] + (state.hole_loc[2]-small[2]);

	return elec_part*part_size + hole_part;
}

bool BitmapStateMapper::check_state(MarkovState state)
{
	size_t index = map_state(state);

	bool res = states.is_set(index);
	states.set(index);

	return res;
}

void BitmapStateMapper::reset()
{
	throw std::string("Reset not supported yet in BitmapStateMapper");
}