//BitmapStateMapper.h

#ifndef __BitmapStateMapper_h__
#define __BitmapStateMapper_h__

#include "StateMapperInterface.h"
#include "Bitmap.h"
#include "Types.h"

class BitmapStateMapper : public StateMapperInterface
{
	private:
	Bitmap states;
	size_t dims[3];
	coord_t small[3];
	size_t part_size;

	size_t map_state(MarkovState state);

	public:
	BitmapStateMapper(coord_t *xb, coord_t *yb, coord_t *zb);
	virtual bool check_state(MarkovState state);
	virtual void reset();
};

#endif