//MarkovState.cpp

#include "MarkovState.h"
#include "World.h"
#include <string>
#include <cmath>
#include <algorithm>

MarkovState::MarkovState()
{
	value = 0;
}

MarkovState MarkovState::FromValue(uint64_t v)
{
	MarkovState state;

	state.value = v;

	return state;
}



MarkovState::MarkovState(coord_t elec[3], coord_t hole[3])
{
	for (size_t i=0; i<3; ++i)
	{
		elec_loc[i] = (int8_t)elec[i];
		hole_loc[i] = (int8_t)hole[i];
	}

	flags1 = 0;
	flags2 = 0;
}

size_t MarkovState::periodic_separation2(int dim)
{
	size_t sep2 = 0;

	for (size_t i=0; i<3; ++i)
	{
		size_t d = std::min(abs(elec_loc[i] - hole_loc[i]), (int)dim - abs(elec_loc[i] - hole_loc[i]));
		sep2 += d*d;
	}

	return sep2;
}

MarkovState::MarkovState(const std::vector<int> &e, const std::vector<int> &h)
{
	for (size_t i=0; i<3; ++i)
	{
		elec_loc[i] = e[i];
		hole_loc[i] = h[i];
	}

	flags1 = 0;
	flags2 = 0;
}

MarkovState::MarkovState(uint64_t absorbing_id)
{
	if (absorbing_id >= (1<<24))
		throw std::string("Tried to create an absorbing markov state with too large of an id");

	value = absorbing_id & ((1 << 24)-1);

	set_absorbing();
}

MarkovState::MarkovState(const MarkovState &rhs)
{
	this->value = rhs.value;
}

MarkovState MarkovState::without_flags() const
{
	MarkovState ret(*this);

	ret.flags1 = 0;
	ret.flags2 = 0;

	return ret;
}

MarkovState MarkovState::operator=(const MarkovState &rhs)
{
	this->value = rhs.value;

	return *this;
}

bool MarkovState::operator<(const MarkovState &rhs) const
{
	return this->get_signature() < rhs.get_signature();
}

bool MarkovState::is_absorbing()
{
	return (flags1 == 0xFF && flags2 == 0xFF);
}

void MarkovState::set_absorbing()
{
	flags1 = 0xFF;
	flags2 = 0xFF;
}

uint64_t MarkovState::get_signature() const
{
	return without_flags().value;
}

CarrierType MarkovState::which_moved(MarkovState other)
{
	if (other.elec_loc[0] == elec_loc[0] && other.elec_loc[1] == elec_loc[1] && other.elec_loc[2] == elec_loc[2])
		return kHole;

	return kElectron;
}

void MarkovState::fill_positions(coord_t *elec, coord_t *hole)
{
	for (size_t i=0; i<3; ++i)
	{
		elec[i] = elec_loc[i];
		hole[i] = hole_loc[i];
	}
}

bool MarkovState::equal_pos(const MarkovState &rhs) const
{
	 for (size_t i=0; i<3; ++i)
	 {
	 	if (elec_loc[i] != rhs.elec_loc[i])
	 		return false;

	 	if (hole_loc[i] != rhs.hole_loc[i])
	 		return false;
	 }

	 return true;
}

bool MarkovState::carriers_overlap(void) const
{
	 for (size_t i=0; i<3; ++i)
	 {
	 	if (elec_loc[i] != hole_loc[i])
	 		return false;
	 }

	 return true;
}

std::vector<int> MarkovState::elec_pos(void)
{
	std::vector<int> pos;

	pos.resize(3);

	for (size_t i=0; i<3; ++i)
		pos[i] = elec_loc[i];

	return pos;
}

std::vector<int> MarkovState::hole_pos(void)
{
	std::vector<int> pos;

	pos.resize(3);

	for (size_t i=0; i<3; ++i)
		pos[i] = hole_loc[i];

	return pos;
}