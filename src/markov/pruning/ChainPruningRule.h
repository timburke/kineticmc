/*
 * ChainPruningRule
 * A functor that decides when to stop tracking a given path through a Markov Chain
 */

#ifndef __ChainPruningRule_h__
#define __ChainPruningRule_h__

#include "MarkovState.h"
#include "ConfiguredObject.h"

class ChainPruningRule : public ConfiguredObject
{
	public:
	virtual MarkovState check_transition(MarkovState current, MarkovState next, double cum_prob, double trans_prob) = 0;

	virtual ~ChainPruningRule() {};
};

#endif