//SingleDesinationRule.cpp

#include "SingleDestinationRule.h"

SingleDestinationRule::SingleDestinationRule(MarkovState final) : final_state(final)
{

}

MarkovState SingleDestinationRule::check_transition(MarkovState current, MarkovState next, double cum_prob, double trans_prob)
{
	if (prune_state(current, next, cum_prob, trans_prob) == true)
		return final_state;

	return next;
}

bool SingleDestinationRule::prune_state(MarkovState current, MarkovState next, double cum_prob, double trans_prob)
{
	return false;
}