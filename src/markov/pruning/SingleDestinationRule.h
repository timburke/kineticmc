/*
 * SingleDestinationRule
 * base class for rules that take a single final state as input during construction
 * and prune chains to end at that state when a given criteria is met.
 */

#ifndef __SingleDestinationRule_h__
#define __SingleDestinationRule_h__

#include "ChainPruningRule.h"

class SingleDestinationRule : public ChainPruningRule
{
	protected:
	MarkovState	final_state;

	//Subclasses should override this method with their specific pruning rule
	virtual bool prune_state(MarkovState current, MarkovState next, double cum_prob, double trans_prob);

	public:
	SingleDestinationRule(MarkovState final);

	virtual MarkovState check_transition(MarkovState current, MarkovState next, double cum_prob, double trans_prob);
};

#endif