//MaximumHopsPruningRule.cpp

#include "MaximumHopsPruningRule.h"
#include "Types.h"
#include <stdio.h>

MaximumHopsPruningRule::MaximumHopsPruningRule(int elec_hops, int hole_hops, MarkovState final) : SingleDestinationRule(final), max_elec(elec_hops), max_hole(hole_hops)
{

}

MarkovState MaximumHopsPruningRule::check_transition(MarkovState current, MarkovState next, double cum_prob, double trans_prob)
{
	if (next.which_moved(current) == kElectron)
	{
		//printf("elec moved");
		next.flags1 = current.flags1 + 1;
		next.flags2 = current.flags2;
	}
	else
	{
		//printf("hole moved");
		next.flags1 = current.flags1;
		next.flags2 = current.flags2+1;
	}

	if (max_elec >= 0 && next.flags1 > max_elec)
	{
		//printf("Pruned by MaxHopsElectron Rule (%d, %d)", current.flags1, max_elec);
		return final_state;
	}

	if (max_hole >= 0 && next.flags2 > max_hole)
	{
		//printf("Pruned by MaxHopsHole Rule: (%d, %d)", current.flags2, max_hole);
		return final_state;
	}

	return next;
}