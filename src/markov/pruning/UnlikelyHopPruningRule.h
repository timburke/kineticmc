/*
 * UnlikelyHopPruningRule
 * Do not allow hops whose probability is less than a given threshold.
 */

#ifndef __UnlikelyHopPruningRule_h__
#define __UnlikelyHopPruningRule_h__

#include "SingleDestinationRule.h"
#include "BasicTypes.h"

class UnlikelyHopPruningRule : public SingleDestinationRule
{
	private:
	double threshold;

	protected:
	virtual bool prune_state(MarkovState current, MarkovState next, double cum_prob, double trans_prob);

	public:
	UnlikelyHopPruningRule(MarkovState final, real_t threshold);

};

#endif