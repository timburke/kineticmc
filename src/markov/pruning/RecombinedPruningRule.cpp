#include "RecombinedPruningRule.h"
#include <stdio.h>

RecombinedPruningRule::RecombinedPruningRule(MarkovState final) : SingleDestinationRule(final)
{

}

bool RecombinedPruningRule::prune_state(MarkovState current, MarkovState next, double cum_prob, double trans_prob)
{
	//printf("Checking (%d, %d, %d) (%d, %d, %d) ... ", next.elec_loc[0], next.elec_loc[1], next.elec_loc[2], next.hole_loc[0], next.hole_loc[1], next.hole_loc[2]);
	for (size_t i=0; i<3; ++i)
	{
		if (next.elec_loc[i] != next.hole_loc[i])
			return false;
	}

	//printf("Pruned by RecombinedRule");
	return true;
}