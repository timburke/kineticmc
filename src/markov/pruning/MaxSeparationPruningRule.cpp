#include "MaxSeparationPruningRule.h"

MaxSeparationPruningRule::MaxSeparationPruningRule(int max_dist, MarkovState final) : SingleDestinationRule(final)
{
	max_sep2 = max_dist*max_dist;
}

bool MaxSeparationPruningRule::prune_state(MarkovState current, MarkovState next, double cum_prob, double trans_prob)
{
	coord_t dist2 = 0;

	for (size_t i=0; i<3; ++i)
	{
		coord_t d = std::min(abs(next.elec_loc[i] - next.hole_loc[i]), dist[i] - abs(next.elec_loc[i] - next.hole_loc[i]));
		dist2 += d*d;
	}

	if (dist2 > max_sep2)
		return true;

	return false;
}