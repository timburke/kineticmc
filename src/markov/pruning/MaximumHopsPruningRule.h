/*
 * MaximumHopsPruningRule 
 * Prune the markov chain one the electron and/or hole has hopped more than
 * a fixed number of times.
 */

#ifndef __MaximumHopsPruningRule_h__
#define __MaximumHopsPruningRule_h__

#include "SingleDestinationRule.h"

class MaximumHopsPruningRule : public SingleDestinationRule
{
	private:
	int max_elec;
	int max_hole;

	public:
	MaximumHopsPruningRule(int max_elec_hops, int max_hole_hops, MarkovState final);

	virtual MarkovState check_transition(MarkovState current, MarkovState next, double cum_prob, double trans_prob);
};

#endif