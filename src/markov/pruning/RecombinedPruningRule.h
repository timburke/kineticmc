/*
 * RecombinedPruningRule
 */

#ifndef __RecombinedPruningRole_h__
#define __RecombinedPruningRole_h__

#include "SingleDestinationRule.h"

class RecombinedPruningRule : public SingleDestinationRule
{
	protected:
	virtual bool prune_state(MarkovState current, MarkovState next, double cum_prob, double trans_prob);

	public:
	RecombinedPruningRule(MarkovState final);
};

#endif