#include "UnlikelyHopPruningRule.h"

UnlikelyHopPruningRule::UnlikelyHopPruningRule(MarkovState final, real_t t) : SingleDestinationRule(final), threshold(t)
{

}

bool UnlikelyHopPruningRule::prune_state(MarkovState current, MarkovState next, double cum_prob, double trans_prob)
{
	if (trans_prob < threshold)
		return true;

	return false;
}