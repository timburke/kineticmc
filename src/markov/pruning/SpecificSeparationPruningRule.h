/*
 * SpecificSeparationPruningRule
 * Prune chains where the separation between electrons and holes equals a specific distance
 */

#ifndef __SpecificSeparationPruningRule_h__
#define __SpecificSeparationPruningRule_h__

#include "SingleDestinationRule.h"

class SpecificSeparationPruningRule : public SingleDestinationRule
{
	private:
	coord_t sep2;

	protected:
	virtual bool prune_state(MarkovState current, MarkovState next, double cum_prob, double trans_prob);

	public:
	SpecificSeparationPruningRule(int exact_sep, MarkovState final);
};

#endif