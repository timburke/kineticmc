/*
 * MaxSeparationPruningRule
 * Prune chains where the separation between electrons and holes exceeds a specific distance
 */

#ifndef __MaxSeparationPruningRule_h__
#define __MaxSeparationPruningRule_h__

#include "SingleDestinationRule.h"

class MaxSeparationPruningRule : public SingleDestinationRule
{
	private:
	coord_t max_sep2;

	protected:
	virtual bool prune_state(MarkovState current, MarkovState next, double cum_prob, double trans_prob);

	public:
	MaxSeparationPruningRule(int max_dist, MarkovState final);
};

#endif