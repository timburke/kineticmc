#include "SpecificSeparationPruningRule.h"

SpecificSeparationPruningRule::SpecificSeparationPruningRule(int sep, MarkovState final) : SingleDestinationRule(final)
{
	sep2 = sep*sep;
}

bool SpecificSeparationPruningRule::prune_state(MarkovState current, MarkovState next, double cum_prob, double trans_prob)
{
	coord_t s2 = 0;

	for (size_t i=0; i<3; ++i)
		s2 += (next.elec_loc[i]-next.hole_loc[i])*(next.elec_loc[i]-next.hole_loc[i]);

	if (s2 == sep2 )
		return true;

	return false;
}