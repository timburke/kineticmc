/*
 * ChainAnalyzer
 * A class for quickly calculating things about a MarkovChain
 * given its fundamental matrix and some other information.
 */

#ifndef __ChainAnalyzer_h__
#define __ChainAnalyzer_h__

#include "MarkovTypes.h"
#include "CSRSparseMatrix.h"

class ChainAnalyzer
{
	private:
	CSRSparseMatrix<uint32_t, markov_real> *mat;
	markov_real						 prefactor;		//unknown units (typically s^-1)
	markov_real						 kT;			//eV

	public:
	ChainAnalyzer(CSRSparseMatrix<uint32_t, markov_real> *mat);
	ChainAnalyzer();

	void set_matrix(CSRSparseMatrix<uint32_t, markov_real> *mat);
	void set_prefactor(markov_real pre);
	void set_temperature(markov_real T);

	//Analysis Functions
	void denormalize(DenseVector<markov_real> *v); //divide every row by v[i] (i.e. left multiply by diag(v))
	void to_hopping_barrier(DenseVector<markov_real> *v);
	void to_energy_barrier(DenseVector<markov_real> *v);
};

#endif