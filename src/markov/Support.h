/*
 * Support.h
 * from http://stackoverflow.com/questions/7993050/multiplatform-atomic-increment
 */

#ifndef __Support_h__
#define __Support_h__

#ifdef __GNUC__
#define atomic_inc(ptr) __sync_fetch_and_add ((ptr), 1)
#elif defined (_WIN32)
#define atomic_inc(ptr) InterlockedIncrement ((ptr))
#else
#error "Need some more porting work here for multiplatform atomic increment"
#endif

#endif