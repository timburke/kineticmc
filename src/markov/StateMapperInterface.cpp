#include "StateMapperInterface.h"

StateMapperInterface::StateMapperInterface() :	next_absorbing_id(0)
{

}

uint64_t StateMapperInterface::get_next_absorbing()
{
	return next_absorbing_id++;
}

MarkovState StateMapperInterface::add_absorbing_state(const std::string &name)
{
	uint64_t id = get_next_absorbing();
	MarkovState state(id);

	absorbing_states.push_back(std::pair<std::string, MarkovState>(name, state));

	return state;
}

uint64_t StateMapperInterface::get_num_absorbing()
{
	return next_absorbing_id;
}