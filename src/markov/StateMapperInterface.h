/* 
 *
 */

#ifndef __StateMapperInterface_h__
#define __StateMapperInterface_h__

#include "MarkovState.h"
#include <vector>
#include <string>

class StateMapperInterface
{
	private:
	std::vector<std::pair<std::string, MarkovState> >  absorbing_states;

	uint64_t next_absorbing_id;

	uint64_t get_next_absorbing();

	public:
	StateMapperInterface();
	virtual ~StateMapperInterface() {};
	virtual bool check_state(MarkovState state) = 0;
	virtual void reset() = 0;

	MarkovState add_absorbing_state(const std::string &name);
	uint64_t get_num_absorbing();
};

#endif