//BigMarkovStateMapper.cpp

#include "BigMarkovStateMapper.h"
#include "Support.h"
#include <stdio.h>

BigMarkovStateMapper::BigMarkovStateMapper()
{
	next_transient_id = 0;
	
	row_map.set_empty_key(0);

	slab_size = kMarkovStateMapperDefaultSlab;
	slab_mask = create_slab_mask(slab_size);
}

BigMarkovStateMapper::~BigMarkovStateMapper()
{
	for (std::vector<MarkovState*>::iterator it = state_slabs.begin(); it != state_slabs.end(); ++it)
		delete[] *it;
}

bool BigMarkovStateMapper::check_power_of_two(uint64_t val)
{
	unsigned int c; // c accumulates the total bits set in v

	for (c = 0; val; val >>= 1)
	{
	  c += val & 1;
	}

	return (c == 1);
}

/*
 * Go from 0b...100000... to 0b...011111....
 *
 */
uint64_t BigMarkovStateMapper::create_slab_mask(uint64_t size)
{
	return (size-1);
}

void BigMarkovStateMapper::set_slab_size(uint64_t size)
{
	if (state_slabs.size() != 0)
		throw std::string("Cannot change slab size after states have been allocated");

	if (!check_power_of_two(size))
		throw std::string("Size must be a power of 2");

	slab_size = size;
	slab_mask = create_slab_mask(slab_size);
}

void BigMarkovStateMapper::reset()
{
	throw std::string("Reset not supported yet in BigMarkovStateMapper\n");
}

uint64_t BigMarkovStateMapper::get_next_transient()
{
	return next_transient_id++;
}

void BigMarkovStateMapper::allocate_new_slab()
{
	state_slabs.push_back(new MarkovState[slab_size]);
}

uint64_t BigMarkovStateMapper::add_transient_row(MarkovState state)
{
	uint64_t id = get_next_transient();
	uint64_t slab = id / slab_size;
	uint64_t offset = id & slab_mask;

	if (slab == state_slabs.size())
	{
		printf("Allocating new slab (id=%llu, slab=%llu, slabsize=%llu)\n", id, slab, slab_size);
		allocate_new_slab();
	}

	state_slabs[slab][offset] = state.without_flags();

	return id;
}

bool BigMarkovStateMapper::check_state(MarkovState state)
{
	google::dense_hash_map<uint64_t, uint64_t>::iterator it = row_map.find(state.get_signature());

	if (it == row_map.end())
	{
		uint64_t id = add_transient_row(state);

		row_map[state.get_signature()] = id;

		return false;
	}

	return true;
}