#ifndef __NearestNeighborAdjacency_h__
#define __NearestNeighborAdjacency_h__

#include "AdjacencyGenerator.h"

class NearestNeighborAdjacency : public AdjacencyGenerator
{
	private:
	MarkovState create_neighbor_state(MarkovState curr, size_t part, size_t ax, size_t dir);
	
	public:
	virtual void find_adj_states(MarkovState s, std::vector<MarkovState> &adj_list);
};

#endif