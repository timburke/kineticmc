#include "NearestNeighborAdjacency.h"

void NearestNeighborAdjacency::find_adj_states(MarkovState s, std::vector<MarkovState> &adj_list)
{
	for (size_t part = 0; part < 2; ++part)
	{
		for (size_t ax = 0; ax < 3; ++ax)
		{
			for (size_t dir = 0; dir < 2; ++dir)
				adj_list.push_back(create_neighbor_state(s, part, ax, dir));
		}
	}
}

MarkovState NearestNeighborAdjacency::create_neighbor_state(MarkovState curr, size_t part, size_t ax, size_t dir)
{
	MarkovState neigh(curr);

	int8_t d = 1 - 2*dir;

	if (part == 0)
		neigh.elec_loc[ax] += d;
	else
		neigh.hole_loc[ax] += d;


	if (neigh.elec_loc[ax] < bounds[ax][0])
		neigh.elec_loc[ax] = bounds[ax][1];
	else if (neigh.elec_loc[ax] > bounds[ax][1])
		neigh.elec_loc[ax] = bounds[ax][0];

	if (neigh.hole_loc[ax] < bounds[ax][0])
		neigh.hole_loc[ax] = bounds[ax][1];
	else if (neigh.hole_loc[ax] > bounds[ax][1])	
		neigh.hole_loc[ax] = bounds[ax][0];

	return neigh;
}
