#ifndef __AdjacencyGenerator_h__
#define __AdjacencyGenerator_h__

#include "MarkovState.h"
#include "ConfiguredObject.h"

#include <vector>

class AdjacencyGenerator : public ConfiguredObject
{
	public:
	//Subclasses must implement this function.
	virtual void find_adj_states(MarkovState s, std::vector<MarkovState> &adj_list) = 0;
};

#endif