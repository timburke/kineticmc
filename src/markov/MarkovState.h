/*
 * MarkovState.h
 * Basic Definitions for the Markov chain based solver imlemented in this directory
 */

#ifndef __MarkovState_h__
#define __MarkovState_h__

#include <stdint.h>
#include "Types.h"

/*
 * Pack all of the state information into a 64 bit integer 
 */

#define kMarkovStateSize 	sizeof(MarkovState)
#define kAbsorbingStateFlag 0xFF
#define kMarkovStateFlag1Mask (255LL << 24)
#define kMarkovStateFlag2Mask (255LL << 56)
#define kMarkovStateFlagsMask (kMarkovStateFlag1Mask | kMarkovStateFlag2Mask)

struct MarkovState
{
	union 
	{
		struct
		{
			int8_t elec_loc[3];
			uint8_t flags1;

			int8_t hole_loc[3];
			uint8_t flags2;
		} __attribute__ ((__packed__));

		uint64_t value;
	};

	//Convenience Functions
	MarkovState();
	MarkovState(coord_t *elec, coord_t *hole);
	MarkovState(const std::vector<int> &e, const std::vector<int> &h);
	MarkovState(uint64_t absorbing_id);
	MarkovState(const MarkovState &rhs);

	bool is_absorbing();
	void set_absorbing();

	uint64_t get_signature() const;
	MarkovState without_flags() const;
	
	CarrierType which_moved(MarkovState other);
	void fill_positions(coord_t *elc, coord_t *hole);

	MarkovState operator=(const MarkovState &rhs);
	bool operator<(const MarkovState &rhs) const;

	bool carriers_overlap(void) const;
	bool equal_pos(const MarkovState &rhs) const;

	size_t periodic_separation2(int dim);

	static MarkovState FromValue(uint64_t value);

	std::vector<int> elec_pos();
	std::vector<int> hole_pos();
};

struct StateKeyExtractor
{
	typedef uint64_t key_type;
	
	key_type operator()(uint64_t record) const
	{
		return record;
	}

	uint64_t min_value() const {return 0;};
	uint64_t max_value() const {return (uint64_t)-1LL;};
};

#endif
