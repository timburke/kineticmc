//StateList.h

#ifndef __StateList_h__
#define __StateList_h__

#include "MarkovState.h"

class StateList
{
	private:
	std::vector<MarkovState> states;

	void load_file(const std::string &file);

	public:
	StateList(const std::string &file);
	~StateList();

	int map_state(const std::vector<int> &elec, const std::vector<int> &hole);
	MarkovState map_index(int index);

	int len();
};

#endif