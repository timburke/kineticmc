//control-sim.h

#ifndef __control_sim_h__
#define __control_sim_h__

#include "kmc-interface.h"
#include "ResultFunctor.h"
#include "Types.h"
#include "MobilityModel.h"
#include <pthread.h>
#include <list>
#include <map>

class SimulationController;

enum ParticleIC
{
	kSpecificDistance,
	kSpecificPositions
};

struct SimulationParameters
{
	real_t field; //Direction of the electric field in this simulation (V/cm)
	FieldType field_type;
	
	real_t dialectric_const; //Dialectric constant of the medium
	real_t lattice_const; //lattice constant for this simple cubic lattice (Angstroms)
	real_t temp; //Kelvin

	ParticleIC initial_type; //Type of initial particle positions
	real_t initial_r; //initial carrier spacing (Angstroms) if initial_type is kSpecificDistance.  Otherwise unused
	std::vector<int> initial_hole;
	std::vector<int> initial_elec;

	real_t recomb_life; //Recombination lifetime for e- and h+ once they are adjacent to each other (s)
	MobilityModelGenerator *elec_mob;
	MobilityModelGenerator *hole_mob;
	
	unsigned long trials; //Number of trials to run
	
	//Specify how to create the world
	WorldGeneratorSpec *world_spec;

	std::vector<int> periodic_size; //should be a tuple with 3 side lengths
	
	long			block_size; //the world will be allocated in blocks with sides of this length
	long			num_blocks; //must be a power of 2
	
	unsigned int	num_recomb_traj;
	unsigned int	num_escaped_traj;
	unsigned int	num_timeout_traj;
	
	bool use_simd;
	bool preallocate;
	
	//Initialize default values for parameters so that nothing breaks weirdly
	SimulationParameters()
	{
		field = 0.0;
		field_type = kIsotropic;
		
		dialectric_const = 4.0;
		lattice_const = 1.0; //Angstroms
		
		initial_r = 10.0;
		initial_type = kSpecificDistance;
		
		temp = 300.0;
		
		recomb_life = 1.0;
		elec_mob = NULL;
		hole_mob = NULL;
		
		trials = 10000;
		
		world_spec = NULL;
		
		block_size = 50;
		num_blocks  = 8;
		
		num_recomb_traj = 0;
		num_escaped_traj = 0;
		num_timeout_traj = 0;
		
		use_simd = false;
		preallocate = false;
	}
};

struct SimEnvironment
{
	KMCInterface *sim;
	SimulationController *master; 
	
	double *results; //preallocated by master thread
	
	unsigned long trials;
	SimulationParameters params;
	
	SimEnvironment() : sim(NULL), results(NULL) {};
	~SimEnvironment()
	{
		if (results != NULL)
			delete[] results;
			
		if (sim)
			delete sim;
	}
};

class SimulationController
{
	private:
	SimulationParameters params;
	
	std::map<std::string, FunctorFactory*> result_types;
	std::list<std::pair<ResultCondition, ResultFunctor*> > tracked_results;
	
	std::vector<CarrierPositions> last_trace;
	
	pthread_mutex_t 			  trace_mutex;
	pthread_mutex_t				  id_mutex;
	
	KMCInterface *calc_sim; //If we're asked to prepare a simulation for short interactive calculations from python
	
	log4cxx::LoggerPtr	perf_l;
	
	bool verbose;
	
	int node_number;
	int next_id;
	
	double *results;
	size_t results_w;
	size_t results_l;
	
	std::vector<SimEnvironment*> sims;
	std::vector<pthread_t> threads;
	
	void clear_sims();
	void clean_results();
	void save_trace(std::vector<CarrierPositions> &trace);
	
	size_t calculate_result_width();
	
	void create_sim_thread(unsigned long trials);
	void build_sim(SimulationParameters &p, KMCInterface **out);
	
	static void* run_sim(void *envP);
	
	void set_world(KMCInterface *sim, coord_t block, coord_t side);
	void preallocate_world(KMCInterface *sim, std::vector<int> &sides, real_t lattice, WorldGenerator *gen);

	public:
	SimulationController();
	~SimulationController();
	
	void initialize(SimulationParameters &p);
	//void preallocate_world(std::vector<int> dims); //Force the simulation to preallocate the entire world rather than generating it procedurally
	
	void run(size_t threads);
	
	//Result tracking features
	void add_result_type(const std::string &name, FunctorFactory *factory);
	void track_result(const std::string &name, ResultCondition cond = kAlwaysCondition);
	
	void visualize_world(const std::string &filename, size_t side);
	void visualize_trace(const std::string &filename, std::vector<CarrierPositions> &trace, bool animation=false);
	
	//Python compat functions
	void get_results(double *n, int n_dim);
	int get_result_width();
	int get_result_length();
	
	//Functions that work on the calc sim for interactive calculations from the python terminal
	void create_calc_sim(SimulationParameters &p);
	void set_calc_field(std::vector<real_t> field);
	void set_calc_carrier_pos(std::vector<int> elec, std::vector<int> hole); //can't use coord_t here because of SWIG type checking bug
	void set_calc_carrier_pos_c(coord_t *elec, coord_t *hole);
	void calc_xy_world_slice(int side, real_t *n, int n_dim);
	
	void calc_hopping_rates(real_t *n, int n_dim);
	
	unsigned int get_result_columns(const std::string &name);
	std::vector<std::string> get_result_column_names(const std::string &name);
	
	//parallelization functions
	void set_node_number(int number);
	std::string unique_id();
};

#endif