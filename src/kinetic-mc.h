//kinetic-mc.h

#ifndef __kinetic_mc_h__
#define __kinetic_mc_h__

#include "kmc-interface.h"
#include "math.h"
#include "simd.h"
#include "sse_mathfun.h"
#include "assert.h"
#include "World.h"
#include <map>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <exception>
#include "MobilityModel.h"
#include <emmintrin.h>

#define JOULE_TO_EV		6.24150974e18
#define BOLTZMANN_EV	8.6173324e-5
#define MAX_DISTANCE_ENTRIES 1000000

typedef struct
{
	coord_t pos[3];
	
	real_t charge;
	
	MobilityModel *mobility;
} ChargeCarrier;

typedef struct
{
	size_t allocated_nodes;
	size_t allocated_dense_regions;
	
	size_t world_cache_hits;
	size_t world_cache_misses;
} SimulationPerformance;

typedef  struct
{
	coord_t elec[4];
	coord_t hole[4];
	
	//How to generate nearest neighbor positions
	coord_t inc_x[8];
	coord_t inc_y[8];
	coord_t inc_z[8];
	
	//positions of 12 nearest neighbor sites
	coord_t elec_x[12];
	coord_t elec_y[12];
	coord_t elec_z[12];
	
	coord_t hole_x[12];
	coord_t hole_y[12];
	coord_t hole_z[12];
	
	real_t hole_levels[12];		
	real_t elec_levels[12];
	
	real_t energy[12];
	real_t prob[12];
	
	real_t scaled_field[4];
	real_t tmp[4];
	
	//Save off last round's computation to avoid recalculating it
	real_t last_en;
	real_t last_hole_level;
	real_t last_elec_level;
}  __attribute__ ((aligned (64))) ComputationData; //Align to one cache line

template<typename EnergyGetter, typename MobilityGetter>
class MCSimulation : public KMCInterface
{
	private:
	ChargeCarrier 	electron;
	ChargeCarrier 	hole;
	real_t 		  	transitions[12]; //6 NN transitions for the e- and 6 NN transitions for the hole
	real_t			cum_rates[12];
	coord_t 		inc_table[6]; //For each transition, do we inc (1) or dec (-1)
	coord_t			dir_table[6]; //For each transition, what axis are we moving (0 .. 2)
	
	World			*space; //Store the local electronic environments
	WorldGenerator  *gen;
	bool			own_space;
	bool			own_gen;
	bool			use_sse;
	
	real_t 			*dist_table;
	coord_t			max_dist;
	
	SimulationData	results;
	
	ComputationData cmp;
	
	EnergyGetter *energy_gen;
	MobilityGetter *mob_gen;
	
	//Loggers for debugging and reporting
	log4cxx::LoggerPtr	progress_l;
	log4cxx::LoggerPtr	energy_l;
	log4cxx::LoggerPtr	mobility_l;
	log4cxx::LoggerPtr	initial_l;
	
	//Simulation Parameters that need to be set
	real_t field[3] SSE; //Direction of the electric field in this simulation (V/m)
	real_t invlat SSE; //precalculated inverse lattice constant (m^-1)
	
	real_t dialectric_const; //Dialectric constant of the medium
	real_t lattice_const; //lattice constant for this simple cubic lattice (m)
	real_t initial_r; //initial carrier spacing (m)
	real_t recomb_rate SSE; //Recombination rate for e- and h+ (s^-1)
	real_t kT;
	
	//Precalculated Values to speed things up
	real_t minusBeta SSE ; // -1.0/kT
	real_t binding_factor SSE;
	real_t r; //last calculated distance
	real_t binding; //last calculated binding energy
	bool context_free; //Do we need to save things to our world?
	
	//Aligned work space for SSE instructions
	coord_t epos[4] SSE;
	coord_t hpos[4] SSE;
	
	//Utility functions
	real_t get_inv_distance(coord_t *p1, coord_t *p2);
	
	bool positions_equal(coord_t *p1, coord_t *p2);
	real_t dot(real_t *v1, real_t *v2);
	void 	normalize(real_t *v);
	real_t dotdiff(real_t *d1, real_t *d2, real_t *d3);
	real_t dotdiff(coord_t *d1, coord_t *d2, real_t *d3);
	
	real_t calculate_energy(coord_t *epos, coord_t *hpos);
	
	//SIMD parallel functions
	void calculate_propensities_simd();
	void calculate_transitions_simd();
	void step_simd();
	void accumulate_rates_simd();
	void initialize_simd();
	
	//Debug functions
	void step_check();

	void build_jump_tables();
	void build_distance_table();
	
	void append_trace();
	
	void set_transition(size_t trans, coord_t *epos, coord_t *hpos, real_t energy);
	void calculate_transitions();
	void accumulate_rates();
	
	size_t find_transition(real_t R);
	size_t select_transition();
	
	bool check_recombined();
	bool check_escaped();
	
	void step();

	void save_step_delay(size_t i);
	
	public:
	MCSimulation();
	virtual ~MCSimulation();
	
	//Initialization
	virtual void initialize_simulation(real_t lattice, real_t separation, real_t eps, real_t temp);
	virtual void set_carrier_params(MobilityModel *electron_mob, MobilityModel *hole_mob, real_t recomb_time);
	virtual void set_field(real_t mag, FieldType type);
	virtual void set_field(real_t *field);
	virtual void calc_with_simd(bool val);
	
	virtual void set_world_generator(WorldGenerator *g, bool own=true);
	virtual void set_world(World *w, bool own=true);

	//Visualization
	virtual void fill_xy_world_slice(int side, real_t *n);

	
	//Initial state creation routines
	virtual void create_initial_state_bulk();
	virtual void create_state(coord_t *elec_pos, coord_t *hole_pos);
	
	virtual void run(size_t max_steps, bool trace);
	virtual void run_simd(size_t max_steps, bool trace);
	
	virtual bool escaped();
	
	virtual SimulationData *get_results();
	
	virtual void print_statistics();
	virtual void visualize_world(const std::string &filename, size_t side);
	virtual void save_world(const std::string &filename);
	
	//Interactive Calculation Routines
	virtual std::vector<real_t> calc_hopping_rates();
};

#include "kinetic-mc.txx"

#endif