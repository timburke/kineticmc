//poisson.cpp

#include "PoissonSolver3D.h"
#include "WorldAnnealer.h"
#include "PreallocatedWorld.h"
#include <stdlib.h>
#include <time.h>

int main(int argc, char **argv)
{
	srand48(time(NULL));
	PoissonSolver3D::initialize_fftw();
	

	int N = 50;

	{
	PoissonSolver3D solver(N,N, N, 5e-11, 4.0, kDirichlet, kDirichlet, kDirichlet);
	
	solver.zero_rho();
	//solver.create_point_charge(40, 40, 40, 1.60217646e-16);
	//solver.create_point_charge(60, 40, 40, -1.60217646e-16);
	//solver.create_point_charge(40, 60, 40, -1.60217646e-16);
	//solver.create_point_charge(40, 40, 60, -1.60217646e-16);
	//solver.create_point_charge(60, 60, 40, 1.60217646e-16);
	//solver.create_point_charge(60, 40, 60, 1.60217646e-16);
	//solver.create_point_charge(40, 60, 60, 1.60217646e-16);
	//solver.create_point_charge(60, 60, 60, -1.60217646e-16);
	solver.solve();
	solver.save_nrrd("out.nrrd");
	}
	
	PoissonSolver3D::cleanup_fftw();
	
	PreallocatedWorld *world = new PreallocatedWorld(N, N, N);
	
	WorldAnnealer builder;
	
	builder.set_world(world);
	builder.add_component("PCBM", 0.5);
	builder.add_component("P3HT", 0.5);
//	builder.add_component("Mixed", 0.3);
	
	builder.randomize_world();

	world->save_nrrd("molecules.nrrd", kMoleculeImage);
	builder.automata(500000000, 50000, true);
	builder.automata(0, 50000, false); //Now anneal at 0T until we converge
	
	printf("Characterizing World\n***************\n");
	WorldStatistics stats = world->characterize();
	
	for (std::map<int, RegionStatistics>::iterator it=stats.regions.begin(); it!=stats.regions.end(); ++it)
	{
		printf("Region %d\n", it->first);
		printf("Volume Fraction: %g\n", it->second.volume_fraction);
		printf("Feature Size: %g\n\n", it->second.feature_size);
	}
	printf("***************\nCharacterization Complete\n");
	
	std::vector<size_t> surfs;
	
	world->save_nrrd("annealed.nrrd", kMoleculeImage);
}