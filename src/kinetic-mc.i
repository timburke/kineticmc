%{
#define SWIG_FILE_WITH_INIT
%}

%include "numpy.i"
%include stl.i
%include "stdint.i"

//Needed for numpy array typemapping
%init %{
import_array();
%}

%apply (double* ARGOUT_ARRAY1, int DIM1) {(double *n, int n_dim)}
%apply (float* ARGOUT_ARRAY1, int DIM1) {(real_t *n, int n_dim)}
%apply (int* ARGOUT_ARRAY1, int DIM1) {(int *n, int n_dim)}
%apply (double* IN_ARRAY1, int DIM1) {(double *arr_in, int n)}

%module kineticmc

%pythoncode %{
	version = "0.5.1"	
%}

#include "python/utilities.py"

%include "utilities/ObjectFactory.hxx"
%include "mobility/MobilityModel.h"
%template(MobilityModelGenerator) ObjectFactory<MobilityModel>;

%{
#include "BasicTypes.h"
#include "markov/MarkovTypes.h"
%}

namespace std {
%template(DoubleVector) vector<double>;
%template(FloatVector) vector<float>;
%template(StringVector) vector<string>;
%template(IntVector) vector<int>;
}

//Automatically add the length parameter needed by numpy.i to convert the array to a numpy array
%feature("pythonprepend") SimulationController::get_results(double*, int) %{
	args = args + (self.get_result_length()*self.get_result_width(),)
%}


%feature("pythonappend") SimulationController::get_results(double*, int) %{
	val = val.reshape([self.get_result_length(), self.get_result_width()])
%}

//Convenience wrapping around calc_hopping_rates
%feature("pythonprepend") SimulationController::calc_hopping_rates(real_t*, int) %{
	args = args + (12,)
%}

%feature("pythonappend") SimulationController::calc_hopping_rates(real_t*, int) %{
	val = val.reshape([2,3,2])
%}

%feature("pythonprepend") SimulationController::calc_xy_world_slice(int, real_t*, int) %{
	dim = args[0]*args[0]*2
	args = args + (dim, )
%}

%feature("pythonappend") SimulationController::calc_xy_world_slice(int, real_t*, int) %{
	
	val = val.reshape([args[0], args[0], 2])
%}

//Convenience wrapping around WorldCreator->convert_world
%feature("pythonprepend") WorldCreator::convert_world(real_t*, int) %{
	size = self.get_allocated_size()
	
	args = args + (size*2,)
%}

%feature("pythonprepend") PreallocatedWorld::np_energies(double*, int) %{
	size = self.np_size()
	
	args = args + (size*2,)
%}

%feature("pythonprepend") PreallocatedWorld::np_tags(int*, int) %{
	size = self.np_size()
	
	args = args + (size,)
%}

%feature("pythonappend") WorldCreator::convert_world(real_t*, int) %{
	#val = val.reshape([dims[0], dims[1], dims[2], 2])
%}

%feature("pythonprepend") MarkovChain::fill_xy_slice(MarkovState, CarrierType, int, double *, int) %{
	args = args + (args[2]*args[2],)
%}

%feature("pythonappend") MarkovChain::fill_xy_slice(MarkovState, CarrierType, int, double *, int) %{
	val = val.reshape([args[2],args[2]])
%}

%feature("pythonprepend") SparseMatrix::fill_rows(int *, int) %{
	args = args + (self.get_num_entries(),)
%}

%feature("pythonprepend") SparseMatrix::fill_cols(int *, int) %{
	args = args + (self.get_dimension()+1,)
%}

%feature("pythonprepend") SparseMatrix::fill_vals(double *, int) %{
	args = args + (self.get_num_entries(),)
%}

%feature("pythonprepend") MatrixMonteCarlo::get_trajectory(int, int*, int) %{
        args = args + (self.trajectory_length(args[0]),)
%}

%feature("pythonprepend") MatrixMonteCarlo::get_times(double *, int) %{
        args = args + (self.total_length(),)
%}

%feature("pythonprepend") MatrixMonteCarlo::get_displacement(int, bool, double *, int) %{
        args = args + (self.total_length(),)
%}

%{
#include "control-sim.h"
#include "Types.h"

//World Generators
#include "worlds/PureMaterialSpec.h"
#include "worlds/BlendSpec.h"
#include "worlds/BilayerSpec.h"
#include "worlds/GaussianSpec.h"
#include "worlds/TrilayerSpec.h"

//Mobility Models
#include "mobility/MobilityModel.h"
#include "mobility/MobilityModelGenerators.h"

//Utilities
#include "utilities/Logger.h"
#include "utilities/TimeTrace.hxx"

//Math
#include "math/CSRSparseMatrix.h"
#include "math/DenseVector.h"
#include "math/MatrixCoarsener.h"

//WorldCreator
#include "worlds/WorldCreator.h"
#include "PreallocatedWorld.h"

//Markov Chains
#include "markov/MarkovChain.h"
#include "markov/MarkovState.h"
#include "markov/ChainAnalyzer.h"

#include "rate_model/RateModel.h"
#include "rate_model/RecombiningRateModel.h"
#include "rate_model/MillerAbrahamsRateModel.h"

#include "markov/pruning/ChainPruningRule.h"
#include "markov/pruning/SingleDestinationRule.h"
#include "markov/pruning/MaximumHopsPruningRule.h"
#include "markov/pruning/RecombinedPruningRule.h"
#include "markov/pruning/MaxSeparationPruningRule.h"
#include "markov/pruning/UnlikelyHopPruningRule.h"
#include "markov/pruning/SpecificSeparationPruningRule.h"
#include "markov/StateList.h"
#include "markov/RateCalculator.h"
#include "markov/adjacency/AdjacencyGenerator.h"
#include "markov/adjacency/NearestNeighborAdjacency.h"
#include "markov/MonteCarlo.h"

//Interactions
#include "interactions/InteractionModel.h"
#include "interactions/PeriodicCoulombModel.h"
#include "interactions/EnergyLevelModel.h"

//Metropolis
#include "metropolis/Lattice.h"
#include "metropolis/MetropolisTypes.h"
#include "metropolis/Sampler.h"
#include "metropolis/SemiperiodicLattice.h"
#include "metropolis/samplers/EnergySampler.h"
#include "metropolis/samplers/PositionSampler.h"
#include "metropolis/samplers/HistogramSampler.h"
#include "metropolis/samplers/EnergyHistogramSampler.h"
#include "metropolis/samplers/SeparationSampler.h"
%}

%include "BasicTypes.h"
%include "markov/MarkovTypes.h"
%include "Types.h"
%include "control-sim.h"

//World Generators
%include "worlds/PureMaterialSpec.h"
%include "worlds/BlendSpec.h"
%include "worlds/BilayerSpec.h"
%include "worlds/GaussianSpec.h"
%include "worlds/TrilayerSpec.h"

//MobilityModels
%template(IsotropicMobilityGenerator) OneParameterObjectFactory<IsotropicMobilityModel, real_t, MobilityModel>;
%template(DirectionalMobilityGenerator) ThreeParameterObjectFactory<DirectionalMobilityModel, std::vector<real_t>, real_t, real_t, MobilityModel>;
%template(AngularMobilityGenerator) ThreeParameterObjectFactory<DirectionalMobilityModel, real_t, real_t, real_t, MobilityModel>;

%include "mobility/MobilityModelGenerators.h"

//Utilities
%include "utilities/Logger.h"

//WorldCreator
%include "worlds/WorldCreator.h"
%include "PreallocatedWorld.h"

//Markov Chains
%include "markov/MarkovChain.h"
%include "markov/MarkovState.h"
%include "markov/ChainAnalyzer.h"

%include "rate_model/RateModel.h"
%include "rate_model/RecombiningRateModel.h"
%include "rate_model/MillerAbrahamsRateModel.h"

%include "markov/pruning/ChainPruningRule.h"
%include "markov/pruning/SingleDestinationRule.h"
%include "markov/pruning/MaximumHopsPruningRule.h"
%include "markov/pruning/RecombinedPruningRule.h"
%include "markov/pruning/MaxSeparationPruningRule.h"
%include "markov/pruning/UnlikelyHopPruningRule.h"
%include "markov/pruning/SpecificSeparationPruningRule.h"
%include "markov/StateList.h"
%include "markov/RateCalculator.h"
%include "markov/adjacency/AdjacencyGenerator.h"
%include "markov/adjacency/NearestNeighborAdjacency.h"
%include "markov/MonteCarlo.h"

//Interactions
%include "interactions/InteractionModel.h"
%include "interactions/PeriodicCoulombModel.h"
%include "interactions/EnergyLevelModel.h"

//Metropolis
%include "metropolis/Lattice.h"
%include "metropolis/MetropolisTypes.h"
%include "metropolis/Sampler.h"
%include "metropolis/SemiperiodicLattice.h"

%include "metropolis/samplers/EnergySampler.h"
%include "metropolis/samplers/PositionSampler.h"
%include "metropolis/samplers/HistogramSampler.h"
%include "metropolis/samplers/EnergyHistogramSampler.h"
%include "metropolis/samplers/SeparationSampler.h"

//Math
%include "math/CSRSparseMatrix.h"
%include "utilities/TimeTrace.hxx"
%include "math/DenseVector.h"
%include "math/MatrixCoarsener.h"

%feature("pythonprepend") CSRSparseMatrix<uint32_t, markov_real>::np_residuals(double*, int) %{
        args = args + (self.residual_len(),)
%}

%feature("pythonprepend") DenseVector<markov_real>::np_vector(double*, int) %{
        args = args + (self.len(),)
%}

%feature("pythonprepend") TimeTrace<int>::get_values(int*, int) %{
        args = args + (self.length(),)
%}

%feature("pythonprepend") TimeTrace<int>::get_counts(int*, int) %{
        args = args + (self.length(),)
%}

%template(RealSparseMatrix)		CSRSparseMatrix<uint32_t, markov_real>;
%template(RealVector)			DenseVector<markov_real>;
%template(IntTimeTrace)			TimeTrace<int>;
