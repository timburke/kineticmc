//ResultFunctor.h

#ifndef __ResultFunctor_h__
#define __ResultFunctor_h__

#include "Types.h"
#include <vector>
#include <string>

struct SimulationParameters;

class ResultFunctor
{
	public:
	virtual ~ResultFunctor() {};
	
	virtual unsigned int num_values() {return 1;};
	virtual std::string name() {return std::string("Unnamed");};
	virtual std::vector<std::string> column_names() {return std::vector<std::string>();};
	
	virtual void operator()(SimulationData *data, SimulationParameters &params, double *out) = 0;
};

class SimulationController;

class FunctorFactory
{
	public:
	virtual ResultFunctor *create() = 0;

	virtual ~FunctorFactory() {};

};

template<class T>
class TemplatedFactory : public FunctorFactory
{
	private:
	SimulationController *master;
	
	public:
	TemplatedFactory(SimulationController *m) : master(m) {};
	
	virtual ResultFunctor* create()
	{
		return new T(master);
	}
};

#endif