//SparseWorld.h

#ifndef __SparseWorld_h__
#define __SparseWorld_h__

#include "World.h"
#include "XYZHelper.h"
#include <stdlib.h>
/*
 * SparseWorld
 * An oct-tree based 3D grid that defines local regions in terms of blocks of a given size
 * Blocks are allocated on demand each time a position within them is requested for the first time
 * The total side length of the SparseWorld must be a blocksize * a power of 2.
 *
 */
 
class DenseWorld;

struct WorldPerformance
{
	size_t cache_hits;
	size_t cache_misses;
};

class SparseWorld : public World
{
	private:
	World *children[8];
	
	coord_t start[3];
	coord_t pivot[3];
	coord_t size[3];
	
	coord_t block_side;
	
	bool is_leaf;
	
	WorldPerformance stats;
	
	DenseWorld *last_region[kNumCarriers];
	
	size_t 	find_child(coord_t *pt);
	void 	allocate_child(size_t index);
	void 	build_child_start(size_t index, coord_t *out);
	void 	ensure_child_exists(size_t index);
	
	DenseWorld *get_leaf(coord_t *pos, CarrierType type, DenseWorld **cached=NULL);
	
	//Performance Calculation Routines
	void count_allocated_regions();
		
	public:
	SparseWorld(coord_t *start, coord_t block, coord_t side);
	virtual ~SparseWorld();
	
	//World Interface
	virtual LocalEnvironment &	get_value(coord_t *pos, CarrierType type); //pos must exist or this will throw an exception
	virtual bool 				value_exists(coord_t *pos, CarrierType type);
	virtual void				set_value(coord_t *pos, const LocalEnvironment &val, CarrierType type);
	
	virtual bool 				contains(coord_t *pos, CarrierType type);
	virtual void				reset();
	
	virtual void print_statistics();
	
	//Logging routines
	virtual size_t 	lattice_size();
	virtual void 	write_xyz(const std::string &name);
	void			append_xyz(FILE *f, XYZHelper &helper);
	

};

#endif
