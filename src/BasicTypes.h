/*
 * BasicTypes
 * This file defines primitive types for cross platform consistency
 *
 */
#ifndef __BasicTypes_h__
#define __BasicTypes_h__

#include <inttypes.h>

#define SSE __attribute__((aligned (16)))

typedef int32_t coord_t;
typedef float	real_t;

typedef coord_t Coordinate3[3];

#define kB_ev			8.6173324e-5
#define eps_0			8.85418782e-12
#define pi_f			3.14159265358979f
#define e_charge		1.60217657e-19

#endif