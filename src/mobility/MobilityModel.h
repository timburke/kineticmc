//MobilityModel.h

#ifndef __MobilityModel_h__
#define __MobilityModel_h__

#include "Types.h"
#include "ObjectFactory.hxx"

class MobilityModel
{
	public:
	virtual real_t mobility(Axis axis, Direction direction) = 0;
	
	virtual void fill_mobilities(real_t *mob);
	virtual void fill_mobilities(coord_t *hole_pos, real_t *mob);
	
	virtual ~MobilityModel() {};
};

//Define Generators for this type of object so we can create it from python
typedef ObjectFactory<MobilityModel> MobilityModelGenerator;

#endif