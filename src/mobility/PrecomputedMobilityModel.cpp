//PrecomputedMobilityModel.cpp

#include "PrecomputedMobilityModel.h"
#include <string.h>

void PrecomputedMobilityModel::fill_mobilities(real_t *mob)
{
	memcpy(mob, this->precomputed_mob, sizeof(real_t)*6);
}

void PrecomputedMobilityModel::fill_mobilities(coord_t *hole_pos, real_t *mob)
{
	memcpy(mob, this->precomputed_mob, sizeof(real_t)*6);
}