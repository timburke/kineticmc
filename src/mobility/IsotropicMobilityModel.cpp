//IsotropicMobilityModel.cpp

#include "IsotropicMobilityModel.h"

IsotropicMobilityModel::IsotropicMobilityModel(real_t m) 
	: mob(m)
{
	for (size_t i=0; i<6; ++i)
		precomputed_mob[i] = mob;
}

real_t IsotropicMobilityModel::mobility(Axis axis, Direction direction)
{
	return mob;
}