//DirectionalMobilityModel.h

#ifndef __DirectionalMobilityModel_h__
#define __DirectionalMobilityModel_h__

#include "PrecomputedMobilityModel.h"

class DirectionalMobilityModel : public PrecomputedMobilityModel
{
	private:
	real_t dir[3];
	real_t on_axis_mob;
	real_t off_axis_mob;
	
	real_t proj[3];
		
	log4cxx::LoggerPtr log;
	
	void initialize();
	
	public:
	DirectionalMobilityModel(const std::vector<real_t> &axis, real_t on_axis, real_t off_axis);
	DirectionalMobilityModel(real_t angle, real_t on_axis, real_t off_axis);
	
	virtual real_t mobility(Axis axis, Direction direction);
};

#endif