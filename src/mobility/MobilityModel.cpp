//MobilityModel.cpp

#include "MobilityModel.h"

void MobilityModel::fill_mobilities(real_t *mob)
{
	for (size_t i=0; i<6; ++i)
		mob[i] = mobility( (Axis)(i/2), (Direction)(i%2));
}

void MobilityModel::fill_mobilities(coord_t *hole_pos, real_t *mob)
{
	for (size_t i=0; i<6; ++i)
		mob[i] = mobility( (Axis)(i/2), (Direction)(i%2));
}