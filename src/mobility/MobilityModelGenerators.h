//A list of all the defined mobility model generators so that we can make python interfaces
//for them easily

#include "ObjectFactory.hxx"
#include "IsotropicMobilityModel.h"
#include "DirectionalMobilityModel.h"
#include <vector>

typedef OneParameterObjectFactory<IsotropicMobilityModel, real_t, MobilityModel> IsotropicMobilityGenerator;
typedef ThreeParameterObjectFactory<DirectionalMobilityModel, std::vector<real_t>, real_t, real_t, MobilityModel> DirectionalMobilityGenerator;
typedef ThreeParameterObjectFactory<DirectionalMobilityModel, real_t, real_t, real_t, MobilityModel> AngularMobilityGenerator;