//IsotropicMobilityModel.h

#ifndef __IsotropicMobilityModel_h__
#define __IsotropicMobilityModel_h__

#include "PrecomputedMobilityModel.h"

class IsotropicMobilityModel : public PrecomputedMobilityModel
{
	private:
	real_t mob;
	
	public:
	IsotropicMobilityModel(real_t m);
	
	virtual real_t mobility(Axis axis, Direction direction);
};

#endif