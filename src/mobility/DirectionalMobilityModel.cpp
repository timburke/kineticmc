//DirectionalMobilityModel.cpp

#include "DirectionalMobilityModel.h"
#include "basic_math.h"
#include <math.h>

DirectionalMobilityModel::DirectionalMobilityModel(real_t angle, real_t on_axis, real_t off_axis) :
	on_axis_mob(on_axis), off_axis_mob(off_axis)
{
	real_t rad_angle = 3.14159265359/180.0*angle;
	dir[0] = cos(rad_angle);
	dir[1] = sin(rad_angle);
	dir[2] = 0.0;
	
	log = log4cxx::Logger::getLogger("kineticmc.sim.calc.mobility");
	
	initialize();
}

DirectionalMobilityModel::DirectionalMobilityModel(const std::vector<real_t> &axis, real_t on_axis, real_t off_axis) :
	on_axis_mob(on_axis), off_axis_mob(off_axis)
{
	dir[0] = axis[0];
	dir[1] = axis[1];
	dir[2] = axis[2];
	
	log = log4cxx::Logger::getLogger("kineticmc.sim.calc.mobility");
	
	initialize();
}

void DirectionalMobilityModel::initialize()
{
	normalize(dir);

	//Precompute the projections onto each coord axis
	//Don't technically need to store them separately, but it makes it clearer what we're doing

	//BUGFIX: Need all three projections to calculate each mobility.
	for (size_t i=0; i<3; ++i)
		proj[i] = abs(dir[i]); //need absolute value because we don't care about the sign of the direction

	for (size_t i=0; i<3; ++i)
	{
		precomputed_mob[2*i+0] = proj[i]*on_axis_mob + proj[(i+1)%3]*off_axis_mob + proj[(i+2)%3]*off_axis_mob;
		precomputed_mob[2*i+1] = proj[i]*on_axis_mob + proj[(i+1)%3]*off_axis_mob + proj[(i+2)%3]*off_axis_mob;
	}
}

real_t DirectionalMobilityModel::mobility(Axis axis, Direction direction)
{
	//LOG_TRACE(log, "Directional Mobility (Axis:" << axis << ", Direction: " << direction << ")");
	//LOG_TRACE(log, "Preferred Axis: (" << dir[0] << ", " << dir[1] << "," << dir[2] << ")");
	
	//Basically return the dot product of the mobility tensor (assumed to be diagonal) and the axis of motion
	real_t val =  proj[axis]*on_axis_mob + proj[(axis+1)%3]*off_axis_mob + proj[(axis+2)%3]*off_axis_mob;
	
	//LOG_DEBUG(log, "Mobility: " << val);
	
	return val;
}