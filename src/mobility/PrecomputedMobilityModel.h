//PrecomputedMobilityModel.h

#ifndef __PrecomputedMobilityModel_h__
#define __PrecomputedMobilityModel_h__

#include "MobilityModel.h"

/*
 * PrecomputedMobilityModel
 * A base class for mobility models that can compute the mobility along all 6 near neighbor
 * hop directions in advance and store it.  This class provides a fast implementation of
 * fill_mobilities based on this precomputed data.
 */
 
class PrecomputedMobilityModel : public MobilityModel
{
	protected:
	real_t precomputed_mob[6];
	
	public:
	virtual void fill_mobilities(real_t *mob);
	virtual void fill_mobilities(coord_t *hole_pos, real_t *mob);
};

#endif