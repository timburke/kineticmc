//main.cpp

#include "control-sim.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char **argv)
{
	srand48(time(NULL));

	if (argc != 5)
	{
		printf("usage: test_mc <separation> <field strength> <trials> <num threads>\n");
		return 0;
	}
	
	double separation = atof(argv[1]); //Separation should be given in angstroms
	double field = atof(argv[2]); //Field should be given in V/cm and will be converted to V/m to feed to the MC code
	unsigned int trials = (unsigned int)atoi(argv[3]);
	unsigned int threads = (unsigned int)atoi(argv[4]);
	
	SimulationParameters p;
	SimulationController c;
	
	p.field = field;
	p.field_type = kIsotropic;
	p.dialectric_const = 4.0;
	p.initial_r = separation;
	p.lattice_const = 1.0;
	p.temp = 300;
	p.recomb_life = 1.0;
	p.elec_mob = 1.0;
	p.hole_mob = 1.0;
	p.trials = trials;
	
	c.initialize(p);
	c.run(threads);
		
	return 0;
}