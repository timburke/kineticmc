#include "Types.h"

void WorldGenerator::fill_neighbor_homos(coord_t *pos, real_t lattice_constant, World *space, real_t *out)
{
	for (size_t i=0; i<6; ++i)
	{
		pos[i/2] += (1 - (i%2)*2);
		
		out[i] = this->generate(pos, lattice_constant, space).homo;
		
		pos[i/2] -= (1 - (i%2)*2);
	}
}

void WorldGenerator::fill_neighbor_lumos(coord_t *pos, real_t lattice_constant, World *space, real_t *out)
{
	for (size_t i=0; i<6; ++i)
	{
		pos[i/2] += (1 - (i%2)*2);
		
		out[i] = this->generate(pos, lattice_constant, space).lumo;
		
		pos[i/2] -= (1 - (i%2)*2);
	}
}