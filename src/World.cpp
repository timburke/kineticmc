//World.cpp

#include "World.h"

size_t World::count_neighbors(coord_t *pos)
{
	size_t count = 0;
		
	int me = get_value(pos, kElectron).type;
		
	//PreallocatedWorld get_value mods index to achieve periodic BC so no range checking needed.
	//The other worlds allocate space on the fly so they don't need range checking either.
	for (size_t d=0; d<3; ++d)
	{
		int n1, n2;
		
		pos[d] += 1;
		n1 = get_value(pos, kElectron).type;
		
		pos[d] -= 2;
		n2 = get_value(pos, kElectron).type;
		
		pos[d] += 1;
		
		if (n1 == me)
			count += 1;
		
		if (n2 == me)
			count += 1;
	}
	
	return count;
}