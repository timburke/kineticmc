#ifndef __PreallocatedWorld_h__
#define __PreallocatedWorld_h__

#include "World.h"
#include <set>
#include <map>

/*
 * PreallocatedWorld
 * A 3-tensor with periodic BC, implicitly covering all space.  The origin of the 
 * allocated space is assumed to be (0,0,0).
 */

struct RegionStatistics
{
	double volume_fraction;
	double feature_size;
};
 
struct WorldStatistics
{
	std::map<int, RegionStatistics> regions;
};

/*
 * When saving nrrd files, what data should be stored in each voxel
 */
enum ImageType
{
	kMoleculeImage,
	kHOMOImage,
	kLUMOImage
};

class PreallocatedWorld : public World
{
	private:
	LocalEnvironment *arr;
	size_t dims[3];

	coord_t smallest[3];
	
	//Utilities
	void allocate();
	
	public:
	PreallocatedWorld(size_t xdim, size_t ydim, size_t zdim);
	PreallocatedWorld(size_t dims[3]);
	virtual ~PreallocatedWorld();
	
	virtual LocalEnvironment &	get_value(coord_t *pos, CarrierType type); //pos must exist or this will throw an exception
	virtual bool 				value_exists(coord_t *pos, CarrierType type);
	virtual void				set_value(coord_t *pos, const LocalEnvironment &val, CarrierType type) ;
	
	virtual bool 				contains(coord_t *pos, CarrierType type);
	
	virtual void				reset(); //Clear this world
	
	virtual void 				print_statistics() {};
	virtual void 				write_xyz(const std::string &name); //write allocated space to xyz file.
	
	virtual size_t				lattice_size(); //return the number of allocated lattice positions

	void save_nrrd(const std::string &name, ImageType chooser);	
	void get_dimensions(coord_t *out);	
	void find_surfaces(std::vector<size_t> &surfaces);
	void position_from_offset(size_t offset, coord_t *pos);
	
	void fill_electron_energies(coord_t *pos, real_t *out);
	void fill_hole_energies(coord_t *pos, real_t *out);

	void set_smallest(const std::vector<int> &smallest);

	void np_energies(double *n, int n_dim);
	void np_tags(int *n, int n_dim);
	int  np_size();
	
	coord_t get_offset(coord_t *pos);

	void fill_dims(coord_t *out);
	void fill_smallest(coord_t *out);
};

#endif