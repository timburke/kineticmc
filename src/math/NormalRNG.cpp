//NormalRNG.cpp

#include "NormalRNG.h"
#include <gsl/gsl_randist.h>
#include <time.h>

gsl_rng * NormalRNG::rng_uniform = NULL;

NormalRNG::NormalRNG()
{
	use_global();
}

NormalRNG::NormalRNG(long seed)
{
	if (seed == 0)
		use_global();
	else
	{
		selected_rng = gsl_rng_alloc(gsl_rng_mt19937);
	
		//seed the random number generator with fixed seed
		gsl_rng_set(selected_rng, seed);
		own_rng = true;
	}
}

void NormalRNG::use_global()
{
	if (rng_uniform == NULL)
	{
		rng_uniform = gsl_rng_alloc(gsl_rng_mt19937);
	
		//seed the random number generator
		gsl_rng_set(rng_uniform, time(NULL));
	}

	own_rng = false;
	selected_rng = rng_uniform;
}

NormalRNG::~NormalRNG()
{
	if (own_rng)
		gsl_rng_free(selected_rng);
}

real_t NormalRNG::gaussian(real_t sigma)
{
	double res = gsl_ran_gaussian(selected_rng, (double)sigma);
		
	return static_cast<real_t>(res);
}