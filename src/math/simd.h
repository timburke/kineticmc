#ifndef __simd_h__
#define __simd_h__

#include <emmintrin.h>

#define simd_broadcast_int(x, i) _mm_shuffle_epi32(x, (i << 6) | (i << 4) | (i << 2) | i)

inline __m128i simd_mul_i32(const __m128i &a, const __m128i &b)
{
    __m128i tmp1 = _mm_mul_epu32(a,b); /* mul 2,0*/
    __m128i tmp2 = _mm_mul_epu32( _mm_srli_si128(a,4), _mm_srli_si128(b,4)); /* mul 3,1 */
    return _mm_unpacklo_epi32(_mm_shuffle_epi32(tmp1, _MM_SHUFFLE (0,0,2,0)), _mm_shuffle_epi32(tmp2, _MM_SHUFFLE (0,0,2,0))); /* shuffle results to [63..0] and pack */
}

/*
 * Compute the inverse distance 1/sqrt(d2float) from d2float which is x^2+y^2+z^2
 */
 
inline __m128 simd_inv_dist(const __m128 d2float)
{	
	//Get first estimate
	__m128 estimate = _mm_rsqrt_ps(d2float);

	//Now do one round of newton raphson
	//aprox = rsqrt(arg)
	//Newton-Raphson invsqrt	= (3-(approx*(approx*arg))*approx*0.5
	
	__m128 three = _mm_set1_ps(3.0);
	__m128 half = _mm_set1_ps(0.5);
	
	return _mm_mul_ps(_mm_sub_ps(three, _mm_mul_ps(_mm_mul_ps(d2float,estimate), estimate)), _mm_mul_ps(estimate, half));
}							

inline __m128 simd_not(const __m128 &in)
{
	__m128i tmp;
	
	tmp = _mm_cmpeq_epi8(tmp, tmp); //Set to all 1 bits
	
	return _mm_xor_ps(in, _mm_castsi128_ps(tmp));
}

/*
 * Return (in1 & mask) | (in2 & ~mask)
 * If mask is 0xFFFFFFFF or 0x00000000 then this just chooses in1 or in2 based on the value of mask.
 * Otherwise, it interleaves the bits of in1 and in2 based on the mask.
 */
inline __m128 simd_pick(const __m128 &in1, const __m128 &in2, const __m128 &mask)
{
	/*__m128 invmask = simd_not(mask);
	//NB when using this old version (most likely) a gcc compiler bug gives incorrect results when this function is inlined
	return _mm_or_ps(_mm_and_ps(in1, mask), _mm_and_ps(in2, invmask)); //recomb&mask | prop & invmask*/
	
	//New version from http://graphics.stanford.edu/~seander/bithacks.html#MaskedMerge
	return _mm_xor_ps(in2, _mm_and_ps(_mm_xor_ps(in1, in2), mask));
}

inline __m128 simd_dot(const __m128 &x, const __m128 &y, const __m128 &z,
					   const __m128 other)
{
	__m128 xd;
	__m128 yd;
	__m128 zd;
	
	__m128 otherx;
	__m128 othery;
	__m128 otherz;
	
	otherx = other;
	otherx = _mm_shuffle_ps(otherx, otherx, 0x00);
	othery = other;
	othery = _mm_shuffle_ps(othery, othery, 0x55);
	otherz = other;
	otherz = _mm_shuffle_ps(otherz, otherz, 0xAA);
	
	xd = _mm_mul_ps(x, otherx);
	yd = _mm_mul_ps(y, othery);
	zd = _mm_mul_ps(z, otherz);
	
	return _mm_add_ps(xd, _mm_add_ps(yd, zd));
}

#endif