//Given a function, sample onto a 3D grid for easy access

#ifndef __SampledFunction_h__
#define __SampledFunction_h__

#include "Types.h"

class SampledFunction
{
	private:
	real_t fill;
	real_t *values;
	coord_t dims[3];

	coord_t row_stride;
	coord_t plane_stride;

	void allocate();
	coord_t offset(coord_t *pos);

	public:
	SampledFunction();
	~SampledFunction();

	void 	set_domain(size_t x, size_t y, size_t z);
	void 	set_fill(real_t filler);
	
	void 	set(coord_t *pos, real_t value);
	void	set1d(coord_t pos, real_t value);

	real_t  get(coord_t *pos);
	real_t  get1d(coord_t pos);
	bool	contains(coord_t *pos);
};

#endif