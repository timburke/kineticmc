//basic_math.cpp

#include "basic_math.h"
#include <math.h>
#include <stdlib.h>

real_t dot(real_t *v1, real_t *v2)
{
	real_t d = 0.0;
	
	for (size_t i=0; i<3; ++i)
		d += v1[i]*v2[i];
		
	return d;
}

void normalize(real_t *v)
{
	real_t r2 = dot(v, v);
	real_t r = sqrt(r2);
	
	for (size_t i=0; i<3; ++i)
		v[i] /= r;
}