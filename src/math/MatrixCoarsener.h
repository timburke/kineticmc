//MatrixCoarsener.h

#ifndef __MatrixCoarsener_h__
#define __MatrixCoarsener_h__

#include <stdint.h>
#include <vector>

class MatrixCoarsener
{
	private:
	std::vector<int64_t>	forward_map;
	std::vector<uint64_t>	reverse_ptrs;
	std::vector<uint64_t>	reverse_vals;

	std::vector<int64_t>	coarse_rows;

	uint64_t 				num_rows;
	int64_t					next_row;
	int64_t					last_unmapped;
	uint64_t 				max_coarse;

	int  alloc_row();
	int64_t get_coarse_row(int coarse);

	public:
	MatrixCoarsener(int num_rows, int max_coarse);

	int  next_unmapped();
	void aggregate(int fine, int coarse);

	void clear_map();

	void build_reverse_map();
	int num_aggregates();

	
	std::vector<int> reverse_map(int coarse_id);
	std::vector<int> reverse_map_row(int row);
	int 			 map(int row);
};

#endif