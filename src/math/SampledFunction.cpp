//SampledFunction.cpp

#include "SampledFunction.h"

SampledFunction::SampledFunction()
{
	values = NULL;
	fill = 0.0;

	for (size_t i=0; i<3; ++i)
		dims[i] = 0;
}

SampledFunction::~SampledFunction()
{
	if (values)
		delete[] values;
}

void SampledFunction::allocate()
{
	if (values)
	{
		delete[] values;
		values = NULL;
	}

	values = new real_t[dims[0]*dims[1]*dims[2]];
}

void SampledFunction::set_domain(size_t x, size_t y, size_t z)
{
	dims[0] = x;
	dims[1] = y;
	dims[2] = z;

	row_stride = x;
	plane_stride = x*y;

	allocate();
}

void SampledFunction::set_fill(real_t filler)
{
	fill = filler;
}

coord_t SampledFunction::offset(coord_t *pos)
{
	return pos[2]*plane_stride + pos[1]*row_stride + pos[0];
}
	
void SampledFunction::set(coord_t *pos, real_t value)
{
	if (!contains(pos))
		return;

	values[offset(pos)] = value;
}

real_t SampledFunction::get(coord_t *pos)
{
	if (!contains(pos))
		return fill;

	return values[offset(pos)];
}

void SampledFunction::set1d(coord_t pos, real_t value)
{
	if (pos >= dims[0])
		return;

	values[pos] = value;
}

real_t SampledFunction::get1d(coord_t pos)
{
	if (pos >= dims[0])
		return fill;

	return values[pos];
}

bool SampledFunction::contains(coord_t *pos)
{
	for (size_t i=0; i<3; ++i)
	{
		if (pos[i] < 0 || pos[i] >= dims[i])
			return false;
	}

	return true;
}
