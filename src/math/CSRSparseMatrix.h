//CSRSparseMatrix.h

#ifndef __CSRSparseMatrix_h__
#define __CSRSparseMatrix_h__

#include "BasicTypes.h"
#include "DenseVector.h"
#include <vector>
#include <stdio.h>
#include "gmm/gmm.h"
#include "MarkovTypes.h"

#define kSparseMatrixMagic			0x5858DADAEEFF0011

enum SparseMatrixError
{
	kNoError = 0,
	kInvalidVectorSize,
	kWrongFileSize,
	kFileIOError,
	kInvalidFileMagic,
	kWrongNumberSize
};

#pragma pack(push,1)
struct CSRFileHeader
{
	uint64_t magic;
	uint64_t valsize;
	uint64_t indexsize;

	uint64_t dim;
	uint64_t n_elems;
};
#pragma pack(pop)

template <typename IndexType, typename ValueType>
class CSRSparseMatrix
{
	public:
	typedef gmm::csr_matrix<ValueType> 		MatrixType;
	typedef gmm::ilutp_precond<MatrixType> 	ILUPreconditioner;
	typedef void (*GMMCallbackType)(const gmm::iteration&);
	typedef std::vector<IndexType> IndexVector;
	typedef std::vector<ValueType> ValueVector;

	IndexVector cols;
	IndexVector rowptrs;
	ValueVector vals;

	private:
	IndexType dim;
	IndexType n_elems;

	//GMM++ Interaction Routines
	MatrixType			mat;
	ILUPreconditioner	pre;

	bool have_preconditioner;

	std::vector<double> residuals;

	void to_gmm();
	void from_gmm();

	public:
	CSRSparseMatrix();
	CSRSparseMatrix(const std::string &filename);
	CSRSparseMatrix(IndexVector &cols, IndexVector &rowptrs, ValueVector &vals, bool own=true);

	SparseMatrixError init_from_vectors(IndexVector &nc, IndexVector &rp, ValueVector &v, bool own);

	//File IO Routines
	SparseMatrixError save(const std::string &filename);
	SparseMatrixError load(const std::string &filename);

	//Preconditioning Routines
	void build_preconditioner(int fill_in, double tol);
	void clear_preconditioner();

	int rows();
	int num_elems();

	IndexType 	row_length(IndexType row);
	IndexType 	row_iter_cols(IndexType row, IndexType offset);
	double 		row_iter_vals(IndexType row, IndexType offset);
	void 		set(IndexType row, IndexType offset, double value);

	size_t mem_size();

	//GMM Solving Routines
	bool gmres(ValueVector &rhs, ValueVector &answer, unsigned int restart, markov_real tol);
	bool gmres(DenseVector<ValueType> &rhs, DenseVector<ValueType> &answer, unsigned int restart, markov_real tol);

	//Control Routines
	void clear();

	void np_cols(int *n, int n_dim);
	void np_rowptrs(int *n, int n_dim);
	void np_values(double *n, int n_dim);

	static void gmres_callback(const gmm::iteration &iter);

	int residual_len();
	void np_residuals(double *n, int n_dim);
};

#include "CSRSparseMatrix.txx"

#endif
