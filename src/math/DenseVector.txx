#include <stdio.h>

template <typename ValueType>
void DenseVector<ValueType>::initialize(std::vector<ValueType> &other)
{
	data.clear();
	data.swap(other);
}

template <typename ValueType>
DenseVector<ValueType>::DenseVector(std::vector<ValueType> &other)
{
	this->initialize(other);
}

template <typename ValueType>
DenseVector<ValueType>::DenseVector()
{
}

template <typename ValueType>
DenseVector<ValueType>::DenseVector(size_t size)
{
	data.resize(size);
}

template <typename ValueType>
DenseVector<ValueType>::DenseVector(const std::string &filename)
{
	this->load(filename);
}

template <typename ValueType>
void DenseVector<ValueType>::set(ValueType v)
{
	for (size_t i=0; i<data.size(); ++i)
		data[i] = v;
}

template <typename ValueType>
void DenseVector<ValueType>::np_vector(double *n, int n_dim)
{
	size_t dim = n_dim;

	if (data.size() < dim)
		dim = data.size();

	for (size_t i=0; i<dim; ++i)
		n[i] = data[i];
}

template <typename ValueType>
int DenseVector<ValueType>::len()
{
	return data.size();
}

template <typename ValueType>
std::vector<ValueType>& DenseVector<ValueType>::get_vector()
{
	return data;
}

template <typename ValueType>
bool DenseVector<ValueType>::load(const std::string &filename)
{
	VectorHeader 	header;
	FILE 			*f;
	uint64_t		num;

	f = fopen(filename.c_str(), "r");

	if (!f)
		return false;

	num = fread(&header, sizeof(VectorHeader), 1, f);
	if (num != 1)
	{
		fclose(f);
		return false;
	}

	if (header.magic != kDenseVectorMagic || header.size != sizeof(ValueType))
	{
		fclose(f);
		return false;
	}

	data.resize(header.length);

	num = fread(data.data(), sizeof(ValueType), header.length, f);
	if (num != header.length)
	{
		fclose(f);
		data.clear();
		return false;
	}

	fclose(f);

	return true;
}

template <typename ValueType>
ValueType& DenseVector<ValueType>::operator[](size_t index)
{
	return data[index];
}

template <typename ValueType>
const ValueType& DenseVector<ValueType>::operator[](size_t index) const
{
	return data[index];
}

template <typename ValueType>
bool DenseVector<ValueType>::save(const std::string &filename)
{
	VectorHeader 	header;
	FILE 			*f;
	uint64_t		num;

	header.magic = kDenseVectorMagic;
	header.size = sizeof(ValueType);
	header.length = data.size();

	f = fopen(filename.c_str(), "w");

	if (!f)
		return false;

	num = fwrite(&header, sizeof(header), 1, f);
	if (num != 1)
	{
		fclose(f);
		return false;
	}

	num = fwrite(data.data(), sizeof(ValueType), data.size(), f);
	if (num != header.length)
	{
		fclose(f);
		return false;
	}

	fclose(f);

	return true;
}

template <typename ValueType>
void DenseVector<ValueType>::set(int index, ValueType v)
{
	data[index] = v;
}