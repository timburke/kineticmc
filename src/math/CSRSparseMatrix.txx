#include "CSRSparseMatrix.h"

template <typename IndexType, typename ValueType>
CSRSparseMatrix<IndexType, ValueType>::CSRSparseMatrix()
{
	dim = 0;
	n_elems = 0;
	
	clear_preconditioner();
}

template <typename IndexType, typename ValueType>
CSRSparseMatrix<IndexType, ValueType>::CSRSparseMatrix(const std::string &filename)
{
	this->load(filename);
	
	clear_preconditioner();
}

template <typename IndexType, typename ValueType>
CSRSparseMatrix<IndexType, ValueType>::CSRSparseMatrix(IndexVector &cols, IndexVector &rowptrs, ValueVector &vals, bool own)
{
	init_from_vectors(cols, rowptrs, vals, own);
	
	clear_preconditioner();
}

template <typename IndexType, typename ValueType>
void CSRSparseMatrix<IndexType, ValueType>::clear()
{
	dim = 0;
	n_elems = 0;

	rowptrs.resize(0);
	cols.resize(0);
	vals.resize(0);
}

template <typename IndexType, typename ValueType>
SparseMatrixError CSRSparseMatrix<IndexType, ValueType>::init_from_vectors(IndexVector &nc, IndexVector &rp, ValueVector &v, bool own)
{
	clear();

	if (nc.size() != v.size())
	{
		printf("Invalid vectors used to create sparse matrix, col and val size did not match (%zu and %zu)\n", nc.size(), v.size());
		return kInvalidVectorSize;
	}

	if (own)
	{
		rowptrs.swap(rp);
		cols.swap(nc);
		vals.swap(v);
	}
	else
	{
		rowptrs = rp;
		cols = nc;
		vals = v;
	}

	dim = rowptrs.size() - 1;
	n_elems = vals.size();

	clear_preconditioner();

	return kNoError;
}

template <typename IndexType, typename ValueType>
void CSRSparseMatrix<IndexType, ValueType>::clear_preconditioner()
{
	pre.clear();
	have_preconditioner = false;
}

template <typename IndexType, typename ValueType>
int CSRSparseMatrix<IndexType, ValueType>::rows()
{
	return dim;
}

template <typename IndexType, typename ValueType>
int CSRSparseMatrix<IndexType, ValueType>::num_elems()
{
	return n_elems;
}

template <typename IndexType, typename ValueType>
SparseMatrixError CSRSparseMatrix<IndexType, ValueType>::load(const std::string &filename)
{
	FILE *f = fopen(filename.c_str(), "r");

	if (!f)
		return kFileIOError;

	CSRFileHeader header;

	if (fread(&header, sizeof(CSRFileHeader), 1, f) != 1)
	{
		fclose(f);
		return kFileIOError;
	}

	if (header.magic != kSparseMatrixMagic)
	{
		fclose(f);
		return kInvalidFileMagic;
	}

	if (header.valsize != sizeof(ValueType) || header.indexsize != sizeof(IndexType))
	{
		fclose(f);
		return kWrongNumberSize;
	}

	clear();

	dim = header.dim;
	n_elems = header.n_elems;

	cols.resize(n_elems);
	vals.resize(n_elems);
	rowptrs.resize(dim+1);

	if (fread(cols.data(), sizeof(IndexType), n_elems, f) != n_elems)
	{
		fclose(f);
		clear();
		return kFileIOError;
	}

	if (fread(vals.data(), sizeof(ValueType), n_elems, f) != n_elems)
	{
		fclose(f);
		clear();
		return kFileIOError;
	}

	if (fread(rowptrs.data(), sizeof(IndexType), dim+1, f) != (dim+1))
	{
		fclose(f);
		clear();
		return kFileIOError;
	}

	fclose(f);
	clear_preconditioner();

	return kNoError;
}

template <typename IndexType, typename ValueType>
SparseMatrixError CSRSparseMatrix<IndexType, ValueType>::save(const std::string &filename)
{
	FILE *f = fopen(filename.c_str(), "w");

	if (!f)
		return kFileIOError;

	CSRFileHeader header;

	header.magic = kSparseMatrixMagic;
	header.valsize = sizeof(ValueType);
	header.indexsize = sizeof(IndexType);
	header.dim = dim;
	header.n_elems = n_elems;

	if (fwrite(&header, sizeof(CSRFileHeader), 1, f) != 1)
	{
		fclose(f);
		return kFileIOError;
	}

	if (fwrite(cols.data(), sizeof(IndexType), n_elems, f) != n_elems)
	{
		fclose(f);
		return kFileIOError;
	}

	if (fwrite(vals.data(), sizeof(ValueType), n_elems, f) != n_elems)
	{
		fclose(f);
		return kFileIOError;
	}

	if (fwrite(rowptrs.data(), sizeof(IndexType), dim+1, f) != (dim+1))
	{
		fclose(f);
		return kFileIOError;
	}

	fclose(f);
	return kNoError;
}

template <typename IndexType, typename ValueType>
void CSRSparseMatrix<IndexType, ValueType>::to_gmm()
{
	if (sizeof(IndexType) != sizeof(typename MatrixType::IND_TYPE))
		throw std::string("Index type of sparse matrix does not match sizeof(unsigned int) for GMM++ usage. FAILING\n");

	mat.pr.clear();
	mat.ir.clear();
	mat.jc.clear();

	mat.nc = dim;
	mat.nr = dim;

	mat.pr.swap(vals);
	mat.ir.swap(cols);
	mat.jc.swap(rowptrs);
}

template <typename IndexType, typename ValueType>
void CSRSparseMatrix<IndexType, ValueType>::from_gmm()
{
	if (sizeof(IndexType) != sizeof(typename MatrixType::IND_TYPE))
		throw std::string("Index type of sparse matrix does not match sizeof(unsigned int) for GMM++ usage. FAILING\n");

	vals.clear();
	cols.clear();
	rowptrs.clear();

	vals.swap(mat.pr);
	cols.swap(mat.ir);
	rowptrs.swap(mat.jc);

	dim = mat.nc;
	n_elems = cols.size();
}

template <typename IndexType, typename ValueType>
size_t CSRSparseMatrix<IndexType, ValueType>::mem_size()
{
	size_t size = 0;

	size += pre.memsize();
	size += vals.size()*sizeof(ValueType);
	size += (cols.size() + rowptrs.size())*sizeof(IndexType);

	return size;
}

template <typename IndexType, typename ValueType>
void CSRSparseMatrix<IndexType, ValueType>::build_preconditioner(int fill_in, double tol)
{
	to_gmm();
	pre.build_with(mat, fill_in, tol);
	from_gmm();

	have_preconditioner = true;
}

template <typename IndexType, typename ValueType>
void CSRSparseMatrix<IndexType, ValueType>::gmres_callback(const gmm::iteration &iter)
{
	CSRSparseMatrix<IndexType, ValueType> *me = (CSRSparseMatrix<IndexType, ValueType> *)(iter.get_userdata());

	if (me == NULL)
		return;

	double rhs_norm = iter.get_rhsnorm();

	if (rhs_norm == 0.0)
		rhs_norm = 1.0;

	me->residuals.push_back(iter.get_res()/rhs_norm);
}

/*
 * Solve the matrix problem using GMRES as implementied in GMM++ library, optionally
 * applying a preconditioner.
 */

template <typename IndexType, typename ValueType>
bool CSRSparseMatrix<IndexType, ValueType>::gmres(ValueVector &rhs, ValueVector &answer, unsigned int restart, markov_real tol)
{
	to_gmm();

	gmm::iteration iter(tol);

	iter.set_noisy(1);
	iter.set_userdata(this);
	iter.set_callback(CSRSparseMatrix<IndexType, ValueType>::gmres_callback);

	residuals.clear();

	if (answer.size() != rhs.size())
	{
		from_gmm();
		return false;
	}

	if (answer.size() != dim)
	{
		from_gmm();
		return false;
	}

	if (have_preconditioner)
		gmm::gmres(mat, answer, rhs, pre, restart, iter);
	else
		gmm::gmres(mat, answer, rhs, gmm::identity_matrix(), restart, iter);

	if (iter.converged())
	{
		from_gmm();
		return true;
	}

	from_gmm();
	return false;
}

template <typename IndexType, typename ValueType>
bool CSRSparseMatrix<IndexType, ValueType>::gmres(DenseVector<ValueType> &rhs, DenseVector<ValueType> &answer, unsigned int restart, markov_real tol)
{
	return gmres(rhs.get_vector(), answer.get_vector(), restart, tol);
}

template <typename IndexType, typename ValueType>
int CSRSparseMatrix<IndexType, ValueType>::residual_len()
{
	return residuals.size();
}


template <typename IndexType, typename ValueType>
void CSRSparseMatrix<IndexType, ValueType>::np_residuals(double *n, int n_dim)
{
	size_t dim = n_dim;

	if (residuals.size() < dim)
		dim = residuals.size();

	for (size_t i=0; i<dim; ++i)
		n[i] = residuals[i];
}

template <typename IndexType, typename ValueType>
void CSRSparseMatrix<IndexType, ValueType>::np_cols(int *n, int n_dim)
{
	int d = n_dim;

	if (n_elems < d)
		d = n_elems;

	for (int i=0; i<d; ++i)
		n[i] = cols[i];
}

template <typename IndexType, typename ValueType>
void CSRSparseMatrix<IndexType, ValueType>::np_rowptrs(int *n, int n_dim)
{
	int d = n_dim;

	if ((dim+1) < d)
		d = dim+1;

	for (int i=0; i<d; ++i)
		n[i] = rowptrs[i];
}

template <typename IndexType, typename ValueType>
void CSRSparseMatrix<IndexType, ValueType>::np_values(double *n, int n_dim)
{
	int d = n_dim;

	if (n_elems < d)
		d = n_elems;

	for (int i=0; i<d; ++i)
		n[i] = vals[i];
}

template <typename IndexType, typename ValueType>
IndexType CSRSparseMatrix<IndexType, ValueType>::row_length(IndexType row)
{
	if (row >= dim)
		return 0;

	return rowptrs[row+1] - rowptrs[row];
}

template <typename IndexType, typename ValueType>
double CSRSparseMatrix<IndexType, ValueType>::row_iter_vals(IndexType row, IndexType offset)
{
	if (row >= dim)
		return 0.0;

	return vals[rowptrs[row] + offset];
}

template <typename IndexType, typename ValueType>
IndexType CSRSparseMatrix<IndexType, ValueType>::row_iter_cols(IndexType row, IndexType offset)
{
	if (row >= dim)
		return 0;

	return cols[rowptrs[row] + offset];
}

template <typename IndexType, typename ValueType>
void CSRSparseMatrix<IndexType, ValueType>::set(IndexType row, IndexType offset, double value)
{
	IndexType off = rowptrs[row] + offset;

	if (off >= n_elems)
		return;

	vals[off] = value;
}