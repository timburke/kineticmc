//Poisson3D.h

#ifndef __Poisson3D_h__
#define __Poisson3D_h__

#include <fftw3.h>

/*
 * PoissonSolver3D
 * A class for solving poisson's equation in a 3D cartesian grid
 * subject to periodic BC in the x,y directions and fixed voltage conditions
 * in the Z direction.  This is meant principally to be useful for calculating
 * electric fields inside organic bulk-heterojunction solar cells.
 */
 
enum BoundaryCondition
{
	kDirichlet = 0,
	kPeriodic
};

class PoissonSolver3D
{
	private:
	fftw_complex *rho_k; 	//Fourier transform of the charge density

	BoundaryCondition x_bnd;
	BoundaryCondition y_bnd;
	BoundaryCondition z_bnd;

	double *x_coeff;
	double *y_coeff;
	double *z_coeff;		
	
	size_t		 dims[3];
	size_t		 max_dim;
	
	double		 h;		 	//lattice site spacing, which must be isotropic (in meters)
	double		 eps;
	double		 norm;
	
	//FFT Plans
	fftw_plan x_planf; //Compute exponential dft on all x lines
	fftw_plan x_plani;
	fftw_plan y_planf; //Compute exponential dft on all y lines
	fftw_plan y_plani;
	fftw_plan z_planf; //Compute discrete sine transform on all z lines
	fftw_plan z_plani;
	
	//Utility Functions
	void initialize();
	void destroy();
	void set_coeffs();
	void order_ffts(fftw_plan *forw, fftw_plan *back);
	
	public:
	PoissonSolver3D(size_t x_dim, size_t y_dim, size_t z_dim, double spacing, double e, BoundaryCondition x = kPeriodic, BoundaryCondition y = kPeriodic, BoundaryCondition z=kDirichlet); //spacing in meters, eps in SI
	~PoissonSolver3D();
	
	void zero_rho();
	
	void create_point_charge(size_t x, size_t y, size_t z, double q);
	
	
	void solve();
	
	//Debug Functions
	void save(const char *filename, bool real=true);
	void save_nrrd(const char *filename);
	
	//Static Initialization Functions
	static void initialize_fftw();
	static void set_threads(int num);
	static void cleanup_fftw();
};
		

#endif