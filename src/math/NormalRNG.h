/*
 * NormalRNG
 * A class for generating normally distributed random numbers using GSL.
 *
 */
 
#ifndef __NormalRNG_h__
#define __NormalRNG_h__

#include <gsl/gsl_rng.h>
#include "BasicTypes.h"

/*WARNING: NOT THREADSAFE UNLESS gsl_rng is because we have a static rng to handle the case
 of creating a large number of rngs before time(NULL) updates itself so they are all seeded with the same
 value

	Creating NormalRNG with a nonzero seed will deterministically seed the rng with that value and the seed
	will be only used for the created RNG rather than using the global one.  This allows easily reproducing 
	random sequences later.
 */

class NormalRNG
{
	private:
	static gsl_rng *rng_uniform;

	gsl_rng 	*selected_rng;
	bool 		own_rng;
	
	void use_global();

	public:
	NormalRNG();
	NormalRNG(long seed);
	~NormalRNG();
	
	real_t gaussian(real_t sigma);
};

#endif