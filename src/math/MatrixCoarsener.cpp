#include "MatrixCoarsener.h"
#include "BasicTypes.h"
#include "IndirectCompare.hxx"
#include <algorithm>
#include <stddef.h>

MatrixCoarsener::MatrixCoarsener(int n_r, int m_c) : num_rows(n_r), next_row(0), last_unmapped(0), max_coarse(m_c)
{
	forward_map.resize(num_rows);
	coarse_rows.resize(max_coarse);
	clear_map();
}

int  MatrixCoarsener::alloc_row()
{
	return next_row++;
}

int64_t MatrixCoarsener::get_coarse_row(int coarse)
{
	if (coarse < 0 || coarse >= coarse_rows.size())
		return -1;

	if (coarse_rows[coarse] == -1)
		coarse_rows[coarse] = alloc_row();

	return coarse_rows[coarse];
}

void MatrixCoarsener::aggregate(int fine, int coarse)
{
	if (fine < 0 || fine >= forward_map.size())
		return;

	forward_map[fine] = get_coarse_row(coarse);
}

void MatrixCoarsener::clear_map()
{
	for(size_t i=0; i<forward_map.size(); ++i)
		forward_map[i] = -1;

	for (size_t i=0; i<coarse_rows.size(); ++i)
		coarse_rows[i] = -1;

	last_unmapped = 0;
	next_row = 0;
	reverse_ptrs.resize(0);
	reverse_vals.resize(0);
}

int  MatrixCoarsener::next_unmapped()
{
	for (size_t i = last_unmapped; i < forward_map.size(); ++i)
	{
		if (forward_map[i] == -1)
		{
			last_unmapped = i;
			return i;
		}
	}

	return -1;
}

void MatrixCoarsener::build_reverse_map()
{
	std::vector<uint64_t> indices;

	indices.resize(num_rows);

	for (size_t i=0; i<num_rows; ++i)
		indices[i] = i;

	IndirectComparator<uint64_t, int64_t> cmp(forward_map);

	std::sort(indices.begin(), indices.end(), cmp);

	//If you use indices now to access forward_map, you get results sorted by the aggregate number
	uint64_t curr_agg = -1LL;
	int64_t out_i;

	reverse_ptrs.resize(num_aggregates() + 1);
	reverse_vals.resize(num_rows);

	for(size_t out_i = 0; out_i < num_rows; ++out_i)
	{
		if (forward_map[indices[out_i]] != curr_agg)
		{
			curr_agg = forward_map[indices[out_i]];
			reverse_ptrs[curr_agg] = out_i;
		}

		reverse_vals[out_i] = indices[out_i];
	}

	reverse_ptrs[num_aggregates()] = num_rows;
}

int MatrixCoarsener::map(int row)
{
	if (row < 0 || row >= forward_map.size())
		return -1;

	return forward_map[row];
}

std::vector<int> MatrixCoarsener::reverse_map(int coarse)
{
	if (coarse < 0 || coarse >= coarse_rows.size())
		return std::vector<int>();

	int64_t row = coarse_rows[coarse];

	return reverse_map_row(row);
}

std::vector<int> MatrixCoarsener::reverse_map_row(int row)
{
	if (row < 0 || row >= num_aggregates())
		return std::vector<int>();

	std::vector<int> rows;

	rows.resize(reverse_ptrs[row+1] - reverse_ptrs[row]);

	for (size_t i=0; i<rows.size(); ++i)
	{
		size_t index = i + reverse_ptrs[row];

		rows[i] = reverse_vals[index];
	}

	return rows;
}

int MatrixCoarsener::num_aggregates()
{
	return next_row;
}