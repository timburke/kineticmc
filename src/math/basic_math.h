//basic_math.h

#ifndef __basic_math_h__
#define __basic_math_h__

#include "BasicTypes.h"

void normalize(real_t *v);
real_t dot(real_t *v1, real_t *v2);

#endif