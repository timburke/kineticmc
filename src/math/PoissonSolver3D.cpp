//PoissonSolver3D.cpp

#define _USE_MATH_DEFINES

#include "PoissonSolver3D.h"
#include "ImageHelper.hxx"
#include <stdlib.h>
#include <string.h>
#include <complex>
#include <cmath>
#include <stdio.h>

using namespace std;

PoissonSolver3D::PoissonSolver3D(size_t x_dim, size_t y_dim, size_t z_dim, double spacing, double e, BoundaryCondition x, BoundaryCondition y, BoundaryCondition z)
	: h(spacing), eps(e*8.85418782e-12), x_bnd(x), y_bnd(y), z_bnd(z)
{
	dims[0] = x_dim;
	dims[1] = y_dim;
	dims[2] = z_dim;
	
	initialize();
}

PoissonSolver3D::~PoissonSolver3D()
{
	destroy();
}

void PoissonSolver3D::set_coeffs()
{
	for (size_t i=0; i<dims[0]; ++i)
	{
		double coeff = 1.0;
		size_t off  = 0;
		
		if (x_bnd == kDirichlet)
		{
			coeff = 4.0;
			off = 1;
		}
			
		x_coeff[i] = pow(sin((i+off)*M_PI/(coeff*(dims[0]+off))),2.0);
	}
	
	for (size_t i=0; i<dims[1]; ++i)
	{
		double coeff = 1.0;
		size_t off  = 0;
		
		if (y_bnd == kDirichlet)
		{
			coeff = 4.0;
			off = 1;
		}
			
		y_coeff[i] = pow(sin((i+off)*M_PI/(coeff*(dims[1]+off))),2.0);
	}

	for (size_t i=0; i<dims[2]; ++i)
	{
		double coeff = 1.0;
		size_t off  = 0;
		
		if (z_bnd == kDirichlet)
		{
			coeff = 4.0;
			off = 1;
		}
			
		z_coeff[i] = pow(sin((i+off)*M_PI/(coeff*(dims[2]+off))),2.0);
	}
}

void PoissonSolver3D::initialize()
{
	//Find the maximal dimension
	max_dim = 0;
	
	for (size_t i=0; i<3; ++i)
	{
		if (dims[i] > max_dim)
			max_dim = dims[i];
	}
	
	norm = 1.0;
	
	//Allocate all the memory
	x_coeff = fftw_alloc_real(dims[0]);
	y_coeff = fftw_alloc_real(dims[1]);
	z_coeff = fftw_alloc_real(dims[2]);
	
	rho_k = fftw_alloc_complex(dims[0]*dims[1]*dims[2]);
	
	//Build our FFT plans
	fftw_iodim size;
	fftw_iodim ranks[2];
	
	set_coeffs();
	
	size_t spacing;
	
	fftw_r2r_kind fkind = FFTW_RODFT00;
	fftw_r2r_kind ikind = FFTW_RODFT00;
	
	if (x_bnd == kDirichlet)
		spacing = 2;
	else
		spacing = 1;
	
	//X Dimension
	size.n = dims[0];			//x dim
	size.is = dims[1]*dims[2]*spacing;
	size.os = dims[1]*dims[2]*spacing;
	ranks[0].n = dims[1];		//y dim
	ranks[0].is = dims[2]*spacing;
	ranks[0].os = dims[2]*spacing;
	ranks[1].n = dims[2];		//z dim
	ranks[1].is = spacing;
	ranks[1].os = spacing;
	
	if (x_bnd == kDirichlet)
	{
		x_planf = fftw_plan_guru_r2r(1, &size, 2, ranks, (double*)rho_k, ((double*)rho_k), &fkind, FFTW_MEASURE);
		x_plani = fftw_plan_guru_r2r(1, &size, 2, ranks, ((double*)rho_k), (double*)rho_k, &ikind, FFTW_MEASURE);
		norm*= 2*(dims[0]+1);
	}
	else
	{
		x_planf = fftw_plan_guru_dft(1, &size, 2, ranks, rho_k, rho_k, FFTW_FORWARD, FFTW_MEASURE);
		x_plani = fftw_plan_guru_dft(1, &size, 2, ranks, rho_k, rho_k, FFTW_BACKWARD, FFTW_MEASURE);
		norm *= dims[0];
	}
	
	if (y_bnd == kDirichlet)
		spacing = 2;
	else
		spacing = 1;
	
	
	//Y Dimension
	ranks[0].n = dims[0];			//x dim
	ranks[0].is = dims[1]*dims[2]*spacing;
	ranks[0].os = dims[1]*dims[2]*spacing;
	size.n = dims[1];				//y dim
	size.is = dims[2]*spacing;
	size.os = dims[2]*spacing;
	ranks[1].n = dims[2];			//z dim
	ranks[1].is = spacing;
	ranks[1].os = spacing;
	
	if (y_bnd == kDirichlet)
	{
		y_planf = fftw_plan_guru_r2r(1, &size, 2, ranks, (double*)rho_k, ((double*)rho_k), &fkind, FFTW_MEASURE);
		y_plani = fftw_plan_guru_r2r(1, &size, 2, ranks, ((double*)rho_k), (double*)rho_k, &ikind, FFTW_MEASURE);
		norm *= 2*(dims[1]+1);
	}
	else
	{
		y_planf = fftw_plan_guru_dft(1, &size, 2, ranks, rho_k, rho_k, FFTW_FORWARD, FFTW_MEASURE);
		y_plani = fftw_plan_guru_dft(1, &size, 2, ranks, rho_k, rho_k, FFTW_BACKWARD, FFTW_MEASURE);
		norm *= dims[1];
	}
	
	//Z Dimension
	if (z_bnd == kDirichlet)
		spacing = 2;
	else
		spacing = 1;
		
	ranks[0].n = dims[0];			//x dim
	ranks[0].is = dims[1]*dims[2]*spacing;
	ranks[0].os = dims[1]*dims[2]*spacing;
	ranks[1].n = dims[1];				//y dim
	ranks[1].is = dims[2]*spacing;
	ranks[1].os = dims[2]*spacing;
	size.n = dims[2];			//z dim
	size.is = spacing;
	size.os = spacing;
	
	if (z_bnd == kDirichlet)
	{
		z_planf = fftw_plan_guru_r2r(1, &size, 2, ranks, (double*)rho_k, ((double*)rho_k), &fkind, FFTW_MEASURE);
		z_plani = fftw_plan_guru_r2r(1, &size, 2, ranks, ((double*)rho_k), (double*)rho_k, &ikind, FFTW_MEASURE);
		norm *= 2*(dims[2]+1);
	}
	else
	{
		z_planf = fftw_plan_guru_dft(1, &size, 2, ranks, rho_k, rho_k, FFTW_FORWARD, FFTW_MEASURE);
		z_plani = fftw_plan_guru_dft(1, &size, 2, ranks, rho_k, rho_k, FFTW_BACKWARD, FFTW_MEASURE);
		norm *= dims[2];
	}
}

void PoissonSolver3D::destroy()
{
	fftw_free(rho_k);
	fftw_free(x_coeff);
	fftw_free(y_coeff);
	fftw_free(z_coeff);
	
	fftw_destroy_plan(x_plani);
	fftw_destroy_plan(x_planf);
	fftw_destroy_plan(y_plani);
	fftw_destroy_plan(y_planf);
	fftw_destroy_plan(z_plani);
	fftw_destroy_plan(z_planf);
}

void PoissonSolver3D::save(const char *fname, bool real) 
{
	FILE *f = fopen(fname, "w");
	
	for (size_t i=0; i<dims[0]*dims[1]*dims[2]; ++i)
	{
		if (real)
			fprintf(f, "%lf\n", rho_k[i][0]);
		else
			fprintf(f, "%lf\n", rho_k[i][1]);
	}
	
	fclose(f);
}

void PoissonSolver3D::save_nrrd(const char *fname)
{
	size_t strides[3];
	
	strides[0] = strides[1] = strides[2] = 2;
	
	ImageHelper<double> helper((double*)rho_k, strides, dims);
	
	helper.write_nrrd(fname);
}

void PoissonSolver3D::initialize_fftw()
{
	fftw_init_threads();
}

void PoissonSolver3D::set_threads(int num)
{
	fftw_plan_with_nthreads(num);
}

void PoissonSolver3D::cleanup_fftw()
{
	fftw_cleanup_threads();
}

void PoissonSolver3D::zero_rho()
{
	memset(rho_k, 0, sizeof(double)*2*dims[0]*dims[1]*dims[2]);
}

void PoissonSolver3D::create_point_charge(size_t x, size_t y, size_t z, double q)
{
	size_t i = x*dims[1]*dims[2] + y*dims[2] + z;
	
	double val = q/eps/(h*h*h);
	
	printf("Lattice Spacing: %e\n", h);
	printf("Epsilon: %e\n", eps);
	printf("q: %e\n", q);
	printf("val: %e\n", val);
	
	rho_k[i][0] = val;
}

void PoissonSolver3D::order_ffts(fftw_plan *forw, fftw_plan *back)
{
	size_t i = 0;
	
	if (x_bnd == kDirichlet)
	{
		forw[0] = x_planf;
		back[2] = x_plani;
		
		if (z_bnd == kDirichlet)
		{
			forw[1] = z_planf;
			forw[2] = y_planf;
			
			back[1] = z_plani;
			back[0] = y_plani;
		}
		else
		{
			forw[1] = y_planf;
			forw[2] = z_planf;
			
			back[1] = y_plani;
			back[0] = z_plani;
		}
	}
	else
	{
		forw[2] = x_planf;
		back[0] = x_plani;
		
		if (z_bnd == kDirichlet)
		{
			forw[0] = z_planf;
			forw[1] = y_planf;
			
			back[2] = z_plani;
			back[1] = y_plani;
		}
		else
		{
			forw[0] = y_planf;
			forw[1] = z_planf;
			
			back[2] = y_plani;
			back[1] = z_plani;
		}
	}
}
	
void PoissonSolver3D::solve()
{	
	fftw_plan forw[3], back[3];
	
	//We need to fft the dirichlet boundary conditions first since they are real to real and can't handle complex
	//coeffs, which could be produced in the periodic bc
	order_ffts(forw, back);

	fftw_execute(forw[0]);
	fftw_execute(forw[1]);
	fftw_execute(forw[2]);
		
	for (size_t i=0; i<dims[0]; ++i)
	{
		for (size_t j=0; j<dims[1]; ++j)
		{
			for (size_t k=0; k<dims[2]; ++k)
			{
				double den = x_coeff[i] + y_coeff[j] + z_coeff[k];
				
				if (den != 0.0)
				{						
					double factor = h*h/(4.0*den)/norm;

					rho_k[i*dims[1]*dims[2] + j*dims[2] + k][0] *= factor;
					rho_k[i*dims[1]*dims[2] + j*dims[2] + k][1] *= factor;
				}
				else
				{
					printf("zeroing\n");
					rho_k[i*dims[1]*dims[2] + j*dims[2] + k][0] = 0.0;
					rho_k[i*dims[1]*dims[2] + j*dims[2] + k][1] = 0.0;
				}
			}
		}
	}
		
	fftw_execute(back[0]);
	fftw_execute(back[1]);
	fftw_execute(back[2]);
}