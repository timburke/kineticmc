//DenseVector.h

#ifndef __DenseVector_h__
#define __DenseVector_h__

#include <vector>
#include <string>

#define kDenseVectorMagic			(uint64_t)0xAFEDC496BBEECCAA

struct VectorHeader
{
	uint64_t magic;
	uint64_t size;
	uint64_t length;
};

template <typename ValueType>
class DenseVector
{
	private:
	std::vector<ValueType> 	data;

	void initialize(std::vector<ValueType> &other);

	public:
	DenseVector(std::vector<ValueType> &other);
	DenseVector(const std::string &filename);
	DenseVector();
	DenseVector(size_t size);

	void set(ValueType v);
	void set(int index, ValueType v);

	bool load(const std::string &filename);
	bool save(const std::string &filename);

	std::vector<ValueType>& get_vector();

	void np_vector(double *n, int n_dim);
	int len();

	ValueType& operator[](size_t index);
	const ValueType& operator[](size_t index) const;

};

#include "DenseVector.txx"

#endif