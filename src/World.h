//World.h

#ifndef __World_h__
#define __World_h__

#include "Types.h"
#include <stdio.h>
#include <string>

class World
{
	public:
	virtual ~World() {};
	
	virtual LocalEnvironment &	get_value(coord_t *pos, CarrierType type) = 0; //pos must exist or this will throw an exception
	virtual bool 				value_exists(coord_t *pos, CarrierType type) = 0;
	virtual void				set_value(coord_t *pos, const LocalEnvironment &val, CarrierType type) = 0;
	
	virtual bool 				contains(coord_t *pos, CarrierType type) = 0;
	
	virtual void				reset() = 0; //Clear this world
	
	virtual void 				print_statistics() {};
	virtual void 				write_xyz(const std::string &name) = 0; //write allocated space to xyz file.
	
	virtual bool 				context_free() {return false;}; //Return true if this world doesn't actually store anything (speedup if the lattice site energy levels can be calculated deterministically (and cheaply) from the lattice position
	
	virtual size_t				lattice_size() = 0; //return the number of allocated lattice positions
	
	//Utility functions built on top of the above
	size_t count_neighbors(coord_t *pos);
};

#endif