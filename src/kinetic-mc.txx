//kinetic-mc.txx
//kinetic-mc.txx

template<typename EnergyGetter, typename MobilityGetter>
MCSimulation<EnergyGetter, MobilityGetter>::MCSimulation() : space(NULL), gen(NULL), own_space(false), own_gen(false), context_free(false)
{
	build_jump_tables();
	electron.mobility = NULL;
	hole.mobility = NULL;

	dist_table = NULL;
	max_dist = 0;
	
	progress_l = log4cxx::Logger::getLogger("kineticmc.sim.progress");
	energy_l = log4cxx::Logger::getLogger("kineticmc.sim.calc.energy");
	mobility_l = log4cxx::Logger::getLogger("kineticmc.sim.calc.mobility");
	initial_l = log4cxx::Logger::getLogger("kineticmc.sim.initial");
	
	energy_gen = new EnergyGetter();
	mob_gen = new MobilityGetter();
}

template<typename EnergyGetter, typename MobilityGetter>
MCSimulation<EnergyGetter, MobilityGetter>::~MCSimulation() 
{
	if (own_space && space)
		delete space;
	
	if (own_gen && gen)
		delete gen;
		
	if (electron.mobility)
		delete electron.mobility;
	
	if (hole.mobility)
		delete hole.mobility;
		
	if (dist_table)
		delete[] dist_table;
	
	if (energy_gen)
		delete energy_gen;
	
	if (mob_gen)
		delete mob_gen;
}

//Create an electron and hole initially separated by initial_r length
template<typename EnergyGetter, typename MobilityGetter>
void MCSimulation<EnergyGetter, MobilityGetter>::create_initial_state_bulk()
{
	coord_t sep = roundl(initial_r / lattice_const);
	
	electron.pos[0] = sep/2;
	electron.pos[1] = 0;
	electron.pos[2] = 0;
	
	hole.pos[0] = sep/2 - sep; //Writen this way so that electron.x - hole.x == sep even if its odd
	hole.pos[1] = 0;
	hole.pos[2] = 0;
	
	LOG_TRACE(initial_l, "Initial hole position (" << hole.pos[0] << ", " << hole.pos[1] << ", " << hole.pos[2] << ")");
	LOG_TRACE(initial_l, "Initial electron position (" << electron.pos[0] << ", " << electron.pos[1] << ", " << electron.pos[2] << ")");
	
	assert(electron.pos[0] - hole.pos[0] == sep);
}

template<typename EnergyGetter, typename MobilityGetter>
void MCSimulation<EnergyGetter, MobilityGetter>::create_state(coord_t *elec_pos, coord_t *hole_pos)
{
	for (size_t i=0; i<3; ++i)
	{
		electron.pos[i] = elec_pos[i];
		hole.pos[i] = hole_pos[i];
	}
	
	LOG_DEBUG(initial_l, "Created electron and hole with fixed positions");
	LOG_DEBUG(initial_l, "Initial hole position (" << hole.pos[0] << ", " << hole.pos[1] << ", " << hole.pos[2] << ")");
	LOG_DEBUG(initial_l, "Initial electron position (" << electron.pos[0] << ", " << electron.pos[1] << ", " << electron.pos[2] << ")");
}

template<typename EnergyGetter, typename MobilityGetter>
real_t MCSimulation<EnergyGetter, MobilityGetter>::get_inv_distance(coord_t *p1, coord_t *p2)
{
	coord_t r = 0;
	
	for (size_t i=0; i<3; ++i)
		r += (p1[i] - p2[i])*(p1[i] - p2[i]);
		
	if (r < max_dist)
	{
		return dist_table[r];
	}
	
	return 1.0/(sqrt((real_t)r)*lattice_const);
}

template<typename EnergyGetter, typename MobilityGetter>
real_t MCSimulation<EnergyGetter, MobilityGetter>::dot(real_t *v1, real_t *v2)
{
	real_t d = 0.0;
	
	for (size_t i=0; i<3; ++i)
		d += v1[i]*v2[i];
		
	return d;
}

//Computes (d1 - d2) dot d3
template<typename EnergyGetter, typename MobilityGetter>
real_t MCSimulation<EnergyGetter, MobilityGetter>::dotdiff(real_t *d1, real_t *d2, real_t *d3)
{
	real_t r[3];
	
	for (size_t i=0; i<3; ++i)
		r[i] = d1[i] - d2[i];
		
	return dot(r, d3);
}

template<typename EnergyGetter, typename MobilityGetter>
real_t MCSimulation<EnergyGetter, MobilityGetter>::dotdiff(coord_t *d1, coord_t *d2, real_t *d3)
{
	real_t r[3];
	
	for (size_t i=0; i<3; ++i)
		r[i] = ((real_t)(d1[i] - d2[i]))*lattice_const;
		
	return dot(r, d3);
}

template<typename EnergyGetter, typename MobilityGetter>
void MCSimulation<EnergyGetter, MobilityGetter>::normalize(real_t *v)
{
	real_t r2 = dot(v, v);
	real_t r = sqrt(r2);
	
	for (size_t i=0; i<3; ++i)
		v[i] /= r;
}

template<typename EnergyGetter, typename MobilityGetter>
void MCSimulation<EnergyGetter, MobilityGetter>::set_world_generator(WorldGenerator *g, bool own)
{
	gen = g;
	own_gen = own;
}

template<typename EnergyGetter, typename MobilityGetter>
void MCSimulation<EnergyGetter, MobilityGetter>::set_world(World *w, bool own)
{
	//Fixed memory leak
	if (space && own_space)
		delete space;
	
	space = w;
	own_space = own;
}

template<typename EnergyGetter, typename MobilityGetter>
bool MCSimulation<EnergyGetter, MobilityGetter>::positions_equal(coord_t *p1, coord_t *p2)
{
	for (size_t i=0; i<3; ++i)
	{
		if (p1[i] != p2[i])
			return false;
	}
	
	return true;
}

/*
 * Calculate the energy of the system if the electron is at position epos and the hole is at position hpos.
 */
template<typename EnergyGetter, typename MobilityGetter>
real_t MCSimulation<EnergyGetter, MobilityGetter>::calculate_energy(coord_t *epos, coord_t *hpos)
{	
	real_t inv_r = get_inv_distance(epos, hpos);
	
	//Calculate energy terms
	real_t energy = binding_factor * inv_r; //binding factor is negative.
	
	real_t elec_en, hole_en;
	
	elec_en = energy_gen->electron_energy(epos, lattice_const, space, gen);
	hole_en = energy_gen->hole_energy(hpos, lattice_const, space, gen);
		
	energy += elec_en;
	energy -= hole_en; //holes float
	
	//DONE: Check this equation.  Peumans 2005 reports the opposite but
	//I think they wrote it wrong. U = -u . F (now a year and a paper later, can definitely validate this.)
	energy -= (1.60217646e-19*dotdiff(hpos, 	epos, field)*JOULE_TO_EV);
			
	return energy;
}

template<typename EnergyGetter, typename MobilityGetter>
void MCSimulation<EnergyGetter, MobilityGetter>::build_jump_tables()
{
	inc_table[0] = 1;
	inc_table[1] = -1;
	inc_table[2] = 1;
	inc_table[3] = -1;
	inc_table[4] = 1;
	inc_table[5] = -1;
	
	dir_table[0] = 0;
	dir_table[1] = 0;
	dir_table[2] = 1;
	dir_table[3] = 1;
	dir_table[4] = 2;
	dir_table[5] = 2;
	
	//build the simd increment table
	for (size_t i=0; i<6; ++i)
	{
		cmp.inc_x[i] = 0;
		cmp.inc_y[i] = 0;
		cmp.inc_z[i] = 0;
	}
	
	cmp.inc_x[0] = 1;
	cmp.inc_x[1] = -1;
	cmp.inc_y[2] = 1;
	cmp.inc_y[3] = -1;
	cmp.inc_z[4] = 1;
	cmp.inc_z[5] = -1;
}

template<typename EnergyGetter, typename MobilityGetter>
void MCSimulation<EnergyGetter, MobilityGetter>::set_transition(size_t trans, coord_t *epos, coord_t *hpos, real_t energy)
{
	size_t i = trans;
	
	coord_t *pchange = epos;
	coord_t *pstill = hpos;
	
	real_t attempt_freq;
	
	if (i < 6)
	{
		//We're moving the electron;
		
		attempt_freq = electron.mobility->mobility((Axis)dir_table[i], (Direction)((inc_table[i]+1)/2));
		//LOG_DEBUG(mobility_l, "Electron attempt freq: " << attempt_freq);
	}		
	else
	{
		//We're moving the hole
		i %= 6;
				
		attempt_freq = hole.mobility->mobility((Axis)dir_table[i], (Direction)((inc_table[i]+1)/2));
		//LOG_DEBUG(mobility_l, "Hole attempt freq: " << attempt_freq);
		
		pchange = hpos; //update the hole instead
		pstill = epos; 
	}
	
	//update position for transition
	pchange[dir_table[i]] += inc_table[i];
	
	if (positions_equal(epos, hpos))
	{
		//printf("Could recombine!\n");
		transitions[trans] = recomb_rate;
	}
	else
	{
		real_t newE = calculate_energy(epos, hpos);
		real_t deltaE = newE - energy;
		
		if (deltaE >= 0.0)
			transitions[trans] = attempt_freq * exp(deltaE*minusBeta);
		else
			transitions[trans] = attempt_freq;
	}
	
	pchange[dir_table[i]] -= inc_table[i]; //reset position back to original
}	

template<typename EnergyGetter, typename MobilityGetter>
void MCSimulation<EnergyGetter, MobilityGetter>::calculate_transitions()
{	
	//Initialize positions with current electron and hole positions
	for (size_t i=0; i<3; ++i)
	{
		epos[i] = electron.pos[i];
		hpos[i] = hole.pos[i];
	}
	
	epos[3] = hpos[3] = 0;
	
	real_t energy = calculate_energy(epos, hpos);
	//printf("Energy: %f\n", energy);
	
	for (size_t i=0; i<12; ++i)
		set_transition(i, epos, hpos, energy);
		
	accumulate_rates();
}

template<typename EnergyGetter, typename MobilityGetter>
void MCSimulation<EnergyGetter, MobilityGetter>::calculate_transitions_simd()
{
	//Initialize positions with current electron and hole positions
	for (size_t i=0; i<3; ++i)
	{
		cmp.elec[i] = electron.pos[i];
		cmp.hole[i] = hole.pos[i];
	}
	
	cmp.elec[3] = cmp.hole[3] = 0;
	
	//calculate all the nearest neighbor positions
	for (size_t i=0; i<6; ++i)
	{
		cmp.hole_x[i] = cmp.hole[0];
		cmp.hole_y[i] = cmp.hole[1];
		cmp.hole_z[i] = cmp.hole[2];
		
		cmp.elec_x[i] = cmp.elec[0] + cmp.inc_x[i];
		cmp.elec_y[i] = cmp.elec[1] + cmp.inc_y[i];
		cmp.elec_z[i] = cmp.elec[2] + cmp.inc_z[i];
		
		cmp.hole_x[i+6] = cmp.hole[0] + cmp.inc_x[i];
		cmp.hole_y[i+6] = cmp.hole[1] + cmp.inc_y[i];
		cmp.hole_z[i+6] = cmp.hole[2] + cmp.inc_z[i];
		
		cmp.elec_x[i+6] = cmp.elec[0];
		cmp.elec_y[i+6] = cmp.elec[1];
		cmp.elec_z[i+6] = cmp.elec[2];
	}
	
	//calculate the hopping rates (store in prob, will be overwritten with actual probability in calculate_propensities_simd)
	electron.mobility->fill_mobilities(&cmp.prob[0]);
	hole.mobility->fill_mobilities(&cmp.prob[6]);
		
	//Calculate the new energies and propensities
	calculate_propensities_simd();
	
	accumulate_rates_simd();
}

template<typename EnergyGetter, typename MobilityGetter>
void MCSimulation<EnergyGetter, MobilityGetter>::calc_with_simd(bool val)
{
	this->use_sse = val;
}

template<typename EnergyGetter, typename MobilityGetter>
void MCSimulation<EnergyGetter, MobilityGetter>::calculate_propensities_simd()
{
	//Fill in energy levels
	energy_gen->fill_electron_energies(cmp.elec, lattice_const, space, gen, &cmp.elec_levels[0]);
	energy_gen->fill_hole_energies(cmp.hole, lattice_const, space, gen, &cmp.hole_levels[6]);
	
	//for 6 of the transitions the hole or elec doesn't move
	for (size_t i=0; i<6; ++i)
	{
		cmp.hole_levels[i] = cmp.last_hole_level;
		cmp.elec_levels[i+6] = cmp.last_elec_level;
	}
	
	/*
	 * This loop is heavily optimized using SSE2 and SSE3 instructions to vectorize computation.
	 * It is functionally equivalent to the nonvectorized version though more opaque.
	 * As a note the use of floating point comparison to 0 for setting the recombination flag
	 * should be okay because it should only compare true when all integers converted to float are 0
	 * and then squared and added, which all results in 0, thus a floating point comparison to 0 is correct
	 * and saves a lot of time and XMM registers.
	 */
	
	//Three loops to calculate all transition rates
	for (size_t off = 0; off < 12; off+=4)
	{		
		//Calculate position differences
		__m128 xdiff = _mm_cvtepi32_ps(_mm_sub_epi32(_mm_load_si128((__m128i *)&cmp.hole_x[off]), _mm_load_si128((__m128i *)&cmp.elec_x[off])));
		__m128 ydiff = _mm_cvtepi32_ps(_mm_sub_epi32(_mm_load_si128((__m128i *)&cmp.hole_y[off]), _mm_load_si128((__m128i *)&cmp.elec_y[off])));
		__m128 zdiff = _mm_cvtepi32_ps(_mm_sub_epi32(_mm_load_si128((__m128i *)&cmp.hole_z[off]), _mm_load_si128((__m128i *)&cmp.elec_z[off])));
		
		//calculate the field contribution (field already in units of eV/m)
		__m128 dipole = simd_dot(xdiff, ydiff, zdiff, _mm_load_ps(cmp.scaled_field));
		
		//Calculate squared distance
		xdiff = _mm_mul_ps(xdiff, xdiff);
		ydiff = _mm_mul_ps(ydiff, ydiff);
		zdiff = _mm_mul_ps(zdiff, zdiff);
		
		__m128 d2 = _mm_add_ps(xdiff, _mm_add_ps(ydiff, zdiff));
		
		//Now create mask to select for recombination transitions
		__m128 mask = _mm_cmpeq_ps(d2, _mm_set1_ps(0.0));
		
		//Calculate the inverse lattice distance
		__m128 rdist;
		rdist = simd_inv_dist(d2);
		rdist = _mm_mul_ps(rdist, _mm_set1_ps(invlat)); 

		__m128 energy;
	
		//energy = coulombic binding energy + -1.0*hole homo + elec lumo
		energy = _mm_mul_ps(_mm_set1_ps(binding_factor), rdist);
		
		energy = _mm_add_ps(energy, _mm_load_ps(&cmp.elec_levels[off]));
		energy = _mm_sub_ps(energy, _mm_load_ps(&cmp.hole_levels[off])); //Have to subtract since holes float
		
		//Subtract off the dipole term
		energy = _mm_sub_ps(energy, dipole);
		
		//Save off the energies
		_mm_store_ps(&cmp.energy[off], energy);

		//Calculate difference between that and the current energy
		__m128 current_e = _mm_set1_ps(cmp.last_en);
		
		energy = _mm_sub_ps(energy, current_e);

		__m128 thermalflag = _mm_cmple_ps(energy, _mm_set1_ps(0.0)); //should be all Fs if deltaE < 0 and all 0 otherwise
		
		//calculate boltzmann factor
		__m128 beta = _mm_set1_ps(minusBeta);
		energy = _mm_mul_ps(energy, beta);
		
		energy = exp_ps(energy);

		__m128 enexp = simd_pick(_mm_set1_ps(1.0), energy, thermalflag); //if deltaE < 0 don't add in the exponential

		energy = _mm_mul_ps(enexp, _mm_load_ps(&cmp.prob[off])); //multiply by attempt frequency
				
		//Mask should be 0xFFFFFFFF for recombination and 0x00000000 for not (set above)		
		__m128 prob = simd_pick(_mm_set1_ps(recomb_rate), energy, mask);
		
		_mm_store_ps(&cmp.prob[off], prob);
	}
	
	//_mm_sfence(); //Make sure are all streaming stores from this function are visible before continuing.
}

template<typename EnergyGetter, typename MobilityGetter>
void MCSimulation<EnergyGetter, MobilityGetter>::accumulate_rates()
{
	cum_rates[0] = transitions[0];
	
	for (size_t i=1; i<12; ++i)
		cum_rates[i] = cum_rates[i-1] + transitions[i];
}

template<typename EnergyGetter, typename MobilityGetter>
void MCSimulation<EnergyGetter, MobilityGetter>::accumulate_rates_simd()
{
	cum_rates[0] = cmp.prob[0];
	
	for (size_t i=1; i<12; ++i)
		cum_rates[i] = cum_rates[i-1] + cmp.prob[i];
}

template<typename EnergyGetter, typename MobilityGetter>
size_t MCSimulation<EnergyGetter, MobilityGetter>::find_transition(real_t R)
{
	if (R < cum_rates[0])
		return 0;
	
	for(size_t i=1; i<12; ++i)
	{
		if (R <= cum_rates[i])
			return i;		
	}
	
	printf("Should never get here! There is an algorithmic problem.\n");
	assert(false);
	return 11;
}
	
template<typename EnergyGetter, typename MobilityGetter>
size_t MCSimulation<EnergyGetter, MobilityGetter>::select_transition()
{
	real_t u = drand48();
	
	while (u == 0.0)
		u = drand48(); //Just in case it actually happened to be zero (unlikely)
	
	real_t R = u*cum_rates[11]; //scale random number by sum of transition rates
	
	return find_transition(R);
}	

/*
 * Initialize simulation parameters
 * lattice constant and initial separation should be given in angstroms
 * temperature should be given in kelvin
 * eps should be the dialectric constant of the medium 
 */
template<typename EnergyGetter, typename MobilityGetter>
void MCSimulation<EnergyGetter, MobilityGetter>::initialize_simulation(real_t lattice, real_t separation, real_t eps, real_t temp)
{
	lattice_const = lattice * 1e-10;
	initial_r = separation * 1e-10;
	dialectric_const = eps;
	
	kT = temp*BOLTZMANN_EV;
	minusBeta = -1.0 / kT;
	binding_factor = -1.0*((1.60217646e-19)*(1.60217646e-19) / (4*3.141592654*dialectric_const*8.85418782e-12))*JOULE_TO_EV; // (q^2/4pi eps) in eV 
		
	//Zero out the field initially
	for (size_t i=0; i<3; ++i)
		field[i] = 0.0;
		
	recomb_rate = 1.0;
	
	build_distance_table();
}

/*
 * Set the electron and hole mobilities as well as the recombination time
 * only the relative values of these quantities matter so the units are not important
 */
 
template<typename EnergyGetter, typename MobilityGetter>
void MCSimulation<EnergyGetter, MobilityGetter>::set_carrier_params(MobilityModel *electron_mob, MobilityModel *hole_mob, real_t recomb_time)
{
	recomb_rate = 1.0 / recomb_time;
	
	electron.mobility 	= electron_mob;	
	hole.mobility 		= hole_mob;
}

template<typename EnergyGetter, typename MobilityGetter>
void MCSimulation<EnergyGetter, MobilityGetter>::set_field(real_t mag, FieldType type)
{
	real_t angle = 0.0; 
	switch(type)
	{
		case kIsotropic:
		//Field should be a random vector in any direction
		angle = drand48() * 2.0 * 3.1415926535;
		break;
		
		case kHalfIsotropic:
		//Field should be a random vector in the left half-plane [pi/2, 3pi/2]
		angle = drand48() * 3.1415926535 + 3.1415926535/2.0;	
	}
	
	field[0] = cos(angle);
	field[1] = sin(angle);
	field[2] = 0.0;
	
	//Just in case, but it should be already be normalized
	normalize(field);
	
	LOG_DEBUG(initial_l, "Setting field strength: " << mag)
	
	for (size_t i=0; i<3; ++i)
		field[i] *= mag;
}

template<typename EnergyGetter, typename MobilityGetter>
void MCSimulation<EnergyGetter, MobilityGetter>::set_field(real_t *f)
{
	field[0] = f[0];
	field[1] = f[1];
	field[2] = f[2];
}

template<typename EnergyGetter, typename MobilityGetter>
void MCSimulation<EnergyGetter, MobilityGetter>::append_trace()
{
	CarrierPositions p;
	
	for (size_t i=0; i<3; ++i)
	{
		p.hole_pos[i] = hole.pos[i];
		p.elec_pos[i] = electron.pos[i];
	}
	
	results.trace.push_back(p);
}

template<typename EnergyGetter, typename MobilityGetter>
bool MCSimulation<EnergyGetter, MobilityGetter>::check_recombined()
{
	return positions_equal(electron.pos, hole.pos);
}

template<typename EnergyGetter, typename MobilityGetter>
bool MCSimulation<EnergyGetter, MobilityGetter>::check_escaped()
{
	//Define the escape probability as when the binding energy is less than kT
	real_t limit = kT;
	
	real_t inv_r = get_inv_distance(hole.pos, electron.pos);
	real_t binding = -1.0*binding_factor * inv_r; //binding_factor is negative to optimize iteration step code
	
	//LOG_DEBUG(energy_l, "Binding Energy: " << binding);
	
	if (binding < limit)
		return true;
		
	return false;
}
	
template<typename EnergyGetter, typename MobilityGetter>
void MCSimulation<EnergyGetter, MobilityGetter>::step()
{
	calculate_transitions();
	
	//Select the transition and carry it out
	size_t i = select_transition();
	
	if (i>=6)
	{
		//Move the hole
		i %= 6;
		
		hole.pos[dir_table[i]] += inc_table[i];
	}
	else
	{
		//Move the electron
		electron.pos[dir_table[i]] += inc_table[i];
	}

	//save_step_delay(i);
}


/*
 * Run both the SIMD and reference code for this step, comparing
 * their results and logging any differences.
 */
template<typename EnergyGetter, typename MobilityGetter>
void MCSimulation<EnergyGetter, MobilityGetter>::step_check()
{
	calculate_transitions_simd();
	calculate_transitions();
	
	//Make sure the two transition probability calculations give the same result
	for (size_t i=0; i<12; ++i)
	{
		//real_t eps = 1e-4*transitions[i];
		//real_t error = fabs(transitions[i] - cmp.prob[i]);
		
		//if (error > eps)
		{
			LOG_ERROR(energy_l, "Probability mismatch in transition " << i);
			LOG_ERROR(energy_l, "Elec Position: (" << electron.pos[0] << ", " << electron.pos[1] << ", " << electron.pos[2] << ")");
			LOG_ERROR(energy_l, "Hole Position: (" << hole.pos[0] << ", " << hole.pos[1] << ", " << hole.pos[2] << ")")
			LOG_ERROR(energy_l, "Reference value: " << transitions[i]);
			LOG_ERROR(energy_l, "SIMD value		: " << cmp.prob[i]);
		}
	}
			
	//Select the transition and carry it out
	size_t i = select_transition();
	
	if (i>=6)
	{
		//Move the hole
		i %= 6;
		
		hole.pos[dir_table[i]] += inc_table[i];
	}
	else
	{
		//Move the electron
		electron.pos[dir_table[i]] += inc_table[i];
	}
}

template<typename EnergyGetter, typename MobilityGetter>
void MCSimulation<EnergyGetter, MobilityGetter>::save_step_delay(size_t i)
{
	double delay = drand48();
	real_t d = -1.0*log(delay)/cum_rates[11];

	LOG_TRACE(progress_l, "Hop Delay: " << d);

	results.delays.push_back(d);
}

template<typename EnergyGetter, typename MobilityGetter>
void MCSimulation<EnergyGetter, MobilityGetter>::step_simd()
{	
	calculate_transitions_simd();
	
	//Select the transition and carry it out
	size_t i = select_transition();
		
	//update last values
	cmp.last_en = cmp.energy[i];
	cmp.last_elec_level = cmp.elec_levels[i];
	cmp.last_hole_level = cmp.hole_levels[i];

	save_step_delay(i);
	
	if (i>=6)
	{
		//Move the hole
		i %= 6;
		
		hole.pos[dir_table[i]] += inc_table[i];
	}
	else
	{
		//Move the electron
		electron.pos[dir_table[i]] += inc_table[i];
	}
}

template<typename EnergyGetter, typename MobilityGetter>
void MCSimulation<EnergyGetter, MobilityGetter>::build_distance_table()
{
	coord_t dist = (coord_t)(ceil(((1.60217646e-19)*(1.60217646e-19) / (4*3.141592654*dialectric_const*8.85418782e-12*kT/JOULE_TO_EV))*1e10)+1.0);
	
	max_dist = dist*dist; //Max dist is really distance squared
		
	if (max_dist > MAX_DISTANCE_ENTRIES)
	{
		max_dist = 0;
		LOG_WARN(energy_l, "NOT building a distance lookup table (would have too many entries): " << max_dist << " entries needed.");
		return;
	}
	
	LOG_INFO(energy_l, "Building a distance lookup table with " << max_dist << " entries based on thermal energy.");
	
	if (dist_table)
	{
		delete[] dist_table;
		dist_table = NULL;
	}
	
	dist_table = new real_t[max_dist];
	
	for (coord_t i=0; i<max_dist; ++i)
		dist_table[i] = 1.0/(sqrt((real_t)i)*lattice_const);
}	

template<typename EnergyGetter, typename MobilityGetter>
void MCSimulation<EnergyGetter, MobilityGetter>::run(size_t max_steps, bool trace)
{
	//Reset the trace and the world
	results.trace.clear();
	results.delays.clear();
	space->reset();
		
	results.sim = this;
	
	LOG_TRACE(progress_l, "Beginning Simulation");
	LOG_TRACE(initial_l, "Simulation Parameters Start");
	LOG_TRACE(initial_l, "Max Steps: " << max_steps);
	LOG_TRACE(initial_l, "Trace: " << (trace ? "True" : "False"));

	if (trace)
		append_trace();
	
	for (size_t i=0; i<max_steps; ++i)
	{	
		try {
			step();
		} catch (...) {
			LOG_WARN(progress_l, "Simulation terminated due to error.");
			results.result = kErrorOccurred;
			return;
		}
	
		if (trace)
			append_trace();
				
		if (check_recombined())
		{
			results.result = kRecombined;
			LOG_DEBUG(progress_l, "Simulation Ended (" << i << " steps) with recombination");
			LOG_DEBUG(progress_l, "Particle Position [" << electron.pos[0] << ", " << electron.pos[1] << ", " << electron.pos[2] << "]");
			return;
		}
		else if (check_escaped())
		{
			results.result = kEscaped;
			LOG_DEBUG(progress_l, "Simulation Ended (" << i << " steps) with escape");
			LOG_DEBUG(progress_l, "Electron Position [" << electron.pos[0] << ", " << electron.pos[1] << ", " << electron.pos[2] << "]");
			LOG_DEBUG(progress_l, "Hole Position 	 [" << hole.pos[0] << ", " << hole.pos[1] << ", " << hole.pos[2] << "]");
			return;
		}
	}
	
	LOG_WARN(progress_l, "Time expired on simulation");
	results.result = kTimeExpired;
}

template<typename EnergyGetter, typename MobilityGetter>
void MCSimulation<EnergyGetter, MobilityGetter>::run_simd(size_t max_steps, bool trace)
{
	//Reset the trace and the world
	results.trace.clear();
	results.delays.clear();
	space->reset();
		
	results.sim = this;
	
	LOG_TRACE(progress_l, "Beginning Simulation (using SIMD)");
	LOG_TRACE(initial_l, "Simulation Parameters Start");
	LOG_TRACE(initial_l, "Max Steps: " << max_steps);
	LOG_TRACE(initial_l, "Trace: " << (trace ? "True" : "False"));

	if (trace)
		append_trace();
	
	//Initialize data for SIMD code
	initialize_simd();
	
	for (size_t i=0; i<max_steps; ++i)
	{	
		try {
			step_simd();
			//step_check();
		} catch (...) {
			LOG_WARN(progress_l, "Simulation terminated due to error.");
			results.result = kErrorOccurred;
			return;
		}
	
		if (trace)
			append_trace();
				
		if (check_recombined())
		{
			results.result = kRecombined;
			LOG_DEBUG(progress_l, "Simulation Ended (" << i << " steps) with recombination");
			LOG_DEBUG(progress_l, "Particle Position [" << electron.pos[0] << ", " << electron.pos[1] << ", " << electron.pos[2] << "]");
			return;
		}
		else if (check_escaped())
		{
			results.result = kEscaped;
			LOG_DEBUG(progress_l, "Simulation Ended (" << i << " steps) with escape");
			LOG_DEBUG(progress_l, "Electron Position [" << electron.pos[0] << ", " << electron.pos[1] << ", " << electron.pos[2] << "]");
			LOG_DEBUG(progress_l, "Hole Position 	 [" << hole.pos[0] << ", " << hole.pos[1] << ", " << hole.pos[2] << "]");
			return;
		}
	}
	
	LOG_WARN(progress_l, "Time expired on simulation");
	results.result = kTimeExpired;
}

template<typename EnergyGetter, typename MobilityGetter>
void MCSimulation<EnergyGetter, MobilityGetter>::initialize_simd()
{
	float dipole_scale = 1.60217646e-19*JOULE_TO_EV*lattice_const;
	
	//Prescale the field in units of eV/m
	for (size_t i=0; i<3; ++i)
		cmp.scaled_field[i] = dipole_scale * field[i];
		
	invlat = 1.0/lattice_const;
	
	//Initialize energy and levels for SIMD code
	cmp.last_en = calculate_energy(electron.pos, hole.pos);
	cmp.last_elec_level = energy_gen->electron_energy(electron.pos, lattice_const, space, gen);
	cmp.last_hole_level = energy_gen->hole_energy(hole.pos, lattice_const, space, gen);
}

template<typename EnergyGetter, typename MobilityGetter>
void MCSimulation<EnergyGetter, MobilityGetter>::save_world(const std::string &filename)
{
	space->write_xyz(filename);
}

template<typename EnergyGetter, typename MobilityGetter>
void MCSimulation<EnergyGetter, MobilityGetter>::visualize_world(const std::string &filename, size_t side)
{
	FILE *file = fopen(filename.c_str(), "w");
	
	if (!file)
	{
		printf("Could not open file %s for writing world\n", filename.c_str());
		return;
	}
	
	size_t num_atoms = side*side*side;
	fprintf(file, "%lu\n", num_atoms);
	fprintf(file, "KineticMC World File\n");
	
	std::map<unsigned long, std::string> sites;
	size_t last_id = 0;
	
	for (size_t i=0; i<side; ++i)
	{
		for (size_t j=0;j<side; ++j)
		{
			for (size_t k=0;k<side; ++k)
			{
				coord_t pos[3];
				real_t a = lattice_const*1.0e10;
				
				pos[0] = i - side/2;
				pos[1] = j - side/2;
				pos[2] = k - side/2;
				
				real_t homod = energy_gen->hole_energy(pos, lattice_const, space, gen);
				
				unsigned long *homo = (unsigned long *)&homod;
				
				if (sites.find(*homo) == sites.end())
				{
					char id[2];
					id[0] = 'A' + last_id++;
					id[1] = '\0';
					sites[*homo] = std::string(id);
					
					printf("Adding site to map (HOMO=%f) id=%s\n", homod, id);
				}
				
				fprintf(file, "%s %.6f %.6f %.6f\n", sites[*homo].c_str(), pos[0]*a, pos[1]*a, pos[2]*a);
			}
		}
	}
	
	fclose(file);
}

template<typename EnergyGetter, typename MobilityGetter>
void MCSimulation<EnergyGetter, MobilityGetter>::print_statistics()
{
	space->print_statistics();
}

template<typename EnergyGetter, typename MobilityGetter>
SimulationData * MCSimulation<EnergyGetter, MobilityGetter>::get_results()
{
	return &results;
}

template<typename EnergyGetter, typename MobilityGetter>
bool MCSimulation<EnergyGetter, MobilityGetter>::escaped()
{
	return results.result == kEscaped;
}

template<typename EnergyGetter, typename MobilityGetter>
void MCSimulation<EnergyGetter, MobilityGetter>::fill_xy_world_slice(int side, real_t *n)
{
	coord_t min_x = electron.pos[0] - side/2;
	coord_t max_x = min_x + side;

	coord_t min_y = electron.pos[1] - side/2;
	coord_t max_y = min_y + side;

	printf("Filling buffer with side %d\n", side);

	for (coord_t x = min_x; x<max_x; ++x)
	{
		for (coord_t y = min_y; y<max_y; ++y)
		{
			coord_t pos[3];
			
			pos[0] = x;
			pos[1] = y;
			pos[2] = electron.pos[2];
			
			real_t homod = energy_gen->hole_energy(pos, lattice_const, space, gen);
			real_t lumod = energy_gen->electron_energy(pos, lattice_const, space, gen);

			int index = (y-min_y)*side + (x-min_x);

			if ((y-min_y)%60 == 0)
				printf("(%d, %d): %f\n", x, y, homod);

			n[index*2 + 0] = homod;
			n[index*2 + 1] = lumod;
		}
	}
}

template<typename EnergyGetter, typename MobilityGetter>
std::vector<real_t> MCSimulation<EnergyGetter, MobilityGetter>::calc_hopping_rates()
{
	std::vector<real_t> trans;
	
	trans.resize(12);
	
	if (use_sse)
	{
		initialize_simd();
		calculate_transitions_simd();
		
		for (size_t i=0; i<12; ++i)
			trans[i] = cmp.prob[i];
	}
	else
	{
		calculate_transitions();
	
		for (size_t i=0; i<12; ++i)
			trans[i] = transitions[i];
	}
	
	return trans;
}