//WorldCreator.cpp

#include "WorldCreator.h"
#include <strings.h>
#include "ImageHelper.hxx"

using namespace std;

WorldCreator::WorldCreator() : world(NULL), conn(NULL), next_id(1)
{
}

WorldCreator::~WorldCreator()
{
	//FIXME: free world generators upon destruction	
}

void WorldCreator::add_component(const string &name, double fraction, WorldGeneratorSpec *env)
{
	int id = next_id++;
	components[name] = id;
	composition[id] = fraction;
	
	//Save off this local environment
	environs[id] = env->create();
	
	printf("Adding id=%d with fraction=%g\n", id, fraction);
}

int WorldCreator::get_component_id(const string &name)
{
	return components[name];
}

void WorldCreator::set_world(PreallocatedWorld *w)
{
	world = w;
	world->get_dimensions(dims);
}

PreallocatedWorld* WorldCreator::get_world()
{
	return world;
}

void WorldCreator::create_world(size_t xdim, size_t ydim, size_t zdim)
{
	if (world)
	{
		delete world;
		world = NULL;
	}
	
	dims[0] = (coord_t)xdim;
	dims[1] = (coord_t)ydim;
	dims[2] = (coord_t)zdim;

	world = new PreallocatedWorld(xdim, ydim, zdim);
}

void WorldCreator::randomize_region(int region, FractionChooser<int> &chooser)
{
	if (!world)
		throw std::string("You must give WorldCreator a world before randomizing it!");
		
	coord_t pos[3];
	
	chooser.prepare(); //Precalc values to speed up the loop
	
	for (pos[0] = 0; pos[0]<dims[0]; ++pos[0])
	{
		for (pos[1] = 0;pos[1]<dims[1]; ++pos[1])
		{
			for (pos[2] = 0; pos[2]<dims[2]; ++pos[2])
			{
				int id = chooser.choose();
				
				if (region == -1 || world->get_value(pos, kElectron).type == region)
					world->get_value(pos, kElectron).type = id;
			}
		}
	}
}

void WorldCreator::randomize_world()
{
	FractionChooser<int> chooser;
	
	for (map<int, double>::iterator it=composition.begin(); it!=composition.end(); ++it)
		chooser.add_option(it->second, it->first);

	randomize_region(-1, chooser);
}

void WorldCreator::random_location(coord_t *out)
{
	for (size_t i=0; i<3; ++i)
	{
		double loc = drand48()*(double)(dims[i]);
		out[i] = (coord_t)loc;
		
		if (out[i] >= dims[i])
			out[i] = dims[i];
	}
}

void WorldCreator::switch_sites(coord_t *pos, size_t d, size_t o)
{
	coord_t other[3]; 
	
	for (size_t i=0; i<3; ++i)
		other[i] = pos[i];
		
	other[d] += o;
	
	int otype = world->get_value(other, kElectron).type;
	world->get_value(other, kElectron).type = world->get_value(pos, kElectron).type;
	world->get_value(pos, kElectron).type = otype;
}

void WorldCreator::assign_energy_levels()
{
	coord_t pos[3];
		
	for (pos[0] = 0; pos[0]<dims[0]; ++pos[0])
	{
		for (pos[1] = 0;pos[1]<dims[1]; ++pos[1])
		{
			for (pos[2] = 0; pos[2]<dims[2]; ++pos[2])
			{
				int type = world->get_value(pos, kElectron).type;
				world->set_value(pos, environs[type]->generate(pos, 10.0, world), kElectron);
				world->get_value(pos, kElectron).type = type;  //Make sure we preserve the site type information
			}
		}
	}
}

/*
 * Recursive descent labeler classifying the connectivity of all sites of the world 
 * assuming only nearest neighbor connections between sites of the same type
 */
void WorldCreator::classify_regions(PreallocatedWorld *world, unsigned int *conn, coord_t *loc)
{
	int me = world->get_value(loc, kElectron).type;
	coord_t dims[3];
	
	coord_t my_off = world->get_offset(loc);
	
	world->get_dimensions(dims);
	
	printf("curr  (%d, %d, %d)\n", loc[0], loc[1], loc[2]);
	printf("conn %d", conn[my_off]);
	
	for (size_t i=0; i<6; ++i)
	{
		//Explore all nearest neighbors
		coord_t dir = 2*(i%2)-1; //+1 or -1
		coord_t dx = (i/2) == 0 ? dir : 0;
		coord_t dy = (i/2) == 1 ? dir : 0;
		coord_t dz = (i/2) == 2 ? dir : 0;
		
		coord_t next_loc[3];
		next_loc[0] = loc[0] + dx;
		next_loc[1] = loc[1] + dy;
		next_loc[2] = loc[2] + dz;
		
		bool at_edge = false;
		
		for (size_t j=0; j<3; ++j)
		{
			if (next_loc[j] < 0 || next_loc[j] >= dims[j])
			{
				at_edge = true;
				break;
			}
		}
		
		//Stop if we would be going off the edge of the world
		if (at_edge)
			continue;
		
		//Make sure that we haven't visited this neighbor yet and that we're connected to it
		coord_t off = world->get_offset(next_loc);
		if (world->get_value(next_loc, kElectron).type == me && conn[off] == 0)
		{
			printf("next (%d, %d, %d)\n", next_loc[0], next_loc[1], next_loc[2]);
			conn[off] = conn[my_off];
			WorldCreator::classify_regions(world, conn, next_loc);
		}
	}
}

void WorldCreator::classify_connectivity()
{
	if (conn != NULL)
	{
		delete conn;
		conn = NULL;
	}
	
	if (world == NULL)
		throw std::string("You must attach a world before calculating connectivity");
	
	//Create the connectivity tensor and initialize it
	conn = new unsigned int[dims[0]*dims[1]*dims[2]];
	bzero(conn, sizeof(unsigned int)*dims[0]*dims[1]*dims[2]);
	
	unsigned int current_region = 1;
	coord_t loc[3];
	
	for (coord_t x=0; x<dims[0]; ++x)
	{
		loc[0] = x;
		for (coord_t y=0; y<dims[1]; ++y)
		{
			loc[1] = y;
			for (coord_t z=0; z<dims[2]; ++z)
			{
				loc[2] = z;
				
				coord_t offset = world->get_offset(loc);
				
				if (conn[offset] == 0)
				{
					conn[offset] = current_region++;
					WorldCreator::classify_regions(world, conn, loc);
				}
			}
		}
	}
}

void WorldCreator::save_connectivity(const std::string &filename)
{
	size_t d[3];
	
	d[0] = dims[0];
	d[1] = dims[1];
	d[2] = dims[2];
	
	if (conn == NULL)
		throw std::string("You must calculate connectivity before saving it");
	
	ImageHelper<unsigned int> helper(conn, d);
	
	helper.write_nrrd(filename.c_str());
}

void WorldCreator::select_different_neighbor(coord_t *pos, size_t *d_out, coord_t *o_out)
{
	int type = world->get_value(pos, kElectron).type;

	size_t neighbors[6];
	size_t diff_count = 0;
	
	for (size_t i=0; i < 6; ++i)
	{
		size_t d = i%3;
		coord_t o = 2*(i & 0x01) - 1;
		
		pos[d] += o;
		if (world->get_value(pos, kElectron).type != type)
			neighbors[diff_count++] = i;
			
		pos[d] -= o;
	}
	
	size_t choice = drand48() * diff_count;
	if (choice == diff_count)
		choice = diff_count - 1;
	
	*d_out = neighbors[choice]%3;
	*o_out = 2*(neighbors[choice] & 0x01) - 1;
}

void WorldCreator::automata(size_t steps, size_t recalc, bool anneal)
{
	size_t num_switches = 0, num_jumps=0;
	size_t block_i=0;
	size_t block_len=50000;
	size_t block_cnt=0;
	size_t block_thresh = 10;
	
	bool auto_stop = false;
		
	world->find_surfaces(surfaces);
	
	if (anneal == 0 && steps == 0)
	{
		steps = (size_t)-1;
		auto_stop = true;
	}
	
	for (size_t i=0; i<steps; ++i)
	{
		size_t surf = drand48() * surfaces.size();
		if (surf >= surfaces.size())
			surf = surfaces.size() - 1;
		
		coord_t pos[3];
		world->position_from_offset(surfaces[surf], pos);
		
		coord_t curr_e = 0;
		coord_t sw_e = 0;
		
		size_t d;
		coord_t o;
		
		select_different_neighbor(pos, &d, &o);
		
		curr_e += world->count_neighbors(pos);
				
		pos[d] += o;
		curr_e += world->count_neighbors(pos);
		pos[d] -= o;
				
		switch_sites(pos, d, o);
		
		sw_e += world->count_neighbors(pos);
				
		pos[d] += o;
		sw_e += world->count_neighbors(pos);
		pos[d] -= o;
		
		if (i % recalc == 0)
			world->find_surfaces(surfaces);
			
		++block_i;
		
		//Check if we're converging
		if (auto_stop && block_i == block_len)
		{
			block_i = 0;
			if (block_cnt < block_thresh)
				return;
			
			block_cnt = 0;
		}
					
		if (sw_e > curr_e)
		{
			++num_switches;
			++block_cnt;
			continue;
		}
		else if (anneal)
		{
			double en = (sw_e - curr_e);
			if (drand48() < exp(en))
			{
				++num_jumps;
				continue;
			}
		}
		
		switch_sites(pos, d, o);
	}
	
	printf("Statistics: %lu switches / %lu steps\n", num_switches, steps);
	printf("Number of up-energy jumps: %lu\n", num_jumps);
}

std::vector<coord_t> WorldCreator::get_dimensions()
{
	std::vector<coord_t> outdims(3);
	
	for (size_t i=0; i<3; ++i)
		outdims[i] = dims[i];
		
	return outdims;
}

int WorldCreator::get_allocated_size()
{
	return (int)dims[0]*dims[1]*dims[2];
}

/*
 * Copy the world into the provided buffer, this is meant to be called from python
 * and passed a 4D [x, y, z, 2] numpy array.
 */
void WorldCreator::convert_world(real_t *n, int n_dim)
{
	//Make sure the dimensions match (need a factor of 2 for the homo and lumo
	if (n_dim != (dims[0]*dims[1]*dims[2]*2))
		return;
	
	for (coord_t i=0; i<dims[0]; ++i)
	{
		for (coord_t j=0; j<dims[1]; ++j)
		{
			for (coord_t k=0; k<dims[2]; ++k)
			{
				coord_t pos[3];
				
				pos[0] = i;
				pos[1] = j;
				pos[2] = k;
				
				LocalEnvironment &env = world->get_value(pos, kElectron);
				
				n[i*dims[1]*dims[2]*2 + j*dims[2]*2 + k*2 + 0] = env.homo;
				n[i*dims[1]*dims[2]*2 + j*dims[2]*2 + k*2 + 1] = env.lumo;
			}
		}
	}
}