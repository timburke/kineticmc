//TrilayerSpec.h

#ifndef __TrilayerSpec_h__
#define __TrilayerSpec_h__

#include "Types.h"
#include "TrilayerGenerator.h"


class TrilayerSpec : public WorldGeneratorSpec
{
	private:
	WorldGeneratorSpec *m1;
	WorldGeneratorSpec *m2;
	WorldGeneratorSpec *middle;
	
	coord_t middle_size;
		
	public:
	TrilayerSpec(WorldGeneratorSpec *mat1, WorldGeneratorSpec *mat2, WorldGeneratorSpec *mid, int size)
		: m1(mat1), m2(mat2), middle(mid), middle_size(size)
	{
	
	}
	
	//Don't take ownership of m1, m2 since they could be (and probably are) coming from python.
	virtual WorldGenerator* create() 
	{
		WorldGenerator *w1 = m1->create();
		WorldGenerator *w2 = m2->create();
		WorldGenerator *mid = middle->create();
		
		return new TrilayerGenerator(w1, w2, mid, middle_size);
	};
};

#endif