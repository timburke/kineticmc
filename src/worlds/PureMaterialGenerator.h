//PureMaterialGenerator.h

#ifndef __PureMaterialGenerator_h__
#define __PureMaterialGenerator_h__

#include "Types.h"

class PureMaterialGenerator : public WorldGenerator
{
	private:
	LocalEnvironment material;
	
	public:
	PureMaterialGenerator(real_t homo, real_t lumo, int tag)
	{
		material.homo = homo;
		material.lumo = lumo;
		material.type = tag;
	}
	
	virtual LocalEnvironment generate(coord_t *pos, real_t lattice_constant, World *space)
	{
		return material;
	}
	
	virtual bool context_free() {return true;}; //We don't need to save the results of this calculation
	virtual bool pos_independent() {return true;}; //We don't use lattice_constant or space
};

#endif