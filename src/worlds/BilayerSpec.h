//BilayerSpec.h

#ifndef __BilayerSpec_h__
#define __BilayerSpec_h__

#include "Types.h"
#include "BilayerGenerator.h"
#include "HomogenousBilayerGenerator.h"
#include "FlatBilayerGenerator.h"

class BilayerSpec : public WorldGeneratorSpec
{
	private:
	WorldGeneratorSpec *m1;
	WorldGeneratorSpec *m2;
	bool ct;
	real_t angle;
	real_t dipole_layer;
		
	public:
	BilayerSpec(WorldGeneratorSpec *mat1, WorldGeneratorSpec *mat2, bool ct_at_iface=false) : m1(mat1), m2(mat2), ct(ct_at_iface), angle(0.0), dipole_layer(0.0) {};
	BilayerSpec(WorldGeneratorSpec *mat1, WorldGeneratorSpec *mat2, bool ct_at_iface, real_t a) : m1(mat1), m2(mat2), ct(ct_at_iface), angle(a), dipole_layer(0.0) {};
	BilayerSpec(WorldGeneratorSpec *mat1, WorldGeneratorSpec *mat2, bool ct_at_iface, real_t a, real_t dipole) : m1(mat1), m2(mat2), ct(ct_at_iface), angle(a), dipole_layer(dipole) {};
	
	//Don't take ownership of m1, m2 since they could be (and probably are) coming from python.
	virtual WorldGenerator* create() 
	{
		WorldGenerator *w1 = m1->create();
		WorldGenerator *w2 = m2->create();
		
		//Return an optimized bilayer in the case of position independent, context free submaterials
		if (w1->pos_independent() && w1->context_free() && w2->pos_independent() && w2->context_free())
		{
			//No optimization is possible if we need to add a dipole with no ct state
			if (!ct && dipole_layer != 0.0)
				return new BilayerGenerator(w1, w2, ct, angle, dipole_layer);
				
			if (abs(angle) < 0.001)
				return new FlatBilayerGenerator(w1, w2, ct, dipole_layer);
			else
				return new HomogenousBilayerGenerator(w1, w2, ct, angle, dipole_layer);
		}
			
		return new BilayerGenerator(w1, w2, ct, angle, dipole_layer);
	};
};

#endif