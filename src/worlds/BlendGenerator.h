//BlendGenerator.h

#ifndef __BlendGenerator_h__
#define __BlendGenerator_h__

#include "Types.h"
#include <stdlib.h>

class BlendGenerator : public WorldGenerator
{
	private:
	WorldGenerator *m1;
	WorldGenerator *m2;
	
	real_t blend_ratio; //from 0.0 to 1.0, 1 being fully m2, 0 being fully m1
	
	public:
	BlendGenerator(WorldGenerator *mat1, WorldGenerator *mat2, real_t ratio) : m1(mat1), m2(mat2), blend_ratio(ratio)
	{
		
	}
	
	virtual ~BlendGenerator()
	{
		if (m1)
			delete m1;
			
		if (m2)
			delete m2;
	}
	
	virtual LocalEnvironment generate(coord_t *pos, real_t lattice_constant, World *space)
	{
		real_t rand = drand48();
		
		//Enforce that -1,0,0 is donor and 0,0,0 is acceptor
		if (pos[0] == -1 && pos[1] == 0 && pos[2] == 0)
			return m1->generate(pos, lattice_constant, space);
		else if (pos[0] == 0 && pos[1] == 0 && pos[2] == 0)
			return m2->generate(pos, lattice_constant, space);
		
		if (rand <= blend_ratio)
			return m2->generate(pos, lattice_constant, space);
		else
			return m1->generate(pos, lattice_constant, space);
	}
};

#endif