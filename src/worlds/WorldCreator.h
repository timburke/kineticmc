//WorldCreator.h

#ifndef __WorldCreator_h__
#define __WorldCreator_h__

#include "PreallocatedWorld.h"
#include "FractionChooser.hxx"
#include <map>
#include <string>
#include <vector>

class WorldCreator
{
	private:
	PreallocatedWorld 	*world;
	unsigned int		*conn; //Shadow connectivity tensor  
	std::vector<size_t>	surfaces;
	coord_t	 dims[3];
	
	int next_id;
	
	std::map<std::string, int> 		components;
	std::map<int, double>      		composition;
	std::map<int, WorldGenerator *> environs;
	
	void switch_sites(coord_t *pos, size_t d, size_t o);	
	void random_location(coord_t *out);

	void select_different_neighbor(coord_t *pos, size_t *d_out, coord_t *o_out);

	static void classify_regions(PreallocatedWorld *world, unsigned int *conn, coord_t *loc);

	public:
	WorldCreator();
	~WorldCreator();
	
	void set_world(PreallocatedWorld *world);
	PreallocatedWorld* get_world(void);
	
	void create_world(size_t xdim, size_t ydim, size_t zdim);

	void add_component(const std::string &name, double fraction, WorldGeneratorSpec *spec);
	int  get_component_id(const std::string &name);
	
	void automata(size_t steps, size_t recalc, bool anneal);

	void randomize_region(int region, FractionChooser<int> &chooser);
	void randomize_world(); //Assign components randomly according to ratios passed in

	void classify_connectivity();
	void assign_energy_levels();
	void save_connectivity(const std::string &filename);
	
	//numpy convenience function, should only be called from python
	std::vector<coord_t> get_dimensions();
	int get_allocated_size();
	void convert_world(real_t *n, int n_dim);
};

#endif