//BlendSpec.h

#ifndef __BlendSpec_h__
#define __BlendSpec_h__

#include "Types.h"
#include "BlendGenerator.h"

class BlendSpec : public WorldGeneratorSpec
{
	private:
	WorldGeneratorSpec *m1;
	WorldGeneratorSpec *m2;
	
	real_t ratio;
	
	public:
	BlendSpec(WorldGeneratorSpec *mat1, WorldGeneratorSpec *mat2, real_t rat) : m1(mat1), m2(mat2), ratio(rat) {};
	
	//Don't take ownership of m1, m2 since they could be (and probably are) coming from python.
	
	virtual WorldGenerator* create() {return new BlendGenerator(m1->create(), m2->create(), ratio);};
};

#endif