//HomogenousBilayerGenerator.h

#ifndef __HomogenousBilayerGenerator_h__
#define __HomogenousBilayerGenerator_h__

#include "Types.h"
#include "BilayerGenerator.h"
#include <stdlib.h>
#include <cmath>

class HomogenousBilayerGenerator : public WorldGenerator
{
	private:
	WorldGenerator *m1;
	WorldGenerator *m2;
	
	LocalEnvironment envs[3];
	
	bool ct;
	real_t dipole;
	
	coord_t normal[3];
	
	/*
	 * Return -1, 0, or 1 if v is negative, zero or positive 
	 * without branching, from here
	 * http://graphics.stanford.edu/~seander/bithacks.html#CopyIntegerSign
	 */
	inline coord_t signof(coord_t v)
	{
		return (v != 0) | -(int)((unsigned int)((int)v) >> (sizeof(coord_t) * 8 - 1));
	}
	
	/*
	 * our two materials are homogenous, so create just two environments and return those
	 *
	 */
	void init_environments()
	{
		coord_t pos[3] = {0,0,0};
		
		envs[0] = m1->generate(pos, 1.0, NULL);
		envs[2] = m2->generate(pos, 1.0, NULL);
		
		LocalEnvironment env1 = envs[0];
		LocalEnvironment env2 = envs[2];
		LocalEnvironment ct_env;
		//Create the CT state if we're doing that
		if (ct)
		{
			ct_env = env1;
			
			//Choose the more negative LUMO and less negative HOMO
			if (env2.lumo < env1.lumo)
				ct_env.lumo = env2.lumo+dipole;
			else
				ct_env.lumo += dipole;
			
			if (env2.homo > env1.homo)
				ct_env.homo = env2.homo-dipole;
			else
				ct_env.homo -= dipole;
		}
		else
			ct_env = env2;
		
		envs[1] = ct_env;
	}
	
	coord_t get_bilayer_side(coord_t *p)
	{
		/*
		 * Replaced real_t precision math with fixed point integer math
		 * precision is fixed at 1e-4. normal is multiplied by 1e4 to make
		 * integer math appropriate
		 * This returns 0 for left of interface, 1 for on interface and 2 for right of interface
		 */
		coord_t dist = 0;
		
		for (size_t i=0; i<3; ++i)
			dist += normal[i]*p[i];
		
		//If -1000 < dist < 1000, this should return 0
		dist /= 1000;
		
		return signof(dist)+1;
	}
		
	public:
	HomogenousBilayerGenerator(WorldGenerator *mat1, WorldGenerator *mat2, bool iface_ct, real_t angle, real_t dp) : m1(mat1), m2(mat2), ct(iface_ct), dipole(dp)
	{
		real_t rad_angle = angle*3.1415926535/180.0;
		real_t normal_f[3];
		
		//We tilt around the y-axis so XZ changes
		
		normal_f[0] = cos(rad_angle);
		normal_f[1] = 0.0;
		normal_f[2] = -1.0*sin(rad_angle);

		for (size_t i=0;i<3;++i)		
			normal[i] = (coord_t)(normal_f[i]*10000.0); //Fixed precision math at 1e-4
		
		LOG_DEBUG(log4cxx::Logger::getLogger("kineticmc.framework"), "Created tilted homogenous bilayer at angle " << angle);
		LOG_DEBUG(log4cxx::Logger::getLogger("kineticmc.framework"), "Normal was (" << normal_f[0] << ", " << normal_f[1] << ", " << normal_f[2] << ")");
		
		if (!ct && dipole != 0.0)
		{
			LOG_WARN(log4cxx::Logger::getLogger("kineticmc.framework"), "Dipole layer specified with no CT state.  IGNORING!");
		}
		else if (dipole != 0.0)
		{
			LOG_INFO(log4cxx::Logger::getLogger("kineticmc.framework"), "Dipole layer specified with value: " << dipole << " eV");
		}
			
		bool ctx_free = mat1->context_free() && mat2->context_free();
		bool pos_indep = mat1->pos_independent() && mat2->pos_independent();
		
		if (!ctx_free)
		{
			LOG_ERROR(log4cxx::Logger::getLogger("kineticmc.sim.initial"), "A HomogenousBilayer was created with non context_free materials");
			throw std::exception();
		}
		
		if (!pos_indep)
		{
			LOG_ERROR(log4cxx::Logger::getLogger("kineticmc.sim.initial"), "A HomogenousBilayer was created with non position independent materials");
			throw std::exception();
		}		
		
		init_environments();
	}
	
	virtual ~HomogenousBilayerGenerator()
	{
		if (m1)
			delete m1;
			
		if (m2)
			delete m2;
	}
	
	virtual LocalEnvironment generate(coord_t *pos, real_t lattice_constant, World *space)
	{	
		coord_t result = get_bilayer_side(pos);
		
		return envs[result]; //ct state creation is handled automatically by init_environments
	}
	
	virtual bool context_free() 
	{
		return true;
	}
};

#endif