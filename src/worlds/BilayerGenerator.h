//BilayerGenerator.h

#ifndef __BilayerGenerator_h__
#define __BilayerGenerator_h__

#include "Types.h"
#include <stdlib.h>
#include <cmath>
#include <exception>

#define LOOKUP_LENGTH 2048
enum InterfaceLatticeResult
{
	kOnInterface,
	kLeftOfInterface,
	kRightOfInterface
};

class BilayerGenerator : public WorldGenerator
{
	private:
	WorldGenerator *m1;
	WorldGenerator *m2;
		
	bool ct;
	real_t dipole;

	bool tilted;

	bool ctx_free;
	bool pos_indep;
	
	coord_t normal[3];
	
	LocalEnvironment gen_env(unsigned int type, coord_t *pos, real_t lattice, World *space)
	{
		LocalEnvironment env;
	
		switch(type)
		{
			case 0:
			//Material 1
			env = m1->generate(pos, lattice, space);
			if (pos[0] == -1)
			{
				env.lumo += dipole;
				env.homo -= dipole;
			}
			return env;
			break;
			
			case 1:
			//Material 2
			env = m2->generate(pos, lattice, space);
			if (pos[0] == 0)
			{
				env.lumo += dipole;
				env.homo -= dipole;
			}			
			return env;
			break;
			
			case 2:
			//Interface State
			{
				LocalEnvironment e1 = m1->generate(pos, lattice, space);
				LocalEnvironment e2 = m2->generate(pos, lattice, space);
			
				//Choose the more negative LUMO and less negative HOMO
				if (e2.lumo < e1.lumo)
					e1.lumo = e2.lumo+dipole;
				else
					e1.lumo += dipole;
			
				if (e2.homo > e1.homo)
					e1.homo = e2.homo-dipole;
				else
					e1.homo -= dipole;
			
				return e1;
			}
			break;
		}

		//FIXME: This is an error;
		return env;
	}		
		
	LocalEnvironment generate_notilt(coord_t *pos, real_t lattice_constant, World *space)
	{	
		//If we're creating a CT state, blend the two materials HOMO and LUMO to create the smallest
		//effective BG.
		
		if (ct && pos[0] == 0)
			return gen_env(2, pos, lattice_constant, space);
			
		//Otherwise just create two materials next to each other.
		if (pos[0] >= 0)
			return gen_env(1, pos, lattice_constant, space);
		else
			return gen_env(0, pos, lattice_constant, space);
	}
	
	InterfaceLatticeResult get_bilayer_side(coord_t *p)
	{
		/*
		 * Replaced real_t precision math with fixed point integer math
		 * precision is fixed at 1e-4. normal is multiplied by 1e4 to make
		 * integer math appropriate
		 */
		coord_t dist = 0;
		
		for (size_t i=0; i<3; ++i)
			dist += normal[i]*p[i];
			
		//LOG_INFO(log4cxx::Logger::getLogger("kineticmc.sim.calc.energy"), "Site bilayer distance: " << dist/10000);
		
		
		if (dist >= 1000) //corresponds to a distance of 0.1
			return kRightOfInterface;
		else if (dist <= -1000)
			return kLeftOfInterface;
		else
			return kOnInterface;
	}
	
	LocalEnvironment generate_tilt(coord_t *pos, real_t lattice_constant, World *space)
	{	
		InterfaceLatticeResult result = get_bilayer_side(pos);
		
		//If we're creating a CT state, blend the two materials HOMO and LUMO to create the smallest
		//effective BG.
		
		if (ct && result == kOnInterface)
			return gen_env(2, pos, lattice_constant, space);
		
		//Otherwise just create two materials next to each other.
		//Do the comparison this way to maintain the same result for angle=0.0 as no tilt
		if (result != kLeftOfInterface)
			return gen_env(1, pos, lattice_constant, space);

		else
			return gen_env(0, pos, lattice_constant, space);
	}
		
	public:
	BilayerGenerator(WorldGenerator *mat1, WorldGenerator *mat2, bool iface_ct, real_t angle, real_t dp) : m1(mat1), m2(mat2), ct(iface_ct), dipole(dp)
	{
		if (angle == 0.0)
		{
			tilted = false;
			LOG_INFO(log4cxx::Logger::getLogger("kineticmc.sim.initial"), "Created no tilt bilayer");
		}
		else
		{
			tilted = true;
			real_t rad_angle = angle*3.1415926535/180.0;
			real_t normal_f[3];
			
			//We tilt around the y-axis so XZ changes
			
			normal_f[0] = cos(rad_angle);
			normal_f[1] = 0.0;
			normal_f[2] = -1.0*sin(rad_angle);
	
			for (size_t i=0;i<3;++i)		
				normal[i] = (coord_t)(normal_f[i]*10000.0); //Fixed precision math at 1e-4
				
			if (dipole != 0.0 && !ct)
			{
				LOG_ERROR(log4cxx::Logger::getLogger("kineticmc.sim.initial"), "Cannot create a bilayer with no CT state and an interface dipole.  Failing.");
				throw std::exception();
			}
						
			LOG_INFO(log4cxx::Logger::getLogger("kineticmc.sim.initial"), "Created tilted bilayer at angle " << angle);
			LOG_INFO(log4cxx::Logger::getLogger("kineticmc.sim.initial"), "Normal was (" << normal_f[0] << ", " << normal_f[1] << ", " << normal_f[2] << ")");
		}
		
		ctx_free = mat1->context_free() && mat2->context_free();
		pos_indep = mat1->pos_independent() && mat2->pos_independent();
	}
	
	virtual ~BilayerGenerator()
	{
		if (m1)
			delete m1;
			
		if (m2)
			delete m2;
	}
	
	virtual LocalEnvironment generate(coord_t *pos, real_t lattice_constant, World *space)
	{	
		if (!tilted)
			return generate_notilt(pos, lattice_constant, space);
		else
			return generate_tilt(pos, lattice_constant, space);
	}
	
	virtual bool context_free() 
	{
		return ctx_free;
	}
};

#endif