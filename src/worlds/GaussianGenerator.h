/*
 * GaussianGenerator
 * take another generator and add some normally distributed noise with a given distribution
 * width to it, returning the modified energy levels.
 *
 *
 *
 */

#ifndef __GaussianGenerator_h__
#define __GaussianGenerator_h__

#include "Types.h"
#include "NormalRNG.h"

class GaussianGenerator : public WorldGenerator
{
	private:
	WorldGenerator *gen;
	NormalRNG		normal_rng;
	real_t			noise_dev;
		
	public:
	GaussianGenerator(WorldGenerator *root, real_t stdev, long n_seed) : gen(root), normal_rng(n_seed), noise_dev(stdev)
	{
	}
	
	~GaussianGenerator()
	{
		if (gen)
			delete gen;
	}
	
	virtual LocalEnvironment generate(coord_t *pos, real_t lattice_constant, World *space)
	{
		LocalEnvironment env = gen->generate(pos, lattice_constant, space);

		env.homo += normal_rng.gaussian(noise_dev);

		env.lumo += normal_rng.gaussian(noise_dev);
		
		return env;
	}
	
	virtual bool context_free() {return false;}; 
	virtual bool pos_independent() {return gen->pos_independent();};
};

#endif