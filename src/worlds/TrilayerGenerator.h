#ifndef __TrilayerGenerator_h__
#define __TrilayerGenerator_h__

#include "Types.h"
#include <stdlib.h>
#include <cmath>

class TrilayerGenerator : public WorldGenerator
{
	private:
	WorldGenerator *mat1;
	WorldGenerator *mat2;
	WorldGenerator *middle;
	
	coord_t middle_xmin;
	coord_t middle_xmax;
	
	public:
	TrilayerGenerator(WorldGenerator *m1, WorldGenerator *m2, WorldGenerator *mid, coord_t midsize)
		: mat1(m1), mat2(m2), middle(mid)
	{
		middle_xmax = midsize/2;
		middle_xmin = middle_xmax - midsize;
	}
	
	virtual ~TrilayerGenerator()
	{
		if (mat1)
			delete mat1;
			
		if (mat2)
			delete mat2;
		
		if (middle)
			delete middle;
	}
	
	virtual LocalEnvironment generate(coord_t *pos, real_t lattice_constant, World *space)
	{
		//Middle region extends [xmin, xmax) 
		if (pos[0] >= middle_xmin && pos[0] < middle_xmax)
			return middle->generate(pos, lattice_constant, space);
		
		if (pos[0] < 0)
			return mat1->generate(pos, lattice_constant, space);
		
		//Otherwise we're in material 2
		return mat2->generate(pos, lattice_constant, space);
	}
	
	virtual bool context_free() 
	{
		return mat1->context_free() && mat2->context_free() && middle->context_free();
	}
};

#endif