//PureMaterialSpec.h

#ifndef __PureMaterialSpec_h__
#define __PureMaterialSpec_h__

#include "Types.h"
#include "PureMaterialGenerator.h"

class PureMaterialSpec : public WorldGeneratorSpec
{
	private:
	real_t homo;
	real_t lumo;
	int tag;

	public:
	PureMaterialSpec(real_t h, real_t l, int t=0) : homo(h), lumo(l), tag(t) {};
	
	virtual WorldGenerator* create() {return new PureMaterialGenerator(homo, lumo, tag);};
};

#endif