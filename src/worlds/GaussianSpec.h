/*
 * GaussianSpec
 * take another spec and add some normally distributed noise with a given distribution
 * width to it, returning the modified energy levels.
 *
 *
 *
 */

#ifndef __GaussianSpec_h__ 
#define __GaussianSpec_h__

#include "Types.h"
#include "GaussianGenerator.h"

class GaussianSpec : public WorldGeneratorSpec
{
	private:
	WorldGeneratorSpec *m1;
	
	real_t noise;
	long seed;
	
	public:
	GaussianSpec(WorldGeneratorSpec *mat1, real_t noise, long n_seed=0) : m1(mat1), noise(noise), seed(n_seed) {};
	
	//Don't take ownership of m1, m2 since they could be (and probably are) coming from python.
	
	virtual WorldGenerator* create() {return new GaussianGenerator(m1->create(), noise, seed);};
};

#endif