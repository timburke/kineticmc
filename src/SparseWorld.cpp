//SpareWorld.cpp

#include "SparseWorld.h"
#include "DenseWorld.h"
#include <stdio.h>

SparseWorld::SparseWorld(coord_t *new_start, coord_t block, coord_t side) : block_side(block), is_leaf(false)
{
	//Initialize children
	for (size_t i=0; i<8; ++i)
		children[i] = NULL;
	
	//Save off the start and size
	for (size_t i=0; i<3; ++i)
	{
		start[i] = new_start[i];
		size[i] = side;
		pivot[i] = new_start[i] + side/2; //Store center for quickly determining which quadrant a given point is in
	}
	
	//Initialize our cache
	for (size_t i=0; i<kNumCarriers; ++i)
		last_region[i] = NULL;
	
	stats.cache_hits = 0;
	stats.cache_misses = 0;
	
	//If we're a 2x2x2 cube of 8 blocks, then we're the last SparseWorld in the list and our
	//children should be DenseWorld objects
	if (side == block*2)
		is_leaf = true;
}

void SparseWorld::reset()
{
	for (size_t i=0; i<8; ++i)
	{
		if (children[i])
			children[i]->reset();
	}
	
	//Initialize our cache
	for (size_t i=0; i<kNumCarriers; ++i)
		last_region[i] = NULL;
}

SparseWorld::~SparseWorld()
{
	for (size_t i=0; i<8; ++i)
	{
		if (children[i])
			delete children[i];
	}
}


/*
 * Efficiently map a point to the child that it corresponds to, assuming that is lies within our  
 * bounding box.  Basically, for each dimension we compute whether it is to the right or left of
 * the center of the box and then combine those three dimensions together.  So the order is X, Y then Z
 *
 */
size_t SparseWorld::find_child(coord_t *pt)
{
	size_t quad[3] = {0, 0, 0};
	
	for (size_t i=0; i<3; ++i)
	{
		if (pt[i] >= pivot[i])
			quad[i] = 1;
	}
	
	return quad[0] + quad[1]*2 + quad[2]*4;
}

/*
 * Build the bounding box for our children.  The best way seems to be just brute force
 */
void SparseWorld::build_child_start(size_t index, coord_t *out)
{
	coord_t hw = size[0]/2; //half-width
	
	out[0] = start[0];
	out[1] = start[1];
	out[2] = start[2];
	
	switch(index)
	{
		case 0: 
		//back-bottom-left
		break;
		
		case 1:
		//back-bottom-right
		out[0] += hw;
		break;
		
		case 2:
		//back-top-left
		out[1] += hw;
		break;
		
		case 3:
		//back-top-right
		out[0] += hw;
		out[1] += hw;
		break;
		
		case 4: 
		//front-bottom-left
		out[2] += hw;
		break;
		
		case 5:
		//front-bottom-right
		out[0] += hw;
		out[2] += hw;
		break;
		
		case 6:
		//front-top-left
		out[1] += hw;
		out[2] += hw;
		break;
		
		case 7:
		//front-top-right
		out[0] += hw;
		out[1] += hw;
		out[2] += hw;
		break;
	}
}

void SparseWorld::ensure_child_exists(size_t index)
{
	if (children[index] == NULL)
		allocate_child(index);
}
		
void SparseWorld::allocate_child(size_t index)
{
	coord_t start[3];
	
	build_child_start(index, start);
	
	if (is_leaf)
		children[index] = new DenseWorld(start, block_side);
	else
	{
		//printf("Allocating SparseWorld with side %ld\n", size[0]/2);
		children[index] = new SparseWorld(start, block_side, size[0]/2);
	}
}

size_t SparseWorld::lattice_size()
{
	size_t sum = 0;
	for (size_t i=0; i<8; ++i)
	{
		if (children[i])
			sum += children[i]->lattice_size();
	}
	
	return sum;
}

DenseWorld *SparseWorld::get_leaf(coord_t *pos, CarrierType type, DenseWorld **cache)
{
	//Check the last DenseWorld this carrier was in as a timesaving cache check
	//cache == NULL means we're the top level SparseWorld
	if (cache == NULL && last_region[type] != NULL)
	{
		if (last_region[type]->contains(pos, type))
		{
			++stats.cache_hits;
			return last_region[type];
		}
	}
	
	size_t child = find_child(pos);
	
	ensure_child_exists(child);
	
	if (is_leaf)
	{
		if (cache != NULL)
		{
			*cache = (DenseWorld*)children[child];
		}
		
		return (DenseWorld*)children[child];
	}
	
	/*
	If we're top level pass down where to cache the found DenseWorld that this point lives in.
	If we're not top level, continue passing down the cache location we were passed in.
	*/
	if (cache == NULL)
	{
		++stats.cache_misses;
		return ((SparseWorld*)children[child])->get_leaf(pos, type, &last_region[type]); //Only cache on the top level since if we miss that cache, we'd miss them all
	}
	else
		return ((SparseWorld*)children[child])->get_leaf(pos, type, cache);
}

LocalEnvironment & SparseWorld::get_value(coord_t *pos, CarrierType type)
{
	return get_leaf(pos, type)->get_value(pos, type);
}

bool SparseWorld::value_exists(coord_t *pos, CarrierType type)
{
	return get_leaf(pos, type)->value_exists(pos, type);
}

void SparseWorld::set_value(coord_t *pos, const LocalEnvironment &val, CarrierType type)
{
	get_leaf(pos, type)->set_value(pos, val, type);
}
	
bool SparseWorld::contains(coord_t *pos, CarrierType type)
{
	for (size_t i=0; i<3; ++i)
	{
		if (pos[i] < start[i] || pos[i] >= start[i] + size[i])
			return false;
	}
	
	return true;
}

void SparseWorld::append_xyz(FILE *f, XYZHelper &helper)
{
	for (size_t i=0; i<8; ++i)
	{
		if (children[i])
		{
			if (is_leaf)
			{
				DenseWorld *w = (DenseWorld*)children[i];
				w->append_xyz(f, helper);
			}
			else
			{
				SparseWorld *w = (SparseWorld*)children[i];
				w->append_xyz(f, helper);
			}
		}
	}
}

void SparseWorld::write_xyz(const std::string &name)
{
	FILE *f = fopen(name.c_str(), "w");
	
	if (!f)
	{
		printf("Could not open file %s\n", name.c_str());
		return;
	}
	
	XYZHelper helper;
	size_t count;
	
	count = lattice_size();
	
	fprintf(f, "%lu\n", count);
	fprintf(f, "Created World\n");
	
	append_xyz(f, helper);
	
	fclose(f);
}

void SparseWorld::print_statistics()
{
	printf("Cache hits:%ld\n", stats.cache_hits);
	printf("Cache misses:%ld\n", stats.cache_misses);
}