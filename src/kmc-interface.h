//kmc-interface.h

#ifndef __kmc_interface__
#define __kmc_interface__

#include <string>
#include "Types.h"

/*
 * KMCInterface
 * Abstract base class for a templated MCSimulation subclass that defines
 * the interface between control-sim and the various optimized MCSimulation 
 * implementations.
 */
 
class MobilityModel;
class WorldGenerator;
class World;

 
class KMCInterface
{
	public:
	virtual ~KMCInterface() {};
	
	virtual void initialize_simulation(real_t lattice, real_t separation, real_t eps, real_t temp) = 0;
	virtual void set_carrier_params(MobilityModel *electron_mob, MobilityModel *hole_mob, real_t recomb_time) = 0;
	virtual void set_field(real_t mag, FieldType type) = 0;
	virtual void set_field(real_t *field) = 0;

	virtual void set_world_generator(WorldGenerator *g, bool own=true) = 0;
	virtual void set_world(World *w, bool own=true)  = 0;
	virtual void fill_xy_world_slice(int side, real_t *n) = 0;
	
	
	//Initial state creation routines
	virtual void create_initial_state_bulk() = 0;
	virtual void create_state(coord_t *elec_pos, coord_t *hole_pos) = 0;

	
	virtual void run(size_t max_steps, bool trace) = 0;
	virtual void run_simd(size_t max_steps, bool trace) = 0;
	
	virtual bool escaped() = 0;
	
	virtual SimulationData *get_results() = 0;
	
	virtual void print_statistics() = 0;
	virtual void visualize_world(const std::string &filename, size_t side) = 0;
	virtual void save_world(const std::string &filename) = 0;
	
	//Interactive Calculation Routines
	virtual std::vector<real_t> calc_hopping_rates() = 0;
	virtual void calc_with_simd(bool val) = 0;
};

#endif