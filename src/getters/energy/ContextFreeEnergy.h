//ContextFreeEnergy.h

#ifndef __ContextFreeEnergy_h__
#define __ContextFreeEnergy_h__

#include "Types.h"

class World;
class WorldGenerator;

class ContextFreeEnergy
{	
	public:
	real_t electron_energy(coord_t *pos, real_t lattice_const, World *space, WorldGenerator *gen);
	void fill_electron_energies(coord_t *pos, real_t lattice_const, World *space, WorldGenerator *gen, real_t *out);
	
	real_t hole_energy(coord_t *pos, real_t lattice_const, World *space, WorldGenerator *gen);
	void fill_hole_energies(coord_t *pos, real_t lattice_const, World *space, WorldGenerator *gen, real_t *out);
};

#endif