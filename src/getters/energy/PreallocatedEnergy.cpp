//PreallocatedEnergy.cpp
#include "PreallocatedEnergy.h"
#include "Types.h"
#include "World.h"
#include "PreallocatedWorld.h"

PreallocatedEnergy::PreallocatedEnergy()
{
	//progress_l = log4cxx::Logger::getLogger("kineticmc.sim.progress");
}

real_t PreallocatedEnergy::electron_energy(coord_t *pos, real_t lattice_const, World *space, WorldGenerator *gen)
{
	return space->get_value(pos, kElectron).lumo;
}

real_t PreallocatedEnergy::hole_energy(coord_t *pos, real_t lattice_const, World *space, WorldGenerator *gen)
{
	return space->get_value(pos, kHole).homo;
}

void PreallocatedEnergy::fill_electron_energies(coord_t *pos, real_t lattice_const, World *space, WorldGenerator *gen, real_t *out)
{
	PreallocatedWorld *w = (PreallocatedWorld*)space;
	
	w->fill_electron_energies(pos, out);
}
void PreallocatedEnergy::fill_hole_energies(coord_t *pos, real_t lattice_const, World *space, WorldGenerator *gen, real_t *out)
{
	PreallocatedWorld *w = (PreallocatedWorld*)space;
	
	w->fill_hole_energies(pos, out);
}