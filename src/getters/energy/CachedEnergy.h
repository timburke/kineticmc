//CachedEnergy.h
#ifndef __CachedEnergy_h__
#define __CachedEnergy_h__

#include "Types.h"

class World;
class WorldGenerator;

class CachedEnergy
{
	private:
	log4cxx::LoggerPtr	progress_l;
	
	public:
	CachedEnergy();
	
	real_t electron_energy(coord_t *pos, real_t lattice_const, World *space, WorldGenerator *gen);
	real_t hole_energy(coord_t *pos, real_t lattice_const, World *space, WorldGenerator *gen);

	void fill_electron_energies(coord_t *pos, real_t lattice_const, World *space, WorldGenerator *gen, real_t *out);
	void fill_hole_energies(coord_t *pos, real_t lattice_const, World *space, WorldGenerator *gen, real_t *out);
};

#endif