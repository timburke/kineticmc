//CachedEnergy.cpp

#include "CachedEnergy.h"
#include "World.h"
#include <exception>

CachedEnergy::CachedEnergy()
{
	progress_l = log4cxx::Logger::getLogger("kineticmc.sim.progress");
}

real_t CachedEnergy::electron_energy(coord_t *pos, real_t lattice_const, World *space, WorldGenerator *gen)
{	
	//Otherwise we need to consult our world and update it if necessary
	if (!space->contains(pos, kElectron))
	{
		LOG_ERROR(progress_l, "We have exceeded the dimensions of this simulation!\n");
		LOG_ERROR(progress_l, "Position was:\n");
		LOG_ERROR(progress_l, "(" << pos[0] << ", " << pos[1] << ", " << pos[2] << ")");
		throw std::exception();
	}
	
	if (space->value_exists(pos, kElectron))
	{
		LocalEnvironment &env = space->get_value(pos, kElectron);
		
		return env.lumo;
	}
	else
	{
		//Create a new local environment and store it
		LocalEnvironment env = gen->generate(pos, lattice_const, space);

		space->set_value(pos, env, kElectron);
		
		return env.lumo;
	}
}

real_t CachedEnergy::hole_energy(coord_t *pos, real_t lattice_const, World *space, WorldGenerator *gen)
{
	//Otherwise we need to consult our world and update it if necessary
	if (!space->contains(pos, kHole))
	{
		LOG_ERROR(progress_l, "We have exceeded the dimensions of this simulation!\n");
		LOG_ERROR(progress_l, "Position was:\n");
		LOG_ERROR(progress_l, "(" << pos[0] << ", " << pos[1] << ", " << pos[2] << ")");
		throw std::exception();
	}
	
	if (space->value_exists(pos, kHole))
	{
		LocalEnvironment &env = space->get_value(pos, kHole);

		return env.homo;
	}
	else
	{
		//Create a new local environment and store it
		LocalEnvironment env = gen->generate(pos, lattice_const, space);

		space->set_value(pos, env, kHole);

		return env.homo;
	}
}

void CachedEnergy::fill_electron_energies(coord_t *pos, real_t lattice_const, World *space, WorldGenerator *gen, real_t *out)
{
	for (size_t i=0; i<6; ++i)
	{
		pos[i/2] += (1 - (i%2)*2);
		
		out[i] = electron_energy(pos, lattice_const, space, gen);
		
		pos[i/2] -= (1 - (i%2)*2);
	}
}

void CachedEnergy::fill_hole_energies(coord_t *pos, real_t lattice_const, World *space, WorldGenerator *gen, real_t *out)
{
	for (size_t i=0; i<6; ++i)
	{
		pos[i/2] += (1 - (i%2)*2);
		
		out[i] = hole_energy(pos, lattice_const, space, gen);
		
		pos[i/2] -= (1 - (i%2)*2);
	}
}