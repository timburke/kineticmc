//ContextFreeEnergy.cpp

#include "ContextFreeEnergy.h"

real_t ContextFreeEnergy::electron_energy(coord_t *pos, real_t lattice_const, World *space, WorldGenerator *gen)
{
	//Create the energy level on the fly
	LocalEnvironment env = gen->generate(pos, lattice_const, space);
	
	return env.lumo;
}


real_t ContextFreeEnergy::hole_energy(coord_t *pos, real_t lattice_const, World *space, WorldGenerator *gen)
{
	//Create the energy level on the fly
	LocalEnvironment env = gen->generate(pos, lattice_const, space);
	
	return env.homo; //holes float

}

void ContextFreeEnergy::fill_electron_energies(coord_t *pos, real_t lattice_const, World *space, WorldGenerator *gen, real_t *out)
{
	gen->fill_neighbor_lumos(pos, lattice_const, space, out);
}

void ContextFreeEnergy::fill_hole_energies(coord_t *pos, real_t lattice_const, World *space, WorldGenerator *gen, real_t *out)
{
	gen->fill_neighbor_homos(pos, lattice_const, space, out);
}
