//PreallocatedEnergy.h

/*
 * Get energies from a preallocated world with periodic boundary conditions so that
 * we can avoid having to save energies and generate on the fly as well avoiding checks
 * for overflow.
 */
 
#ifndef __PreallocatedEnergy_h__
#define __PreallocatedEnergy_h__

#include "BasicTypes.h"

class World;
class WorldGenerator;

class PreallocatedEnergy
{
	private:
	
	public:
	PreallocatedEnergy();
	
	real_t electron_energy(coord_t *pos, real_t lattice_const, World *space, WorldGenerator *gen);
	real_t hole_energy(coord_t *pos, real_t lattice_const, World *space, WorldGenerator *gen);

	void fill_electron_energies(coord_t *pos, real_t lattice_const, World *space, WorldGenerator *gen, real_t *out);
	void fill_hole_energies(coord_t *pos, real_t lattice_const, World *space, WorldGenerator *gen, real_t *out);
};

#endif