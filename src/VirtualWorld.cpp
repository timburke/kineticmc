//World.cpp

#include "VirtualWorld.h"
#include <exception>

LocalEnvironment &	VirtualWorld::get_value(coord_t *pos, CarrierType type)
{
	throw std::exception();
}

bool VirtualWorld::value_exists(coord_t *pos, CarrierType type)
{
	return false;
}

void VirtualWorld::set_value(coord_t *pos, const LocalEnvironment &val, CarrierType type)
{
	throw std::exception();
}

bool VirtualWorld::contains(coord_t *pos, CarrierType type)
{
	return false;
}

void VirtualWorld::reset()
{
}

void VirtualWorld::write_xyz(const std::string &name)
{
	throw std::exception();
}

bool VirtualWorld::context_free() 
{
	return true;
}

size_t VirtualWorld::lattice_size()
{
	return 0;
}