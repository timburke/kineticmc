#ifndef __SemiperiodicLattice_h__
#define __SemiperiodicLattice_h__

#include <emmintrin.h>
#include <pmmintrin.h>
#include <smmintrin.h>
#include <tmmintrin.h>
#include "Lattice.h"

/*
 * Simulate a lattice with periodic boundary conditions in x,y and nonperiodic BC in z.
 */

class SemiperiodicLattice : public Lattice
{
	private:
	__m128i semiperiodic_sep2_simd(size_t i, size_t j);
	bool 	invalid_carrier();

	void print_simd(__m128i ints);
	void print_simd(__m128 floats);

	protected:
	virtual float calc_energy();

	public:
	SemiperiodicLattice();
};

#endif