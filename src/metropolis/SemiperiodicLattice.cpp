#include "SemiperiodicLattice.h"
#include "simd.h"

SemiperiodicLattice::SemiperiodicLattice()
{
	set_periodicity(true, true, false);
}

/*
 * Return a separation vector periodic in x and y and not periodic in z.
 */

__m128i SemiperiodicLattice::semiperiodic_sep2_simd(size_t i, size_t j)
{
	__m128i p1 = _mm_load_si128((__m128i*)(sse_data.positions + 4*i));	//load aligned
	__m128i p2 = _mm_load_si128((__m128i*)(sse_data.positions + 4*j));
	__m128i sep = _mm_sub_epi32(p1, p2);    //get separation
	__m128i bnd = _mm_load_si128((__m128i*)dims);

	sep = _mm_abs_epi32(sep);

	__m128i mirror_sep = _mm_sub_epi32(bnd, sep);
	__m128i min_sep = _mm_min_epi32(sep, mirror_sep);
	
	min_sep = _mm_blend_epi16(min_sep, sep, 0b00110000);

	//d2[i] = min_sep[i]^2
	__m128i d2 = _mm_mullo_epi32(min_sep, min_sep);
	d2 = _mm_hadd_epi32(d2, d2);
	d2 = _mm_hadd_epi32(d2, d2);		//now the sum is in the lowest 32 bits of d2

	return _mm_shuffle_epi32(d2, 0);	//broadcast the low 32 bits to all of the dwords
}

bool SemiperiodicLattice::invalid_carrier()
{
	for (size_t i=0; i<num_carriers; ++i)
	{
		coord_t *pos = &sse_data.positions[4*i];

		coord_t z_pos = pos[2] - origin[2];

		if (z_pos < 0 || z_pos >= dims[2])
			return true;
	}

	return false;
}

void SemiperiodicLattice::print_simd(__m128i ints)
{
	int f1, f2, f3, f4;

	f1 = _mm_extract_epi32(ints, 0);
	f2 = _mm_extract_epi32(ints, 1);
	f3 = _mm_extract_epi32(ints, 2);
	f4 = _mm_extract_epi32(ints, 3);

	printf("__m128i: (%d, %d, %d, %d)\n", f1, f2, f3, f4);
}

void SemiperiodicLattice::print_simd(__m128 floats)
{
	float f1, f2, f3, f4;

	_MM_EXTRACT_FLOAT(f1, floats, 0);
	_MM_EXTRACT_FLOAT(f2, floats, 1);
	_MM_EXTRACT_FLOAT(f3, floats, 2);
	_MM_EXTRACT_FLOAT(f4, floats, 3);

	printf("__m128: (%f, %f, %f, %f)\n", f1, f2, f3, f4);
}

float SemiperiodicLattice::calc_energy()
{
	//Make sure all carriers are inside the z bounds
	if (invalid_carrier())
		return 1e12;

	float en = 0.0;

	__m128 bfact = _mm_set1_ps(binding_factor);
	__m128 energy_acc = _mm_setzero_ps();

	//We're guaranteed that len pair_idx is a multiple of 8 (2*4) by Lattice class invariant.
	//so work 4 pairs at a time.
	for (size_t i=0; i<pair_idx.size(); i+=8)
	{
		size_t i1 = pair_idx[i+0], j1 = pair_idx[i+1];
		size_t i2 = pair_idx[i+2], j2 = pair_idx[i+3];
		size_t i3 = pair_idx[i+4], j3 = pair_idx[i+5];
		size_t i4 = pair_idx[i+6], j4 = pair_idx[i+7];

		__m128i sep1 = semiperiodic_sep2_simd(i1, j1);
		__m128i sep2 = semiperiodic_sep2_simd(i2, j2);
		__m128i sep3 = semiperiodic_sep2_simd(i3, j3);
		__m128i sep4 = semiperiodic_sep2_simd(i4, j4);

		__m128i seps;

		//Blend all 4 separations into 1 register
		seps = _mm_blend_epi16(seps, sep1, 0b00000011);
		seps = _mm_blend_epi16(seps, sep2, 0b00001100);
		seps = _mm_blend_epi16(seps, sep3, 0b00110000);
		seps = _mm_blend_epi16(seps, sep4, 0b11000000);

		//Check if any carriers have recombined, in which case return the sentinel value.
		__m128i zero = _mm_setzero_si128();
		if(_mm_movemask_epi8(_mm_cmpeq_epi32(seps,zero)) != 0)
			return 1e12;

		__m128 seps_float = _mm_cvtepi32_ps(seps);
		__m128 signs = _mm_load_ps((&sse_data.pair_signs[i>>1]));	//Load the precomputed signs

		//Now seps contains all of the 4 squared separations
		//Calculate the binding energy of all 4 pairs
		__m128 energy;
		
		energy = _mm_mul_ps(bfact, simd_inv_dist(seps_float));
		energy = _mm_mul_ps(energy, signs);

		//Accumulate the 4 energies in parallel and combine after the loop
		energy_acc = _mm_add_ps(energy_acc, energy);		
	}

	//Now add all of the energies together and extract the sum into en
	energy_acc = _mm_hadd_ps(energy_acc, energy_acc);
	energy_acc = _mm_hadd_ps(energy_acc, energy_acc);
	
	float extracted_e;
	_MM_EXTRACT_FLOAT(extracted_e, energy_acc, 0);

	en += world_energy();
	en += extracted_e;
	
	return en;
}
