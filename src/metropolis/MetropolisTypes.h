#ifndef __MetropolisTypes_h__
#define __MetropolisTypes_h__

#include "Types.h"

//MetroCarrier size 16.
struct MetroCarrier
{
	coord_t pos[4];

	float sign;
	size_t accept;
	size_t reject;
	float  randomize;
};

#endif