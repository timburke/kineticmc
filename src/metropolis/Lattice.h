#ifndef __Lattice_h__
#define __Lattice_h__

#include "PreallocatedWorld.h"
#include "Types.h"
#include "MetropolisTypes.h"
#include "Sampler.h"
#include "SampledFunction.h"
#include <vector>
#include <gsl/gsl_rng.h>
#include <emmintrin.h>
#include <pmmintrin.h>
#include <smmintrin.h>
#include <tmmintrin.h>

struct UpdateRecord
{
	MetroCarrier	*carrier;
	coord_t	 		old_pos[3];
	int 			carrier_idx;
};

struct SSEData
{
	int32_t	*positions;
	float 	*pair_signs;
};

class Lattice
{
	private:
	gsl_rng 					*rng_uniform;

	float 						accept_target;
	float						slop;
	size_t						eval_delay;

	UpdateRecord 				last_update;
	float						last_energy;

	std::vector<SamplerObject*>	samplers;

	void clear_stats();
	void update_pos();
	int  random_range(int max);

	void alloc_carriers(int num);
	void dealloc();

	void create_pairs();

	void randomize_carriers();
	void randomize_carrier(int carrier);

	float calc_prob(float energy);

	void init_samplers(int num_samples);
	void accept_update();
	void reject_update();

	void from_simd();

	void sample(int i);

	protected:
	//Simulation Information
	MetroCarrier 				*carriers;
	std::vector<size_t> 		pair_idx;

	SSEData						sse_data;

	PreallocatedWorld 			*world;
	float temp;
	float eps_r;
	float neg_inv_kT;
	float binding_factor;
	float field;				//an electric field in the z direction (V/nm)

	size_t						num_carriers;
	coord_t 					origin[3];
	coord_t						dims[3];
	bool						periodic[3];

	//Subclasses need to fill in this function
	virtual float calc_energy() = 0;

	//Helper functions for common routines
	void  fill_sep(MetroCarrier &c1, MetroCarrier &c2, coord_t *out);
	float world_energy();

	public:
	Lattice();
	virtual ~Lattice();

	void set_world(WorldGenerator *gen, int x, int y, int z);
	void set_periodicity(bool x_periodic, bool y_periodic, bool z_periodic);

	void set_numpairs(int pairs, float randomize=0.5);
	void set_temp(float temp);
	void set_epsilon(float eps);
	void set_field(float field); //Set the field strength in V/cm
	void set_slop(float slop);
	void set_seed(unsigned long int seed);

	void mc_step();
	void metropolis(int num_samples, int discard, int sample_every);

	void add_sampler(SamplerObject *sampler);

	//Debug Routines
	void print_carriers();

	void  init_simulation();
};

#endif