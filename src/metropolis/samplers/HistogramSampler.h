#ifndef __HistogramSampler_h__
#define __HistogramSampler_h__

#include "Sampler.h"
#include <vector>
#include <math.h>

class HistogramSampler : public SamplerObject
{
	protected:
	std::vector<float> 	histogram;
	float hist_min;
	float hist_max;
	float range;

	void add_value(float value)
	{
		float offset = (value - hist_min)/range;

		int index = (int)floorf(offset*histogram.size());

		if (index < 0 || index >= histogram.size())
			return;

		histogram[index] += 1.0;
	}

	//Subclasses should override this function
	virtual void fill_histogram(MetroCarrier *carriers, size_t num_carriers, float energy) = 0;

	public:
	HistogramSampler(int num_bins) : SamplerObject(num_bins)
	{
		histogram.resize(num_bins);
	}

	void set_range(float min, float max)
	{
		hist_min = min;
		hist_max = max;

		range = hist_max - hist_min;
	}

	virtual void sample(MetroCarrier *carriers, size_t num_carriers, float energy, int i)
	{
		for (int j=0; j<histogram.size(); ++j)
			histogram[j] = 0.0;

		this->fill_histogram(carriers, num_carriers, energy);

		for (int j=0; j<histogram.size(); ++j)
			samples[i*histogram.size() + j] = histogram[j];
	}
};

#endif