#ifndef __SeparationSampler_h__
#define __SeparationSampler_h__

#include "Sampler.h"
#include <emmintrin.h>
#include <pmmintrin.h>
#include <smmintrin.h>
#include <tmmintrin.h>
#include <math.h>

class SeparationSampler : public SamplerObject
{
	private:
	coord_t semiperiodic_sep2_simd(MetroCarrier &c1, MetroCarrier &c2, coord_t *dims)
	{
		__m128i p1 = _mm_load_si128((__m128i*)c1.pos);	//load unaligned
		__m128i p2 = _mm_load_si128((__m128i*)c2.pos);
		__m128i sep = _mm_sub_epi32(p1, p2);    //get separation
		__m128i bnd = _mm_load_si128((__m128i*)dims);

		sep = _mm_abs_epi32(sep);

		__m128i mirror_sep = _mm_sub_epi32(bnd, sep);
		__m128i min_sep = _mm_min_epi32(sep, mirror_sep);
		
		min_sep = _mm_blend_epi16(min_sep, sep, 0b00110000);

		//d2[i] = min_sep[i]^2
		__m128i d2 = _mm_mullo_epi32(min_sep, min_sep);
		d2 = _mm_hadd_epi32(d2, d2);
		d2 = _mm_hadd_epi32(d2, d2);	//now the sum is in the lowest 32 bits of d2

		return _mm_extract_epi32(d2, 0);
	}

	protected:
	//Calculate the average distance between each carrier and its closest neighbor
	virtual float calc_sample(MetroCarrier *carriers, size_t num_carriers, float energy)
	{
		float avg_sep = 0.0;
		coord_t dims[3];

		world->fill_dims(dims);

		for (size_t i=0; i<num_carriers; ++i)
		{
			coord_t min_sep = 999999;
			for (size_t j=0; j<num_carriers; ++j)
			{
				if (i == j)
					continue;

				coord_t sep = semiperiodic_sep2_simd(carriers[i], carriers[j], dims);
				if (sep < min_sep)
					min_sep = sep;
			}

			avg_sep += sqrtf(min_sep);
		}

		avg_sep /= (float)(num_carriers);

		return avg_sep;
	};

	public:
};

#endif