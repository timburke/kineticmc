#ifndef __EnergyHistogramSampler_h__
#define __EnergyHistogramSampler_h__

#include "HistogramSampler.h"

class EnergyHistogramSampler : public HistogramSampler
{
	protected:
	virtual void fill_histogram(MetroCarrier *carriers, size_t num_carriers, float energy)
	{
		for (size_t i=0; i<num_carriers; ++i)
		{
			LocalEnvironment &env = world->get_value(carriers[i].pos, kElectron);

			if (carriers[i].sign < 0.0)
				add_value(env.lumo);
			else
				add_value(env.homo);
		}
	}

	public:
	EnergyHistogramSampler(float min, float max, float bin_size) : HistogramSampler((int)((max-min)/bin_size))
	{
		set_range(min, max);
	}


};

#endif