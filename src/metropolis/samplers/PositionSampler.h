#ifndef __PositionSampler_h__
#define __PositionSampler_h__

#include "Sampler.h"

class PositionSampler : public SamplerObject
{
	private:
	int axis;

	protected:
	virtual float calc_sample(MetroCarrier *carriers, size_t num_carriers, float energy)
	{
		float avg_p = 0.0;

		for (size_t i=0; i<num_carriers; ++i)
			avg_p += carriers[i].pos[axis];

		return avg_p / num_carriers;
	};

	public:
	PositionSampler(int dir)
	{
		axis = dir;
	};
};

#endif