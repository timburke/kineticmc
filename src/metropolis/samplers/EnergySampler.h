#ifndef __EnergySampler_h__
#define __EnergySampler_h__

#include "Sampler.h"

class EnergySampler : public SamplerObject
{
	protected:
	virtual float calc_sample(MetroCarrier *carriers, size_t num_carriers, float energy)
	{
		return energy;
	};
};

#endif