#include "Sampler.h"

SamplerObject::SamplerObject()
{
	sample_size = 1;
}

SamplerObject::SamplerObject(int size)
{
	sample_size = size;
}

void SamplerObject::set_numsamples(int num)
{
	samples.resize(num*sample_size);
}

void SamplerObject::np_fill(double *n, int n_dim)
{
	if (n_dim != samples.size())
		return;

	for (size_t i=0; i<samples.size(); ++i)
			n[i] = samples[i];
} 

void SamplerObject::sample(MetroCarrier *carriers, size_t num_carriers, float energy, int i)
{
	samples[i] = this->calc_sample(carriers, num_carriers, energy);
}

void SamplerObject::set_world(PreallocatedWorld *w)
{
	world = w;
}

int SamplerObject::get_samplesize()
{
	return sample_size;
}

int SamplerObject::get_length()
{
	return samples.size();
}