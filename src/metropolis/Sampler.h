#ifndef __Sampler_h__
#define __Sampler_h__

#include <vector>
#include "MetropolisTypes.h"
#include "PreallocatedWorld.h"

class SamplerObject
{
	protected:
	std::vector<float> 	samples;
	int 				sample_size;
	PreallocatedWorld 	*world;

	//Samplers logging a single value should override this function.
	virtual float calc_sample(MetroCarrier *carriers, size_t num_carriers, float energy) {return 0.0;};

	public:
	SamplerObject();
	SamplerObject(int sample_size);
	virtual ~SamplerObject() {};

	void set_numsamples(int num);
	void np_fill(double *n, int n_dim); 
	void set_world(PreallocatedWorld *world);

	int get_samplesize();
	int get_length();			//return the total length num_samples*sample_size

	//Samplers logging many values should override this function.
	virtual void sample(MetroCarrier *carriers, size_t num_carriers, float energy, int i);
};

#endif