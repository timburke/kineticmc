#include "Lattice.h"
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string>

Lattice::Lattice() : world(NULL)
{
	slop 		  = 0.1;
	eval_delay 	  = 10000;
	eps_r 		  = 4.0;
	field 		  = 0.0;

	rng_uniform = gsl_rng_alloc(gsl_rng_mt19937);

	set_periodicity(true, true, true); //By default make the lattice completely periodic

	set_seed(0);

	carriers = NULL;
	num_carriers = 0;

	sse_data.positions = NULL;
	sse_data.pair_signs = NULL;
}

Lattice::~Lattice()
{
	gsl_rng_free(rng_uniform);
	dealloc();
}

void Lattice::set_world(WorldGenerator *gen, int x, int y, int z)
{
	coord_t loc[3];

	std::vector<int> smallest;

	if (world)
	{
		delete world;
		world = NULL;
	}

	smallest.resize(3);
	smallest[0] = 0;
	smallest[1] = 0;
	smallest[2] = 0;
	

	world = new PreallocatedWorld(x, y, z);
	world->set_smallest(smallest);

	for (loc[0]=0; loc[0]<x; ++loc[0])
	{
		for (loc[1]=0; loc[1]<y; ++loc[1])
		{
			for (loc[2]=0; loc[2]<z; ++loc[2])
				world->set_value(loc, gen->generate(loc, 10.0, world), kElectron);
		}
	}
}

void Lattice::set_epsilon(float eps)
{
	eps_r = eps;
}

void Lattice::clear_stats()
{
	for (int i=0; i<num_carriers; ++i)
	{
			carriers[i].accept = 0;
			carriers[i].reject = 0;
			carriers[i].randomize = 0.5;
	}
}

void Lattice::dealloc()
{
	if (carriers)
	{
		free(carriers);
		carriers = NULL;
	}

	if (sse_data.positions)
	{
		free(sse_data.positions);
		sse_data.positions = NULL;
	}

	if (sse_data.pair_signs)
	{
		free(sse_data.pair_signs);
		sse_data.pair_signs = NULL;
	}
}

void Lattice::alloc_carriers(int num)
{
	int num_pairs = num*(num-1)/2;
	dealloc();

	int res = posix_memalign((void**)&carriers, 16, num*sizeof(MetroCarrier));
	if (res != 0)
		throw std::string("Could not allocate aligned memory for SSE operations.");

	res = posix_memalign((void**)&sse_data.positions, 16, num*sizeof(uint32_t)*4);
	if (res != 0)
		throw std::string("Could not allocate aligned memory for SSE operations.");

	res = posix_memalign((void**)&sse_data.pair_signs, 16, (num_pairs)*sizeof(float));
	if (res != 0)
		throw std::string("Could not allocate aligned memory for SSE operations.");
}

void Lattice::create_pairs()
{
	pair_idx.resize(num_carriers*(num_carriers-1));
	size_t out_i = 0;
	size_t out_sign = 0;

	for (size_t i=0; i<num_carriers; ++i)
	{
		for (size_t j=i+1; j<num_carriers; ++j)
		{
			pair_idx[out_i++] = i;
			pair_idx[out_i++] = j;

			sse_data.pair_signs[out_sign++] = carriers[i].sign*carriers[j].sign;
		}
	}

	if (out_sign != (num_carriers*(num_carriers-1)/2))
		printf("Invalid number of carrier pairs created.\n");

	if (out_i != pair_idx.size())
		printf("Invalid number of carrier pairs created.  Vector size: %lu, number filled: %lu\n", pair_idx.size(), out_i);
}

void Lattice::set_numpairs(int num, float randomize)
{
	if ((num%4) != 0)
	{
		printf("You must have an number of carrier pairs divisible by 4 so that the SIMD algorithms can work speedily.\n");
		return;
	}

	alloc_carriers(num*2);
	num_carriers = num*2;

	for (int i=0; i<num_carriers; ++i)
	{
		//Make equal numbers of electrons and holes
		if (i%2 == 0)
			carriers[i].sign = -1.0;
		else
			carriers[i].sign = 1.0;

		carriers[i].randomize = randomize;
	}

	//Zero out the acceptance ratio information to start
	clear_stats();
	create_pairs();
}

int Lattice::random_range(int max)
{
	return gsl_rng_uniform_int(rng_uniform, max+1);
}

void Lattice::sample(int i)
{
	//Load positions back into MetroCarrier objects
	from_simd();

	for (std::vector<SamplerObject*>::iterator it = samplers.begin(); it != samplers.end(); ++it)
		(*it)->sample(carriers, num_carriers, last_energy, i);
}

void Lattice::update_pos()
{
	int carrier = random_range(num_carriers-1);

	//Save off our last location in case this move is rejected
	last_update.old_pos[0] = sse_data.positions[carrier*4 + 0];
	last_update.old_pos[1] = sse_data.positions[carrier*4 + 1];
	last_update.old_pos[2] = sse_data.positions[carrier*4 + 2];
	last_update.carrier = &carriers[carrier];
	last_update.carrier_idx = carrier;

	//Each carrier has a chance of being randomized that is optimized
	//to get a good acceptance ratio.
	if (gsl_rng_uniform(rng_uniform) < carriers[carrier].randomize)
	{
		randomize_carrier(carrier);
		return;
	}

	//Otherwise, go to one of our close neighbors uniformly
	int signed_dir = random_range(5);
	int sign = (signed_dir % 2)*2 - 1;
	int dir = signed_dir / 2;

	int step = random_range(2) + 1;

	//Update the coordinate position and if we're periodic, wrap around
	sse_data.positions[carrier*4 + dir] += step*sign;
	if (periodic[dir])
	{
		while (sse_data.positions[carrier*4 + dir] < origin[dir])
			sse_data.positions[carrier*4 + dir] += dims[dir];

		while (sse_data.positions[carrier*4 + dir] >= (origin[dir] + dims[dir]))
			sse_data.positions[carrier*4 + dir] -= dims[dir];
	}
}

void Lattice::set_seed(unsigned long int s)
{
	if (s == 0)
		s = time(NULL);

	gsl_rng_set(rng_uniform, s);
}

void Lattice::reject_update()
{
	last_update.carrier->reject += 1;

	sse_data.positions[last_update.carrier_idx*4 + 0] = last_update.old_pos[0];
	sse_data.positions[last_update.carrier_idx*4 + 1] = last_update.old_pos[1];
	sse_data.positions[last_update.carrier_idx*4 + 2] = last_update.old_pos[2];
}

void Lattice::accept_update()
{
	last_update.carrier->accept += 1;
}

float Lattice::calc_prob(float energy)
{
	float prob = expf(energy*neg_inv_kT);

	return prob;
}

void Lattice::set_temp(float new_t)
{
	temp = new_t;

	neg_inv_kT = -1.0/(kB_ev*temp);
}

void Lattice::from_simd()
{
	for (size_t i=0; i<num_carriers; ++i)
	{
		for (size_t j=0; j<4; ++j)
			carriers[i].pos[j] = sse_data.positions[(i*4) + j];
	}
}

void Lattice::set_field(float new_f)
{
	field = new_f*1e-7; //new_f in V/cm, field stored in V/nm
}

void Lattice::init_simulation()
{
	if (!world)
		return;

	world->fill_dims(dims);
	world->fill_smallest(origin);

	clear_stats();
	binding_factor = e_charge / (4*pi_f*eps_0*eps_r) * 1e9; //this times 1/r is the coulomb attraction for 2 point charges on a 1nm lattice
}

void Lattice::randomize_carrier(int carrier)
{
	for (size_t j=0; j<3; ++j)
	{
		coord_t p = random_range(dims[j]-1) - origin[j];
		sse_data.positions[4*carrier + j] = p;
	}

	sse_data.positions[4*carrier + 3] = 0; //initialize the 4th component to zero so that we can efficiently work on this using simd.
}

void Lattice::randomize_carriers()
{
	for (size_t i=0; i<num_carriers; ++i)
		randomize_carrier(i);
}

void Lattice::print_carriers()
{
	for (size_t i=0; i<num_carriers; ++i)
	{
		MetroCarrier &c = carriers[i];

		printf("Carrier %d (%d, %d, %d)\n", i, c.pos[0], c.pos[1], c.pos[2]);
	}
}

void Lattice::mc_step()
{
	update_pos();

	float trial_en = calc_energy();
	float delta_e = trial_en - last_energy;

	//printf("Trial energy: %.3f, last energy: %.3f\n", trial_en, last_energy);

	if (delta_e <= 0.0)
	{
		accept_update();
		last_energy = trial_en;
	}
	else if (delta_e > 100.0)
		reject_update();		//exp(-100/kT) is going to be neglibly tiny. Avoid overflowing the expf function
	else
	{
		float ratio = calc_prob(delta_e);
		float r = gsl_rng_uniform(rng_uniform);

		if (r < ratio)
		{
			last_energy = trial_en;
			accept_update();
		}
		else
			reject_update();
	}
}

void Lattice::init_samplers(int num_samples)
{
	for (std::vector<SamplerObject*>::iterator it = samplers.begin(); it != samplers.end(); ++it)
	{
		(*it)->set_numsamples(num_samples);
		(*it)->set_world(world);
	}
}

void Lattice::set_slop(float s)
{
	slop=s;
}

void Lattice::metropolis(int num_samples, int discard, int sample_every)
{
	time_t start;
	time_t end;

	//Get our world boundaries, setup updating parameters and randomize our starting conditions
	init_simulation();
	init_samplers(num_samples);
	randomize_carriers();

	//initialize our current state
	last_energy = calc_energy();

	printf("Starting Burn-in.\n");
	start = time(NULL);
	for (int i=0; i<discard; ++i)
		mc_step();

	end = time(NULL);
	printf("Burn in period: %d seconds\n", end-start);

	start = time(NULL);
	printf("Starting Sample Period.\n");
	for (int i=0; i<num_samples; ++i)
	{
		sample(i);

		for (int j=0; j<sample_every; ++j)
			mc_step();
	}
	end = time(NULL);
	printf("Sample period: %d seconds\n", end-start);
}

void Lattice::add_sampler(SamplerObject *sampler)
{
	samplers.push_back(sampler);
}

void Lattice::fill_sep(MetroCarrier &c1, MetroCarrier &c2, coord_t *out)
{
	for(size_t i=0; i<3; ++i)
		out[i] = c1.pos[i] - c2.pos[i];
}

float Lattice::world_energy()
{
	float total_en = 0.0;

	for (size_t i=0; i< num_carriers; ++i)
	{
		coord_t *pos = &sse_data.positions[4*i];
		LocalEnvironment &env = world->get_value(pos, kElectron);
		float field_en = (field*pos[2]);

		if (carriers[i].sign < 0.0)
			total_en += env.lumo + field_en;
		else
			total_en -= env.homo + field_en;	//holes want to live in higher energy sites
	}

	return total_en;
}

void Lattice::set_periodicity(bool x_periodic, bool y_periodic, bool z_periodic)
{
	periodic[0] = x_periodic;
	periodic[1] = y_periodic;
	periodic[2] = z_periodic;
}
