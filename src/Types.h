//Types.h

#ifndef __Types_h__
#define __Types_h__

#include <vector>
#include <stdlib.h>

#include "BasicTypes.h"
#include "Logger.h"

enum CarrierType
{
	kElectron = 0,
	kHole = 1,
	kNumCarriers
};

enum FieldType
{
	kIsotropic = 0,
	kHalfIsotropic
};

//Store the local energy levels
/* holes are assigned the homo energy
 * electrons are assigned the lumo energy
 */

//Force 8 byte alignment so we can easily write nrrd images in PreallocatedWorld
#pragma pack(push)
#pragma pack(8)
struct LocalEnvironment
{
	real_t homo;
	real_t lumo;
	
	int type;
	
	//LocalEnvironment() : homo(0.0), lumo(0.0), type(0) {};
};

#pragma pack(pop)

//A procedural generation routine to allow for lazily populating the 3D lattice of carrier
//positions

class World;

class WorldGenerator
{
	public:
	virtual LocalEnvironment generate(coord_t *pos, real_t lattice_constant, World *space) = 0;
	virtual void fill_neighbor_homos(coord_t *pos, real_t lattice_constant, World *space, real_t *out);
	virtual void fill_neighbor_lumos(coord_t *pos, real_t lattice_constant, World *space, real_t *out);
	
	virtual bool context_free() {return false;};
	virtual bool pos_independent() {return false;};
	
	virtual ~WorldGenerator() {};
};

class WorldGeneratorSpec
{
	public:
	virtual ~WorldGeneratorSpec() {};
	
	virtual WorldGenerator* create() = 0;
};

typedef struct
{
	coord_t hole_pos[3];
	coord_t elec_pos[3];
} CarrierPositions;

//Enforce numbering so that we can unambiguously convert to double on any machine
//I don't think this is necessary but maybe.
enum SimulationResult
{
	kEscaped = 0,
	kRecombined = 1,
	kTimeExpired = 2,
	kErrorOccurred = 3
};

//Certain result calculations should only be run when the simulation has eneded a certain
//way.  Allow the user to specify this.
enum ResultCondition
{
	kEscapedCondition = 1 << 0,
	kRecombinedCondition = 1 << 2,
	kTimeExpiredCondition = 1 << 4,
	kAlwaysCondition = kEscapedCondition | kRecombinedCondition | kTimeExpiredCondition
};

class KMCInterface;

typedef struct
{
	SimulationResult result;
	KMCInterface 	 *sim;
	
	std::vector<CarrierPositions> 	trace;
	std::vector<float> 				delays;
} SimulationData;

enum Axis
{
	kXAxis = 0,
	kYAxis,
	kZAxis
};

enum Direction
{
	kPositiveDirection = 0,
	kNegativeDirection
};

#endif