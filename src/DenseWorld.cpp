//DenseWorld.cpp

#include "DenseWorld.h"
#include <stdio.h>
#include <exception>
#include <string.h>

DenseWorld::DenseWorld(coord_t *new_start, coord_t *new_size) : space(NULL), alloc_map(NULL)
{
	//Save off the start and size
	for (size_t i=0; i<3; ++i)
	{
		start[i] = new_start[i];
		size[i] = new_size[i];
	}
	
	allocate();
}

DenseWorld::DenseWorld(coord_t *new_start, coord_t new_size) : space(NULL), alloc_map(NULL)
{
	//Save off the start and size
	for (size_t i=0; i<3; ++i)
	{
		start[i] = new_start[i];
		size[i] = new_size;
	}
	
	allocate();
}

DenseWorld::~DenseWorld()
{
	release();
}

void DenseWorld::reset()
{
	size_t n = (size_t)(size[0]*size[1]*size[2])/8 + 1;
	
	memset(alloc_map, 0, n);
}

void DenseWorld::release()
{
	if (space)
	{
		delete[] space;
		space = NULL;
		//printf("Releasing DenseWorld\n");
	}
	
	if (alloc_map)
	{
		delete[] alloc_map;
		alloc_map = NULL;
	}
}

void DenseWorld::allocate()
{
	size_t n = (size_t)(size[0]*size[1]*size[2]);
	
	release();
	
	//printf("Allocating %lu MB for DenseWorld\n", n*sizeof(LocalEnvironment)/1000000);
	
	space = new LocalEnvironment[n];
	alloc_map = new unsigned char[n/8+1];
	
	memset(alloc_map, 0, n/8+1);
}

coord_t DenseWorld::get_offset(coord_t *pos)
{
	coord_t rel_pos[3];
	
	for (size_t i=0;i<3; ++i)
		rel_pos[i] = pos[i] - start[i];
		
	return rel_pos[0]*size[0]*size[1] + rel_pos[1]*size[1] + rel_pos[2];
}

LocalEnvironment & DenseWorld::get_value(coord_t *pos, CarrierType type)
{
	coord_t offset = get_offset(pos);
	
	return space[offset];
}
	
bool DenseWorld::value_exists(coord_t *pos, CarrierType type)
{
	coord_t offset = get_offset(pos);
	
	unsigned char mask = 1 << (offset%8);
	coord_t bitmap_offset = offset/8;
	
	return alloc_map[bitmap_offset] & mask;
}

void DenseWorld::set_value(coord_t *pos, const LocalEnvironment &val, CarrierType type)
{
	coord_t offset = get_offset(pos);
	
	unsigned char mask = 1 << (offset%8);
	coord_t bitmap_offset = offset/8;
	
	space[offset] = val;
	space[offset] = val;
	
	alloc_map[bitmap_offset] |= mask;
}
	
bool DenseWorld::contains(coord_t *pos, CarrierType type)
{	
	for (size_t i=0;i<3; ++i)
	{
		coord_t rel = pos[i] - start[i];
		if (rel < 0 || rel >= size[i])
			return false;
	}
	
	return true;
}

void DenseWorld::append_xyz(FILE *f, XYZHelper &helper)
{	
	for (size_t i=0; i<size[0]; ++i)
	{
		for (size_t j=0; j<size[1]; ++j)
		{
			for (size_t k=0; k<size[2]; ++k)
			{
				coord_t pos[3];
				
				pos[0] = start[0] + i;
				pos[1] = start[1] + j;
				pos[2] = start[2] + k;
				
				if (value_exists(pos, kElectron))
					helper.append_location(f, pos, get_value(pos, kElectron));
			}
		}
	}
}

void DenseWorld::write_xyz(const std::string &name)
{
	//TODO: Implement this function
	throw std::exception();
}

size_t DenseWorld::lattice_size()
{
	//Return the number of set bits in the alloc map (Hamming Weight or popcount)  There are faster ways
	size_t n = (size_t)(size[0]*size[1]*size[2])/8+1;
	size_t result = 0;
	
	//Create a lookup table for the number of set bits per 8 bit value
	unsigned char lookup[256];
	for (size_t i=0; i<256;++i)
	{
		size_t val = 0;
		for (size_t j=0; j<8; ++j)
			val += (i>>j) & 0x01;
			
		lookup[i] = val;
	}
	
	for (size_t i=0; i<n; ++i)
		result += lookup[alloc_map[i]];
		
	return result;
}