//Template Implementation of ImageHelper

template<class T>
ImageHelper<T>::ImageHelper(T* indata, size_t istrides[3], size_t idims[3])
{
	for (size_t i=0; i<3; ++i)
	{
		stride[i] = istrides[i];
		dims[i] = idims[i];
	}
	
	initialize(indata);
}

template<class T>
ImageHelper<T>::ImageHelper(T* indata, size_t idims[3])
{
		for (size_t i=0; i<3; ++i)
	{
		stride[i] = 1;
		dims[i] = idims[i];
	}
	
	initialize(indata);
}

template<class T>
void ImageHelper<T>::initialize(T* indata)
{
	image = ImageType::New();
	typename ImageType::RegionType region;
	typename ImageType::IndexType start;
	typename ImageType::SizeType size;
	
	start[0] = start[1] = start[2] = 0;
	size[0] = dims[0];
	size[1] = dims[1];
	size[2] = dims[2];
	
	region.SetSize(size);
	region.SetIndex(start);
	image->SetRegions(region);
	image->Allocate();
	
	
	for (size_t i=0; i<dims[0]; ++i)
	{
		for (size_t j=0; j<dims[1]; ++j)
		{
			for (size_t k=0; k<dims[2]; ++k)
			{
				size_t i_off = i*dims[1]*dims[2]*stride[0] + j*dims[2]*stride[1] + k*stride[2];
				typename ImageType::IndexType o_index;
				
				o_index[0] = i;
				o_index[1] = j;
				o_index[2] = k;
				
				image->SetPixel(o_index, indata[i_off]);
			}
		}
	}			
}

template<class T>
void ImageHelper<T>::write_nrrd(const char *filename)
{
	typedef itk::ImageFileWriter<ImageType> WriterType;
	typename WriterType::Pointer writer = WriterType::New();
	
	itk::NrrdImageIO::Pointer io = itk::NrrdImageIO::New();
	
	io->SetFileType(itk::ImageIOBase::Binary);
	
	writer->SetInput(image);
	writer->SetFileName(filename);
	writer->SetImageIO(io);
	writer->Update();
}