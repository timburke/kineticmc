//ObjectFactory.hxx

#ifndef __ObjectFactory_h__
#define __ObjectFactory_h__

template <class T>
class ObjectFactory
{
	public:
	virtual T* create() = 0;

	virtual ~ObjectFactory() {};
};

template <class T, class ParamType, class BaseT=T>
class OneParameterObjectFactory : public ObjectFactory<BaseT>
{
	private:
	ParamType p1;
	
	public:
	OneParameterObjectFactory(const ParamType &p);
	
	virtual BaseT* create();
};

template <class T, class Param1Type, class Param2Type, class BaseT=T>
class TwoParameterObjectFactory : public ObjectFactory<BaseT>
{
	private:
	Param1Type p1;
	Param2Type p2;
	
	public:
	TwoParameterObjectFactory(const Param1Type &p1, const Param2Type &p2);
	
	virtual BaseT* create();
};

template <class T, class Param1Type, class Param2Type, class Param3Type, class BaseT=T>
class ThreeParameterObjectFactory : public ObjectFactory<BaseT>
{
	private:
	Param1Type p1;
	Param2Type p2;
	Param3Type p3;
	
	public:
	ThreeParameterObjectFactory(const Param1Type &p1, const Param2Type &p2, const Param3Type &p3);
	
	virtual BaseT* create();
};

#include "ObjectFactory.txx"

#endif