//Logger.cpp

/*
 * Logger
 * This object allows the configuration of the kineticmc logging system based on apache 
 * log4cxx.  The purpose of exposing the configuration api this way is to allow for 
 * easy configuration via python.
 *
 */

#include "Logger.h"
#include <log4cxx/propertyconfigurator.h>
#include <log4cxx/basicconfigurator.h>
#include <log4cxx/ndc.h>

Logger::Logger()
{
	l = log4cxx::Logger::getLogger("kineticmc.framework");
}
 
void Logger::configure_file(const std::string &path)
{
	log4cxx::PropertyConfigurator::configure(path.c_str());
	LOG_INFO(l, "Set logging file: " << path);
}

void Logger::configure_basic()
{
	log4cxx::BasicConfigurator::configure();
	LOG_INFO(l, "Configured logger with default values");
}

void Logger::trace(const std::string &msg)
{
	LOG_TRACE(l, msg);
}

void Logger::warn(const std::string &msg)
{
	LOG_WARN(l, msg);
}

void Logger::info(const std::string &msg)
{
	LOG_INFO(l, msg);
}

void Logger::debug(const std::string &msg)
{
	LOG_DEBUG(l, msg);
}

void Logger::error(const std::string &msg)
{
	LOG_ERROR(l, msg);
}

void Logger::fatal(const std::string &msg)
{
	LOG_FATAL(l, msg);
}

void Logger::push_ctx(const std::string &msg)
{
	log4cxx::NDC::push(msg);
}

void Logger::pop_ctx()
{
	log4cxx::NDC::pop();
}