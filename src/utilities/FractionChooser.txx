template<class T>
struct CompareFraction
{
	bool operator()(const std::pair<real_t, T> &left , const std::pair<real_t, T> &right)
	{
		return left.first < right.first;
	}
};

template<class T>
void FractionChooser<T>::add_option(real_t fraction, const T &value)
{
	fractions.push_back(std::pair<real_t, T>(fraction, value));
}

template<class T>
real_t FractionChooser<T>::total_fractions()
{
	real_t total = 0.0;
	
	for (size_t i=0; i<fractions.size(); ++i)
		total += fractions[i].first;
		
	return total;
}

template<class T>
void FractionChooser<T>::prepare()
{
	total = total_fractions();
	std::sort(fractions.begin(), fractions.end());
}

template<class T>
T& FractionChooser<T>::choose()
{
	
	real_t rand = drand48() * total;
	real_t sum = 0.0;
	for(size_t i=0; i<fractions.size(); ++i)
	{
		sum += fractions[i].first;
		if (rand <= sum)
			return fractions[i].second;
	}
	
	return fractions[fractions.size()-1].second;
}