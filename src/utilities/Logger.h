//Logger.h

#ifndef __LOGGER_H__
#define __LOGGER_H__

#include <string>
#include <log4cxx/logger.h>

#define LOG_TRACE(l, m) LOG4CXX_TRACE(l, m)
#define LOG_WARN(l, m) 	LOG4CXX_WARN(l, m)
#define LOG_INFO(l, m)	LOG4CXX_INFO(l, m)
#define LOG_DEBUG(l, m)	LOG4CXX_DEBUG(l, m)
#define LOG_ERROR(l, m)	LOG4CXX_ERROR(l, m)
#define LOG_FATAL(l, m) LOG4CXX_FATAL(l, m)

class Logger
{
	private:
	log4cxx::LoggerPtr l;
	
	public:
	Logger();
	
	void configure_file(const std::string &path);
	void configure_basic();
	
	//Logging functions for export to python
	void trace(const std::string &msg);
	void warn(const std::string &msg);
	void info(const std::string &msg);
	void debug(const std::string &msg);
	void error(const std::string &msg);
	void fatal(const std::string &msg);
	
	void push_ctx(const std::string &msg);
	void pop_ctx();
};

#endif
