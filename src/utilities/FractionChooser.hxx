//FractionChooser.hxx

#ifndef __FractionChooser_h__
#define __FractionChooser_h__

#include <vector>
#include <map>
#include <math.h>
#include <algorithm>

template <class T>
class FractionChooser
{
	private:
	std::vector<std::pair<real_t, T> > fractions;
	
	real_t total;
	
	real_t total_fractions();
	
	public:
	void add_option(real_t fraction, const T &value);
	
	void prepare();
	
	T& choose();
};

#include "FractionChooser.txx"

#endif