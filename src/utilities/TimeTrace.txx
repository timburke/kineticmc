
template<typename ValueType>
TimeTrace<ValueType>::TimeTrace(double bin_spacing, double length)
{
	reset(bin_spacing, length);
}

template<typename ValueType>
TimeTrace<ValueType>::TimeTrace()
{
	spacing = 0.0;
	max_length = 0.0;
	num_bins = 0;
}

template<typename ValueType>
void TimeTrace<ValueType>::log(double time, ValueType value)
{
	size_t bin = time/spacing;

	if (bin >= num_bins)
		return;

	trace[bin] += value;
	counts[bin] += 1;
}

template<typename ValueType>
void TimeTrace<ValueType>::get_values(ValueType *n, int n_dim)
{
	if (n_dim != num_bins)
		return;

	for (int i=0; i<n_dim; ++i)
		n[i] = trace[i];
}

template<typename ValueType>
void TimeTrace<ValueType>::get_counts(int *n, int n_dim)
{
	if (n_dim != num_bins)
		return;

	for (int i=0; i<n_dim; ++i)
		n[i] = counts[i];
}

template<typename ValueType>
int TimeTrace<ValueType>::length()
{
	return num_bins;
}

template<typename ValueType>
void TimeTrace<ValueType>::reset(double bin_spacing, double length)
{
	spacing = bin_spacing;
	max_length = length;

	num_bins = (size_t)(max_length/spacing);

	trace.resize(num_bins);
	counts.resize(num_bins);

	for (size_t i=0; i<num_bins; ++i)
	{
		trace[i] = ValueType();
		counts[i] = 0;
	}
}