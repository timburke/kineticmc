//ObjectFactory.txx

template <class T, class ParamType, class BaseT>
OneParameterObjectFactory<T, ParamType, BaseT>::OneParameterObjectFactory(const ParamType &p)
	: p1(p)
{

}

template <class T, class ParamType, class BaseT>
BaseT* OneParameterObjectFactory<T, ParamType, BaseT>::create()
{
	return reinterpret_cast<BaseT *>(new T(p1));
}

template <class T, class Param1Type, class Param2Type, class BaseT>
TwoParameterObjectFactory<T, Param1Type, Param2Type, BaseT>::TwoParameterObjectFactory(const Param1Type &p, const Param2Type &pp)
	: p1(p), p2(pp)
{

}

template <class T, class Param1Type, class Param2Type, class BaseT>
BaseT* TwoParameterObjectFactory<T, Param1Type, Param2Type, BaseT>::create()
{
	return reinterpret_cast<BaseT*>(new T(p1, p2));
}

template <class T, class Param1Type, class Param2Type, class Param3Type, class BaseT>
ThreeParameterObjectFactory<T, Param1Type, Param2Type, Param3Type, BaseT>::ThreeParameterObjectFactory(const Param1Type &p, const Param2Type &pp, const Param3Type &ppp)
	: p1(p), p2(pp), p3(ppp)
{

}

template <class T, class Param1Type, class Param2Type, class Param3Type, class BaseT>
BaseT* ThreeParameterObjectFactory<T, Param1Type, Param2Type, Param3Type, BaseT>::create()
{
	return reinterpret_cast<BaseT*>(new T(p1, p2, p3));
}