//VectorList.hxx

#ifndef __VectorList_h__
#define __VectorList_h__

#include <list>
#include <vector>
#include <stdint.h>
#include <unistd.h>

template<typename ValueType>
class VectorList
{
	public:
	typedef std::vector<ValueType> VectorType;
	typedef std::vector<VectorType *>  ListType;

	private:
	size_t 		vector_size;

	ListType	vector_list;
	VectorType	*curr_vector;

	void add_vector();
	void deallocate();

	public:
	VectorList(size_t preallocate_size);
	~VectorList();

	void 		append(const ValueType &value);
	ValueType 	get(size_t offset);
	size_t 		length();
	void		clear();

	VectorType*	unify();
};

#include "VectorList.txx"

#endif