
template<typename ValueType>
VectorList<ValueType>::VectorList(size_t preallocate_size) : vector_size(preallocate_size)
{
	add_vector();
}

template<typename ValueType>
VectorList<ValueType>::~VectorList()
{
	deallocate();
}

template<typename ValueType>
void VectorList<ValueType>::deallocate()
{
	for (typename ListType::iterator it=vector_list.begin(); it != vector_list.end(); ++it)
		delete *it;
}

template<typename ValueType>
void VectorList<ValueType>::clear()
{
	deallocate();
	add_vector();
}


template<typename ValueType>
void VectorList<ValueType>::append(const ValueType &value)
{
	curr_vector->push_back(value);
	if (curr_vector->size() == vector_size)
		add_vector();
}

template<typename ValueType>
std::vector<ValueType>* VectorList<ValueType>::unify()
{
	size_t l = length();
	size_t out_i = 0;

	VectorType *vec = new VectorType(l);


	for (size_t i=0; i<vector_list.size(); ++i)
	{
		VectorType *curr = vector_list[i];
		size_t curr_size = curr->size();

		for (size_t j=0; j<curr_size; ++j)
			vec->operator[](out_i++) = curr->operator[](j);
	}

	return vec;
}

template<typename ValueType>
ValueType VectorList<ValueType>::get(size_t offset)
{
	size_t vector = offset / vector_size;
	size_t v_offset = offset % vector_size;

	if (vector >= vector_list.size())
		return ValueType();

	return vector_list[vector]->at(v_offset);
}

template<typename ValueType>
size_t VectorList<ValueType>::length()
{
	return (vector_size*(vector_list.size()-1) + curr_vector->size());
}

template<typename ValueType>
void VectorList<ValueType>::add_vector()
{
	VectorType *new_vector = new VectorType();
	new_vector->reserve(vector_size);

	vector_list.push_back(new_vector);

	curr_vector = new_vector;
}