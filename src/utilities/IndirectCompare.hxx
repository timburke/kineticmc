//IndirectCompare.hxx

#ifndef __IndirectCompare_hxx
#define __IndirectCompare_hxx

#include <vector>
#include <stdio.h>

template<class IndexType, class ValueType>
class IndirectComparator
{
	private:
	const std::vector<ValueType> &values;

	public:
	IndirectComparator(const std::vector<ValueType> &vals) : values(vals)
	{

	}

	bool operator()(const IndexType &lhs, const IndexType &rhs)
	{
		uint64_t mask = (1LL << 63);

		IndexType vl, vr;

		if ((lhs & mask) != 0)
			vl = lhs & ~mask;
		else
			vl = values[lhs];

		if ((rhs & mask) != 0)
			vr = rhs & ~mask;
		else
			vr = values[rhs];

		return vl < vr;
	}
};

template<class IndexType, class ValueType>
class IndirectComparator_NoMask
{
	private:
	const std::vector<ValueType> &values;

	public:
	IndirectComparator_NoMask(const std::vector<ValueType> &vals) : values(vals)
	{

	}

	bool operator()(const IndexType &lhs, const IndexType &rhs)
	{
		return values[lhs] < values[rhs];
	}
};

#endif