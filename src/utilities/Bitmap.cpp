//Bitmap.cpp

#include "Bitmap.h"
#include <stdio.h>
#include <string>

Bitmap::Bitmap(): data(NULL), size(0)
{
}

Bitmap::~Bitmap()
{
	if (data)
		delete[] data;
}

void Bitmap::allocate()
{
	if (data)
		delete[] data;

	data = new unsigned int[size];
}


void Bitmap::set(size_t offset)
{
	size_t entry = offset / sizeof(unsigned int);
	size_t bit = offset % sizeof(unsigned int);

	if (entry >= size)
	{
		printf("Invalid offset, too big: %lu", offset);
		return;
	}

	data[entry] |= 1 << bit;
}

void Bitmap::clear(size_t offset)
{
	size_t entry = offset / sizeof(unsigned int);
	size_t bit = offset % sizeof(unsigned int);

	if (entry >= size)
	{
		printf("Invalid offset, too big: %lu", offset);
		return;
	}

	data[entry] &= ~(1 << bit);
}

bool Bitmap::is_set(size_t offset)
{
	size_t entry = offset / sizeof(unsigned int);
	size_t bit = offset % sizeof(unsigned int);

	if (entry >= size)
	{
		printf("Invalid offset, too big: %lu", offset);
		throw std::string("Error.");
		return false;
	}

	return data[entry] & (1 << bit);
}

void Bitmap::resize(size_t size)
{
	this->size = size/sizeof(unsigned int);

	allocate();
}