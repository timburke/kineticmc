/*
 * Bitmap.h
 */ 

#ifndef __Bitmap_h__
#define __Bitmap_h__

#include <stdint.h>
#include <stdlib.h>

class Bitmap
{
	private:
	unsigned int *data;
	size_t size;

	void allocate();

	public:
	Bitmap();
	~Bitmap();

	void set(size_t offset);
	void clear(size_t offset);
	bool is_set(size_t offset);

	void resize(size_t size);
};

#endif