//TimeTrace.hxx

#ifndef __TimeTrace_hxx__
#define __TimeTrace_hxx__

#include <vector>
#include <unistd.h>

template <typename ValueType>
class TimeTrace
{
	private:
	std::vector<ValueType>		trace;
	std::vector<uint32_t>		counts;

	double spacing;
	double max_length;
	size_t num_bins;

	public:
	TimeTrace(double bin_spacing, double max_length);
	TimeTrace();

	void reset(double bin_spacing, double max_length);

	void log(double time, ValueType value);
	
	void get_values(ValueType *n, int n_dim);
	void get_counts(int *n, int n_dim);
	int length();
};

#include "TimeTrace.txx"

#endif