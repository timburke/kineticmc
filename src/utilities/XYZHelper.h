//XYZHelper.h

#ifndef __XYZHelper_h__
#define __XYZHelper_h__

#include "Types.h"
#include <string>
#include <stdio.h>
#include <map>

class XYZHelper
{
	private:
	std::map<unsigned long, std::string> sites;
	size_t last_id;
	
	std::string get_name(double homod);
	
	public:
	XYZHelper() : last_id(0) {};
	
	void write_trajectory(const std::string &file, const std::vector<CarrierPositions> &trace, double lattice_const, bool animation);
	
	void append_location(FILE *f, coord_t *pos, LocalEnvironment &env);
};

#endif