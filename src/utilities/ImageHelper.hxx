//ImageHelper.h

#ifndef __ImageHelper_h__
#define __ImageHelper_h__

#include <itkImage.h>
#include <itkExceptionObject.h>
#include <itkImageFileWriter.h>
#include <itkNrrdImageIO.h>

template <class T>
class ImageHelper
{
	typedef itk::Image<T, 3> ImageType;
	private:
	typename ImageType::Pointer image;
		
	size_t stride[3];
	size_t dims[3];
	
	void initialize(T* indata);
	
	public:
	explicit ImageHelper(T* indata, size_t strides[3], size_t dims[3]);
	ImageHelper(T* indata, size_t idims[3]);
	
	void write_nrrd(const char *filename);
};

//Include Implementation
#include "ImageHelper.txx"

#endif