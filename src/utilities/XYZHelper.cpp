//XYZHelper.cpp

#include "XYZHelper.h"
#include <stdio.h>
#include <exception>
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

void XYZHelper::write_trajectory(const std::string &filename, const std::vector<CarrierPositions> &trace, double lattice_const, bool animation)
{
	FILE *f = fopen(filename.c_str(), "w");
	
	if (!f)
	{
		printf("Could not open file %s\n", filename.c_str());
		return;
	}
	
	if (animation == false)
	{
		fprintf(f, "%lu\n", trace.size());
		fprintf(f, "Electron and hole trajectories\n");
	}
	
	for (size_t i=0; i<trace.size(); ++i)
	{
		if (animation)
		{
			fprintf(f, "2\n");
			fprintf(f, "Frame %lu\n", i+1);
		}
		
		const CarrierPositions &p = trace[i];
		
		fprintf(f, "E %.6f %.6f %.6f\n", p.elec_pos[0]*lattice_const, p.elec_pos[1]*lattice_const, p.elec_pos[2]*lattice_const);
		fprintf(f, "H %.6f %.6f %.6f\n", p.hole_pos[0]*lattice_const, p.hole_pos[1]*lattice_const, p.hole_pos[2]*lattice_const);
	}
	
	fclose(f);
}

std::string XYZHelper::get_name(double homod)
{
	unsigned long *homo = (unsigned long *)&homod;
				
	if (sites.find(*homo) == sites.end())
	{
		char id[2];
		id[0] = 'A' + last_id++;
		id[1] = '\0';
		sites[*homo] = string(id);
		
		printf("Adding site to map (HOMO=%f) id=%s\n", homod, id);
	}
	
	return sites[*homo];
}

void XYZHelper::append_location(FILE *f, coord_t *pos, LocalEnvironment &env)
{				
	fprintf(f, "%s %.6f %.6f %.6f\n", get_name(env.homo).c_str(), (double)pos[0], (double)pos[1], (double)pos[2]);
}

/*void XYZHelper::read_trajectory(const std::string &filename, std::vector<CarrierPositions &trace, double lattice_const)
{
	FILE *f = fopen(filename.c_str(), "r");
	
	if (!f)
	{
		printf("Could not open file %s\n", filename.c_str());
		throw std::exception();
	}
	
*/