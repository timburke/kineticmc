//DenseWorld.h

#ifndef __DenseWorld_h__
#define __DenseWorld_h__

#include "World.h"
#include "XYZHelper.h"

class DenseWorld : public World
{
	private:
	LocalEnvironment *space;
	unsigned char	 *alloc_map;
	
	coord_t size[3];
	coord_t	start[3];
	
	void allocate();
	void release();
	coord_t get_offset(coord_t *pos);
	
	public:
	DenseWorld(coord_t *start, coord_t *size); //start should be the index of the bottom-left-back corner (smallest coord in the world)
	DenseWorld(coord_t *new_start, coord_t new_size);
	virtual ~DenseWorld();
	
	//World Interface
	virtual LocalEnvironment &	get_value(coord_t *pos, CarrierType type); //pos must exist or this will throw an exception
	virtual bool 				value_exists(coord_t *pos, CarrierType type);
	virtual void				set_value(coord_t *pos, const LocalEnvironment &val, CarrierType type);
	
	virtual bool 				contains(coord_t *pos, CarrierType type);
	virtual void				reset();
	
	virtual size_t				lattice_size();
	virtual void 				write_xyz(const std::string &name);
	
	void 						append_xyz(FILE *f, XYZHelper &helper); 

};

#endif