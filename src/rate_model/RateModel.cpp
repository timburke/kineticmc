#include "RateModel.h"

RateModel::RateModel()
{
	temp = 300.0;

	recalc_temp();
}

void RateModel::recalc_temp()
{
	kT = temp*8.6173324e-5;
	inv_kT = 1.0/kT;
}

void RateModel::set_temp(markov_real t)
{
	temp = t;

	recalc_temp();
}