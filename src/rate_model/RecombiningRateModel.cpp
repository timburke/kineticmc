
#include "RecombiningRateModel.h"

RecombiningRateModel::RecombiningRateModel()
{
	recomb_rate = 1.0;
}

void RecombiningRateModel::set_lifetime(markov_real lifetime)
{
	recomb_rate = 1.0 / lifetime;
}

void RecombiningRateModel::set_recomb(markov_real recomb)
{
	recomb_rate = recomb;
}

markov_real RecombiningRateModel::calculate_rate(MarkovState src, MarkovState dst, markov_real src_en, markov_real dst_en)
{
	if (dst.carriers_overlap())
		return recomb_rate;

	return calc_nonrecomb(src, dst, src_en, dst_en);
}
