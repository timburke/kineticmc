
#ifndef __MillerAbrahamsRateModel_h__
#define __MillerAbrahamsRateModel_h__

#include "RecombiningRateModel.h"

class MillerAbrahamsRateModel : public RecombiningRateModel
{
	private:
	markov_real elec_pre;
	markov_real hole_pre;

	protected:
	virtual markov_real calc_nonrecomb(MarkovState src, MarkovState dst, markov_real src_en, markov_real dst_en);

	public:
	void set_prefactor(markov_real pre);
	void set_prefactors(markov_real elec, markov_real hole);
};

#endif