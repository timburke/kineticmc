#ifndef __RateModel_h__
#define __RateModel_h__

#include "MarkovTypes.h"
#include "MarkovState.h"

class RateModel
{
	private:
	void recalc_temp();
	
	protected:
	markov_real temp;
	markov_real kT;
	markov_real inv_kT;

	public:
	RateModel();
	virtual ~RateModel() {};

	virtual markov_real calculate_rate(MarkovState src, MarkovState dst, markov_real src_en, markov_real dst_en) = 0;

	void set_temp(markov_real t);
};

#endif