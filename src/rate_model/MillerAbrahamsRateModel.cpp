#include "MillerAbrahamsRateModel.h"
#include <cmath>

markov_real MillerAbrahamsRateModel::calc_nonrecomb(MarkovState src, MarkovState dst, markov_real src_en, markov_real dst_en)
{
	markov_real prefactor = hole_pre;
	if (src.which_moved(dst) == kElectron)
		prefactor = elec_pre;

	if (dst_en <= src_en)
		return prefactor;

	return prefactor*std::exp((src_en-dst_en)*inv_kT);
}

void MillerAbrahamsRateModel::set_prefactor(markov_real pre)
{
	hole_pre = pre;
	elec_pre = pre;
}

void MillerAbrahamsRateModel::set_prefactors(markov_real elec, markov_real hole)
{
	elec_pre = elec;
	hole_pre = hole;
}