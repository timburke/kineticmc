
#ifndef __RecombiningRateModel_h__
#define __RecombiningRateModel_h__

#include "RateModel.h"

class RecombiningRateModel : public RateModel
{
	private:
	markov_real recomb_rate;

	protected:
	virtual markov_real calc_nonrecomb(MarkovState src, MarkovState dst, markov_real src_en, markov_real dst_en) = 0;

	public:
	RecombiningRateModel();

	void set_lifetime(markov_real lifetime);
	void set_recomb(markov_real recomb);

	virtual markov_real calculate_rate(MarkovState src, MarkovState dst, markov_real src_en, markov_real dst_en);
};

#endif