//PeriodicCoulombModel.cpp

#include "PeriodicCoulombModel.h"
#include <math.h>
#include <stdio.h>

markov_real PeriodicCoulombModel::calc_energy(coord_t *elec, coord_t *hole)
{
	markov_real coeff = -1.0/(4.0*3.1415926535*8.85418782e-12*this->dielectric)*1.60217657e-19;	//eV*m
	markov_real dist = sqrt(lattice*lattice*nearest_replica_dist2(elec, hole))*1e-10;

	return coeff/dist;
}
