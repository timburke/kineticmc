#include "EnergyLevelModel.h"

markov_real EnergyLevelModel::calc_energy(coord_t *elec, coord_t *hole)
{
	markov_real energy = 0.0;

	energy = world->get_value(elec, kElectron).lumo - world->get_value(hole,kHole).homo;

	return energy;
}