#ifndef __EnergyLevelModel_h__
#define __EnergyLevelModel_h__

#include "InteractionModel.h"

class EnergyLevelModel : public InteractionModel
{
	public:
	virtual markov_real calc_energy(coord_t *elec, coord_t *hole);	
};

#endif