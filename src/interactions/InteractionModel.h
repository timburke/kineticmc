//InteractionModel.h

#ifndef __InteractioNModel_h__
#define __InteractioNModel_h__

#include "Types.h"
#include "MarkovTypes.h"
#include "ConfiguredObject.h"
#include <vector>

class InteractionModel : public ConfiguredObject
{	
	public:
	virtual markov_real calc_energy(coord_t *elec, coord_t *hole) = 0;
};

#endif