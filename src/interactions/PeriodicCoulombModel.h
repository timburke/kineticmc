//PeriodicCoulombModel.h

#ifndef __PeriodicCoulombModel_h__
#define __PeriodicCoulombModel_h__

#include "InteractionModel.h"

class PeriodicCoulombModel : public InteractionModel
{
	public:
	virtual markov_real calc_energy(coord_t *elec, coord_t *hole);
};

#endif