//VirtualWorld.h

/* 
 * VirtualWorld
 *
 * An implementation of the World interface that doesn't store or allocate anything.
 * This is useful when we have a WorldGenerator that can quickly return energy levels
 * deterministically just based on the position passed in, so we don't need to save 
 * anything.
 */
 
#ifndef __VirtualWorld_h__
#define __VirtualWorld_h__
 
#include "World.h"
 
class VirtualWorld : public World
{
	public:
 	virtual LocalEnvironment &	get_value(coord_t *pos, CarrierType type);
	virtual bool 				value_exists(coord_t *pos, CarrierType type);
	virtual void				set_value(coord_t *pos, const LocalEnvironment &val, CarrierType type);
	
	virtual bool 				contains(coord_t *pos, CarrierType type);
	
	virtual void				reset();; //Clear this world
	
	virtual void 				write_xyz(const std::string &name); //write allocated space to xyz file.
	
	virtual bool 				context_free();
	
	virtual size_t				lattice_size(); //return the number of allocated lattice positions
};

#endif