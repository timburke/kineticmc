#include "PreallocatedWorld.h"
#include <stddef.h>
#include "ImageHelper.hxx"

PreallocatedWorld::PreallocatedWorld(size_t xdim, size_t ydim, size_t zdim) 
{
	dims[0] = xdim;
	dims[1] = ydim;
	dims[2] = zdim;
	
	allocate();
}

PreallocatedWorld::PreallocatedWorld(size_t ndims[3])
{
	for (size_t i=0; i<3; ++i)
		dims[i] = ndims[i];
	
	allocate();
}

void PreallocatedWorld::set_smallest(const std::vector<int> &s)
{
	for (size_t i=0; i<3; ++i)
		smallest[i] = s[i];
}

void PreallocatedWorld::fill_dims(coord_t *out)
{
	for(size_t i=0; i<3;++i)
		out[i] = dims[i];
}

void PreallocatedWorld::fill_smallest(coord_t *out)
{
	for(size_t i=0; i<3;++i)
		out[i] = smallest[i];
}

void PreallocatedWorld::allocate()
{
	size_t len = dims[0]*dims[1]*dims[2];
	
	for (size_t i=0; i<3; ++i)
	{
		smallest[i] = - (dims[i]/2);
	}

	arr = new LocalEnvironment[len];
}

PreallocatedWorld::~PreallocatedWorld()
{
	if (arr)
		delete[] arr;
}

coord_t PreallocatedWorld::get_offset(coord_t *pos)
{
	coord_t rpos[3];
	
	for (size_t i=0; i<3; ++i)
	{
		rpos[i] = (pos[i] - smallest[i]);

		/*if (rpos[i] < 0)
			rpos[i] = -1*((-1*rpos[i]) % dims[i]) + dims[i];
		else
			rpos[i] %= dims[i];*/

		while(rpos[i] < 0)
			rpos[i] += dims[i];

		while (rpos[i] >= dims[i])
			rpos[i] -= dims[i];

	}
	
	return rpos[0]*dims[1]*dims[2] + rpos[1]*dims[2] + rpos[2];
}

LocalEnvironment & PreallocatedWorld::get_value(coord_t *pos, CarrierType)
{
	return arr[get_offset(pos)];
}
	
bool PreallocatedWorld::value_exists(coord_t *pos, CarrierType type)
{
	return true;
}

void PreallocatedWorld::set_value(coord_t *pos, const LocalEnvironment &val, CarrierType type)
{
	arr[get_offset(pos)] = val;
}

bool PreallocatedWorld::contains(coord_t *pos, CarrierType type)
{
	return true;
}

void PreallocatedWorld::reset()
{
	//FIXME: Do we need to reset anything?
}

void PreallocatedWorld::write_xyz(const std::string &name)
{
	//FIXME: write an XYZ file here
}

size_t PreallocatedWorld::lattice_size()
{
	return dims[0]*dims[1]*dims[2];
}

int PreallocatedWorld::np_size()
{
	return dims[0]*dims[1]*dims[2];
}

void PreallocatedWorld::get_dimensions(coord_t *out)
{
	for (size_t i=0; i<3; ++i)
		out[i] = dims[i];
}

void PreallocatedWorld::save_nrrd(const std::string &name, ImageType chooser)
{
	size_t strides[3];
	
	//FIXME: This uses pointer offset magic and may well break on other architectures
	if (chooser == kMoleculeImage)
	{
		size_t offset = offsetof(LocalEnvironment, type) / sizeof(int);
		strides[0] = strides[1] = strides[2] = sizeof(LocalEnvironment)/sizeof(int);
		
		ImageHelper<int> helper(((int*)arr)+offset, strides, dims);
		
		helper.write_nrrd(name.c_str());
	}
	else
	{
		//We're saving homo/lumo
		size_t offset;
		strides[0] = strides[1] = strides[2] = sizeof(LocalEnvironment)/sizeof(real_t);
		
		if (chooser == kHOMOImage)
			offset = offsetof(LocalEnvironment, homo) / sizeof(real_t);
		else //kLUMOImage
			offset = offsetof(LocalEnvironment, lumo) / sizeof(real_t);
		
		ImageHelper<real_t> helper(((real_t*)arr)+offset, strides, dims);
		
		helper.write_nrrd(name.c_str());
	}
}

void PreallocatedWorld::find_surfaces(std::vector<size_t> &surfaces)
{
	surfaces.clear();
	surfaces.reserve(dims[0]*dims[1]*dims[2]);
	
	for (size_t i=0; i<dims[0]*dims[1]*dims[2]; ++i)
	{
		
		coord_t pos[3];
		position_from_offset(i, pos);
		
		size_t count = count_neighbors(pos);
		
		if (count != 6)
			surfaces.push_back(i);
	}
}

void PreallocatedWorld::position_from_offset(size_t offset, coord_t *pos)
{
	size_t temp = offset;
	
	pos[0] = temp / (dims[1]*dims[2]);
	temp %= dims[1]*dims[2];
	
	pos[1] = temp/dims[2];
	pos[2] = temp % dims[2];
}

void PreallocatedWorld::fill_electron_energies(coord_t *pos, real_t *out)
{
	for (size_t i=0; i<6; ++i)
	{
		pos[i/2] += (1 - (i%2)*2);
		
		out[i] = get_value(pos, kElectron).lumo;
		
		pos[i/2] -= (1 - (i%2)*2);
	}
}

void PreallocatedWorld::fill_hole_energies(coord_t *pos, real_t *out)
{
	for (size_t i=0; i<6; ++i)
	{
		pos[i/2] += (1 - (i%2)*2);
		
		out[i] = get_value(pos, kElectron).homo;
		
		pos[i/2] -= (1 - (i%2)*2);
	}
}

void PreallocatedWorld::np_energies(double *n, int n_dim)
{
	int needed = dims[0]*dims[1]*dims[2]*2;

	if (n_dim != needed)
		return;

	for (size_t i=0; i<dims[0]; ++i)
	{
		for (size_t j=0; j<dims[1]; ++j)
		{
			for (size_t k=0; k<dims[2]; ++k)
			{
				size_t index = (i*dims[1]*dims[2] + j*dims[2] + k);
 
				n[2*index + 0] = arr[index].homo;
				n[2*index + 1] = arr[index].lumo;
			}
		}
	}
}

void PreallocatedWorld::np_tags(int *n, int n_dim)
{
	int needed = dims[0]*dims[1]*dims[2];

	if (n_dim != needed)
		return;

	for (size_t i=0; i<dims[0]; ++i)
	{
		for (size_t j=0; j<dims[1]; ++j)
		{
			for (size_t k=0; k<dims[2]; ++k)
			{
				size_t index = (i*dims[1]*dims[2] + j*dims[2] + k);
 
				n[index] = arr[index].type;
			}
		}
	}
}
