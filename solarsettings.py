#solarsettings.py
#A small module for storing settings in a User's home directory and reading them back
#later

import os.path
import shelve

class SolarSettings:
	dirname = ".solar"
	def __init__(self):
		"""
		Load settings from the appropriate user directory, creating an empty config
		if one does not exist
		"""
		
		home = os.path.expanduser("~")
		
		self.settingsdir = os.path.join(home, self.dirname)
		self.settingsfile = os.path.join(self.settingsdir, "settings")
		
		if not os.path.isdir(self.settingsdir) or not os.path.isfile(self.settingsfile):
			self._initialize()
		
		self.shelf = shelve.open(self.settingsfile)
			
	def _initialize(self):
		"""
		Create an empty settings file and directory for a previously unused computer
		"""
		
		os.mkdir(self.settingsdir)
		
	def get(self, key):
		return self.shelf[key]
	
	def set(self, key, val):
		self.shelf[key] = val
		
	def exists(self, key):
		return key in self.shelf
	
	#Support the with statement
	def __enter__(self):
		return self.shelf
	
	def __exit__( self, type, value, tb ):
		self.shelf.close()