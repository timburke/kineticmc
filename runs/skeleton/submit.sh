#!/usr/bin/bash
cd '/Users/timburke/Documents/Graduate School/Research/Monte Carlo/kineticmc/runs/FOLDERNAME'
python '/Users/timburke/Documents/Graduate School/Research/Monte Carlo/kineticmc/scripts/run_cluster.py' -f config.json -f log.conf -f userconfig.py -n FOLDERNAME --mpi --slots SLOTS driver.py -- --config config.json