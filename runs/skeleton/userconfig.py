import sets
import kineticmc as mc

def initialize_parameters(combined_vars):
	"""
	Return a kineticmc.SimulationParameters object given the variables defined in combined_vars
	dictionary
	"""
		
	log = mc.Logger()
	
	params = mc.SimulationParameters()

	needed = list_all_params(params)	
	filled = copy_identical_values(params, combined_vars)
	
	needed = needed.difference(filled)
	
	## CUSTOM INITIALIZATION CODE
	
	#Be sure to remove the values you initialize
	#from needed so the code in driver.py doesn't print 
	#unnecessary warnings

	raise ValueError("You must add code to initialize_parameters.")
	
	## END CUSTOM INITIALIZATION CODE
	
	return (params, needed)



##
## Utility functions, you should not have to modify these
##
def copy_identical_values(params, vars):
	"""
	Copy parameters that have the same name from vars to params
	"""
	
	global processor
	
	filled = sets.Set()

	for key,item in vars.items():
		if hasattr(params, key):
			if key in processor:
				setattr(params, key, processor[key](item))
			else:
				setattr(params, key, item)
	
			filled.add(key)
	
	return filled
	
def list_all_params(params):
	"""
	List all the parameters that we need to fill in for a simulation
	so we can make sure we don't miss any.
	"""
	
	vals = dir(params)
	
	#Certain values are prefilled and don't need to be changed, also this is not a param
	ignored = sets.Set(('this', 'num_blocks', 'block_size'))
	
	#The following parameters are optional
	ignored.add('initial_hole')
	ignored.add('initial_elec')
	ignored.add('initial_type')
	
	attribs = sets.Set(filter(lambda x: not x.startswith('__'), vals))

	return attribs.difference(ignored)
	
###Default parameter processors for those parameters that cannot be passed directly
def process_fieldtype(type):
	if type == "kIsotropic":
		return mc.kIsotropic
	elif type == 'kHalfIsotropic':
		return mc.kHalfIsotropic
	
	mc.Logger().error("Invalid field type given: %s" % str(type))

	raise ValueError("Invalid field type given")

def process_initial_state(type):
	if type == "FixedSeparation":
		return mc.kSpecificDistance
	elif type == "FixedPositions":
		return mc.kSpecificPositions
		
	mc.Logger().error("Invalid initial state given; %s" % str(type))
	
	raise ValueError("Invalid initial state type.")

#Parameter Processing Map
processor = {}
processor["field_type"] = process_fieldtype
processor["initial_type"] = process_initial_state
