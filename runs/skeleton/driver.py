#skeleton file driving a batch simulation iterating over many variable combinations
#you should not have to change this file, but rather change submit.sh

import kineticmc as mc
from subprocess import call
import numpy as np
import os
import sys
import time

import userconfig

#Logging is initialized by parallel_container
log = mc.Logger()

c = mc.SimulationController()

def exception_handler(type, value, tb):
    log.error("Uncaught exception: {0}".format(str(value)))
    
sys.excepthook = exception_handler

#Entry point for parallel_container
def start_job(env):
	#Start main program
	global log
	
	#Initialize default values if we're not MPI
	suffix = ""
	threads = 1
	nodes = 1
	me = 0
	
	log.debug("Using kineticmc version %s" % mc.version)
	
	if env["multinode"]:
		threads = 1
		nodes = env["num_nodes"]
		me = env["node_number"]
		suffix = env["suffix"]
		
		log.debug("Computing with %d nodes, I'm %d" % (nodes, me))
		
	else:
		threads = env["cores"]
		
		if threads < 1:
			threads = 1
			
	
		log.debug("One node assigned, using %d threads" % threads)
	
	#load the parameters for this run
	config = env["config"]
	
	fixed = config.get_fixed_params()
	space = config.parameter_space()
	itervar = config.get_iteration_variable()
	
	#Add the result types that we want to track
	results = config.get_results()
	for r in results:
		c.track_result(r)
	
	#determine how many trials we need to run
	trials = config.trials()
	if nodes != 1:
		#if we're using multiple nodes, reduce our trials
		load = trials / nodes
		if me == nodes-1:
			#last node takes up rounding slack
			trials = trials - (nodes-1)*load
		else:
			trials = load
	
	for v in space:
		var_params = config.make_parameter_dictionary(v)
		
		simulate_configuration( fixed, var_params, itervar,
								trials=trials, 
								threads=threads, 
								config=config,
								suffix=suffix)

def simulate_configuration(fixed, variable, iteration, trials, threads, suffix, config):
	global c, log
	
	combined_vars = dict(fixed.items() + variable.items())
	
	#Call user supplied function to initialize the parameters according to the config file
	(params, needed) = userconfig.initialize_parameters(combined_vars)
	params.trials = trials
	
	#iteration is a (name, [values]) tuple	
	itername = iteration[0]
	itervals = iteration[1]
	
	#Make sure all of the values were appropriately filled in (we fill in the iteration variable so ignore that
	needed.discard(itername)
	for elem in needed:
		log.error('Simulation Parameter %s was not set in initialize_parameters' % elem)
	
	num_cols = config.get_total_result_width()
	#Build the result array
	res = np.ndarray([len(itervals)*num_cols, trials])
	
	for i in xrange(0, len(itervals)):
		setattr(params, itername, itervals[i]) #Set the iteration parameter
		
		#Run the simulation for this configuration
		c.initialize(params)
		c.run(threads)
		
		arr = c.get_results()		
		
		#Copy the results into our combined output array
		for j in xrange(0, num_cols):
			res[num_cols*i+j,:] = arr[:,j]
		
	#Save the resulting file
	format = config.get_format()
	path = format.format(**variable) + suffix + '.npy'
	log.debug("Data will be saved in file %s" % str(path))
	
	#Check if we are supposed to postprocess the results
	post = config.get_postprocessing()
	
	if post is not None:
		log.trace("Postprocessing")
		if post == "sum":
			res = np.sum(res, axis=-1)
		elif post == "average":
			np.mean(res, axis=-1)
		else:
			log.error("Unknown postprocessing step requested: %s. Doing nothing." % post)
	
	log.trace("Saving")
	np.save(path, res)
	log.trace("Compressing")
	call(['gzip', path])
	call(['rm', '-rf', path]) #get rid of original file if it's still there