#!/usr/bin/python

from distutils.core import setup,Extension
import numpy as np
import os
import importlib

if "kmc_computer" in os.environ:
  mod = os.environ['kmc_computer']
  config = importlib.import_module('config.settings_%s' % mod)

def build_itk_libs():
  ver = config.itk_ver
  base = ['ITKIONRRD', 'ITKCommon', 'itkvnl', 'itkvnl_algo', 'ITKIOImageBase']  

  return map(lambda x: x + '-' + ver, base)

#macosx_link = '-headerpad_max_install_names'

libs = []

extra_includes = []
extra_link_args = []
#If we are on the cluster, we need to include the log4cxx user install dir
if "cluster" in os.environ:
    print "Compiling for cluster"
    lib_dirs = ['/afs/ir.stanford.edu/users/t/i/timburke/support/kineticmath/lib', '/afs/ir.stanford.edu/users/t/i/timburke/support/lib', '/afs/ir.stanford.edu/users/t/i/timburke/support/lib/vtk-5.10']
    extra_includes = ['/afs/ir.stanford.edu/users/t/i/timburke/support/include', '/afs/ir.stanford.edu/users/t/i/timburke/support/include/vtk-5.10', '/afs/ir.stanford.edu/users/t/i/timburke/support/include/ITK-4.3']
else:
    print "Compiling locally"
    lib_dirs = [config.itk_libdir, '/opt/kmc/lib']
    extra_includes += [config.itk_incdir, config.vtk_dir,'/usr/local/include', os.path.join(config.stxxl_dir, 'include')]
    extra_link_args = [os.path.join(config.stxxl_dir, 'lib','libstxxl.a')]

libs += build_itk_libs()

setup(  name='kineticmc',
        version='1.0',
        description='Python interface to kinetic monte carlo solver for modeling geminate charge splitting.',
        author='Tim Burke',
        author_email='timburke@stanford.edu',
        ext_modules=[Extension('_kineticmc', ['src/metropolis/Lattice.cpp', 'src/math/SampledFunction.cpp', 'src/metropolis/Sampler.cpp', 'src/metropolis/SemiperiodicLattice.cpp', 'src/kinetic-mc.i', 'src/control-sim.cpp', 'src/SparseWorld.cpp', 'src/DenseWorld.cpp', 'src/utilities/XYZHelper.cpp', 'src/mobility/MobilityModel.cpp', 'src/mobility/IsotropicMobilityModel.cpp', 'src/math/basic_math.cpp', 'src/mobility/DirectionalMobilityModel.cpp', 'src/utilities/Logger.cpp', 'src/VirtualWorld.cpp',
                                              'src/PreallocatedWorld.cpp', 'src/getters/energy/CachedEnergy.cpp', 'src/getters/energy/ContextFreeEnergy.cpp', 'src/getters/energy/PreallocatedEnergy.cpp', 'src/WorldGenerator.cpp', 'src/mobility/PrecomputedMobilityModel.cpp', 'src/worlds/WorldCreator.cpp', 'src/World.cpp', 'src/math/NormalRNG.cpp',
                                              'src/markov/MarkovChain.cpp', 'src/markov/MarkovState.cpp', 'src/markov/BigMarkovStateMapper.cpp',
                                              'src/markov/pruning/SingleDestinationRule.cpp', 'src/markov/pruning/MaximumHopsPruningRule.cpp',
                                              'src/markov/pruning/RecombinedPruningRule.cpp', 'src/markov/pruning/MaxSeparationPruningRule.cpp',
                                              'src/markov/pruning/UnlikelyHopPruningRule.cpp', 'src/markov/BitmapStateMapper.cpp', 'src/utilities/Bitmap.cpp',
                                              'src/markov/pruning/SpecificSeparationPruningRule.cpp', 'src/markov/StateMapperInterface.cpp', 'src/markov/StateList.cpp',
                                              'src/markov/RateCalculator.cpp', 'src/markov/ConfiguredObject.cpp', 'src/interactions/PeriodicCoulombModel.cpp',
                                              'src/rate_model/RateModel.cpp', 'src/rate_model/RecombiningRateModel.cpp', 'src/rate_model/MillerAbrahamsRateModel.cpp',
                                              'src/markov/adjacency/NearestNeighborAdjacency.cpp', 'src/interactions/EnergyLevelModel.cpp',
                                              'src/math/MatrixCoarsener.cpp', 'src/markov/ChainAnalyzer.cpp', 'src/markov/MonteCarlo.cpp'
                                              ],
                    swig_opts=['-c++']+map(lambda x: "-I" + x, extra_includes),
                    include_dirs = [np.get_include(),'extern', 'src/metropolis', 'src/metropolis/samplers', 'src', 'src/results', 'src/worlds', 'src/utilities', 'src/mobility', 'src/math', 'src/getters/energy', 'src/getters/mobility', 'src/interactions', 'src/markov', 'src/rate_model', 'src/markov/adjacency'] + extra_includes,
                    library_dirs = lib_dirs,
                    libraries = ["log4cxx", 'gsl', 'gslcblas']+libs,
                    extra_compile_args= ['-msse', '-msse2', '-msse3', '-mssse3', '-msse4.1', '-msse4.2', '-mfpmath=sse', '-O3', '-g', '-fopenmp'],
                    extra_objects=extra_link_args,
                    extra_link_args=['-fopenmp']
                    )],
        py_modules=['kineticmc', 'kmc_config', 'solarsettings'],
    )
